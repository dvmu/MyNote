﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Cut_Form
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.KeyWord = New System.Windows.Forms.TabPage()
        Me.CheckBox_WID = New System.Windows.Forms.CheckBox()
        Me.Button_WCancel = New System.Windows.Forms.Button()
        Me.Button_WOK = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox_KeyWord = New System.Windows.Forms.TextBox()
        Me.WordNum = New System.Windows.Forms.TabPage()
        Me.CheckBox_NID = New System.Windows.Forms.CheckBox()
        Me.Button_NCancel = New System.Windows.Forms.Button()
        Me.Button_NOK = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox_Num = New System.Windows.Forms.TextBox()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.TabControl1.SuspendLayout()
        Me.KeyWord.SuspendLayout()
        Me.WordNum.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.KeyWord)
        Me.TabControl1.Controls.Add(Me.WordNum)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(218, 169)
        Me.TabControl1.TabIndex = 5
        '
        'KeyWord
        '
        Me.KeyWord.Controls.Add(Me.CheckBox_WID)
        Me.KeyWord.Controls.Add(Me.Button_WCancel)
        Me.KeyWord.Controls.Add(Me.Button_WOK)
        Me.KeyWord.Controls.Add(Me.Label1)
        Me.KeyWord.Controls.Add(Me.TextBox_KeyWord)
        Me.KeyWord.Location = New System.Drawing.Point(4, 22)
        Me.KeyWord.Name = "KeyWord"
        Me.KeyWord.Padding = New System.Windows.Forms.Padding(3)
        Me.KeyWord.Size = New System.Drawing.Size(210, 143)
        Me.KeyWord.TabIndex = 0
        Me.KeyWord.Text = "按关键字"
        Me.KeyWord.UseVisualStyleBackColor = True
        '
        'CheckBox_WID
        '
        Me.CheckBox_WID.AutoSize = True
        Me.CheckBox_WID.Location = New System.Drawing.Point(11, 87)
        Me.CheckBox_WID.Name = "CheckBox_WID"
        Me.CheckBox_WID.Size = New System.Drawing.Size(48, 16)
        Me.CheckBox_WID.TabIndex = 1
        Me.CheckBox_WID.Text = "编号"
        Me.CheckBox_WID.UseVisualStyleBackColor = True
        '
        'Button_WCancel
        '
        Me.Button_WCancel.Location = New System.Drawing.Point(116, 109)
        Me.Button_WCancel.Name = "Button_WCancel"
        Me.Button_WCancel.Size = New System.Drawing.Size(80, 25)
        Me.Button_WCancel.TabIndex = 3
        Me.Button_WCancel.Text = "取消"
        Me.Button_WCancel.UseVisualStyleBackColor = True
        '
        'Button_WOK
        '
        Me.Button_WOK.Location = New System.Drawing.Point(11, 109)
        Me.Button_WOK.Name = "Button_WOK"
        Me.Button_WOK.Size = New System.Drawing.Size(80, 25)
        Me.Button_WOK.TabIndex = 2
        Me.Button_WOK.Text = "确定"
        Me.Button_WOK.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 6)
        Me.Label1.MaximumSize = New System.Drawing.Size(188, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(185, 48)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "请输入关键字：" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "说明：关键字用于标识章节名，一般为：""章""、""节""、""篇""、""卷""等。"
        '
        'TextBox_KeyWord
        '
        Me.TextBox_KeyWord.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.TextBox_KeyWord.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.TextBox_KeyWord.Location = New System.Drawing.Point(11, 60)
        Me.TextBox_KeyWord.Name = "TextBox_KeyWord"
        Me.TextBox_KeyWord.Size = New System.Drawing.Size(185, 21)
        Me.TextBox_KeyWord.TabIndex = 0
        '
        'WordNum
        '
        Me.WordNum.Controls.Add(Me.CheckBox_NID)
        Me.WordNum.Controls.Add(Me.Button_NCancel)
        Me.WordNum.Controls.Add(Me.Button_NOK)
        Me.WordNum.Controls.Add(Me.Label2)
        Me.WordNum.Controls.Add(Me.TextBox_Num)
        Me.WordNum.Location = New System.Drawing.Point(4, 22)
        Me.WordNum.Name = "WordNum"
        Me.WordNum.Padding = New System.Windows.Forms.Padding(3)
        Me.WordNum.Size = New System.Drawing.Size(210, 143)
        Me.WordNum.TabIndex = 1
        Me.WordNum.Text = "固定字数"
        Me.WordNum.UseVisualStyleBackColor = True
        '
        'CheckBox_NID
        '
        Me.CheckBox_NID.AutoSize = True
        Me.CheckBox_NID.Location = New System.Drawing.Point(11, 87)
        Me.CheckBox_NID.Name = "CheckBox_NID"
        Me.CheckBox_NID.Size = New System.Drawing.Size(48, 16)
        Me.CheckBox_NID.TabIndex = 7
        Me.CheckBox_NID.Text = "编号"
        Me.CheckBox_NID.UseVisualStyleBackColor = True
        '
        'Button_NCancel
        '
        Me.Button_NCancel.Location = New System.Drawing.Point(116, 109)
        Me.Button_NCancel.Name = "Button_NCancel"
        Me.Button_NCancel.Size = New System.Drawing.Size(80, 25)
        Me.Button_NCancel.TabIndex = 9
        Me.Button_NCancel.Text = "取消"
        Me.Button_NCancel.UseVisualStyleBackColor = True
        '
        'Button_NOK
        '
        Me.Button_NOK.Location = New System.Drawing.Point(11, 109)
        Me.Button_NOK.Name = "Button_NOK"
        Me.Button_NOK.Size = New System.Drawing.Size(80, 25)
        Me.Button_NOK.TabIndex = 8
        Me.Button_NOK.Text = "确定"
        Me.Button_NOK.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 6)
        Me.Label2.MaximumSize = New System.Drawing.Size(188, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(185, 36)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "请输入字数：" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "说明：字数指每一段的文本字数，包括标点符号。"
        '
        'TextBox_Num
        '
        Me.TextBox_Num.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.TextBox_Num.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.TextBox_Num.Location = New System.Drawing.Point(11, 60)
        Me.TextBox_Num.Name = "TextBox_Num"
        Me.TextBox_Num.Size = New System.Drawing.Size(185, 21)
        Me.TextBox_Num.TabIndex = 6
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ProgressBar1.Location = New System.Drawing.Point(0, 169)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(218, 8)
        Me.ProgressBar1.TabIndex = 16
        '
        'Cut_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(218, 177)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Cut_Form"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "分割当前文本"
        Me.TabControl1.ResumeLayout(False)
        Me.KeyWord.ResumeLayout(False)
        Me.KeyWord.PerformLayout()
        Me.WordNum.ResumeLayout(False)
        Me.WordNum.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents KeyWord As System.Windows.Forms.TabPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox_KeyWord As System.Windows.Forms.TextBox
    Friend WithEvents WordNum As System.Windows.Forms.TabPage
    Friend WithEvents Button_WCancel As System.Windows.Forms.Button
    Friend WithEvents Button_WOK As System.Windows.Forms.Button
    Friend WithEvents Button_NCancel As System.Windows.Forms.Button
    Friend WithEvents Button_NOK As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox_Num As System.Windows.Forms.TextBox
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents CheckBox_WID As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox_NID As System.Windows.Forms.CheckBox
End Class
