Imports System.net.Mail
Imports System.Text
Imports System.Security
Imports System.Net.Sockets

Public Class Email_Form
    Public UserStr As String = ""
    Public PswdStr As String
    Sub CheckEmail()
        Me.Cursor = Cursors.WaitCursor
        Main_Form.AutoUpDate(Main_Form.NewVerUrl, False, False)
        If UserStr <> "" Then btnSend.Enabled = True
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Dim mail As New MailMessage()
        mail.From = New MailAddress(UserStr)
        mail.To.Add("dvmu@163.com")
        'set the content
        mail.Subject = txtSubject.Text
        mail.Body = txtContent.Text
        ''specify the priority of the mail message
        mail.Priority = MailPriority.High
        Dim smtp As New SmtpClient("smtp." & UserStr.Split("@")(1))
        smtp.Credentials = New System.Net.NetworkCredential(UserStr, EncryptMod.DeText(PswdStr, "1243"))
        '检测是否有附件
        If TextBox1.Text.Trim <> "" Then  ''带附件   
            If Dir(TextBox1.Text.Trim) <> "" Then
                Dim attach As Net.Mail.Attachment = New Net.Mail.Attachment(TextBox1.Text)
                mail.Attachments.Add(attach)
            End If
        End If
        Try
            Me.Cursor = Cursors.WaitCursor
            smtp.Send(mail)
            Me.Cursor = Cursors.WaitCursor
            MsgBox("感谢您的反馈，我会尽快解决您发现的问题！", MsgBoxStyle.Information, "信息")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "反馈发送失败")
        End Try

    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If OpenFile_Pic.ShowDialog = Windows.Forms.DialogResult.OK Then
            TextBox1.Text = OpenFile_Pic.FileName
        End If
    End Sub

    Private Sub Email_Form_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        CheckEmail()
    End Sub
End Class
