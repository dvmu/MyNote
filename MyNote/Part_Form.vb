﻿Public Class Part_Form

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Main_Form.ConText.SelectionBullet = CheckBox1.Checked
        Select Case ComboBox1.SelectedIndex '对齐方式
            Case 0 '左对齐
                Main_Form.ConText.SelectionAlignment = HorizontalAlignment.Left
            Case 1 '右对齐
                Main_Form.ConText.SelectionAlignment = HorizontalAlignment.Right
            Case 2 '居中对齐
                Main_Form.ConText.SelectionAlignment = HorizontalAlignment.Center
            Case Else
                Exit Select
        End Select

        Select Case ComboBox2.SelectedIndex '上标下标
            Case 0 '上标
                Main_Form.ConText.SelectionCharOffset = 5
            Case 1 '下标
                Main_Form.ConText.SelectionCharOffset = -5
            Case 2 '普通
                Main_Form.ConText.SelectionCharOffset = 0
            Case Else
                Exit Select
        End Select

        Select Case ComboBox3.SelectedIndex '缩进距离
            Case 0
                Main_Form.ConText.SelectionIndent = Val(TextBox1.Text)
                Main_Form.ConText.SelectionHangingIndent = -Val(TextBox1.Text)
            Case 1 '悬停缩进
                Main_Form.ConText.SelectionHangingIndent = Val(TextBox1.Text)
            Case 2 '左
                Main_Form.ConText.SelectionIndent = Val(TextBox1.Text)
            Case 3 '右
                Main_Form.ConText.SelectionRightIndent = Val(TextBox1.Text)
            Case Else
                Exit Select
        End Select
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Char.IsDigit(e.KeyChar) Or e.KeyChar = Chr(8) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub


    Private Sub Part_Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CheckBox1.Checked = Main_Form.ConText.SelectionBullet
        Select Case Main_Form.ConText.SelectionAlignment '对齐方式
            Case HorizontalAlignment.Left '左对齐
                ComboBox1.SelectedIndex = 0
            Case HorizontalAlignment.Right  '右对齐
                ComboBox1.SelectedIndex = 1
            Case HorizontalAlignment.Center '居中对齐
                ComboBox1.SelectedIndex = 2
            Case Else
                Exit Select
        End Select

        Select Case Main_Form.ConText.SelectionCharOffset '上标下标
            Case Is > 0 '上标
                ComboBox2.SelectedIndex = 0
            Case Is < 0 '下标
                ComboBox2.SelectedIndex = 1
            Case 0 '普通
                ComboBox2.SelectedIndex = 2
            Case Else
                Exit Select
        End Select

        ComboBox3.SelectedIndex = 0

    End Sub
End Class