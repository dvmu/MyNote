﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Symbol_Form
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ComboBox_Symbol = New System.Windows.Forms.ComboBox()
        Me.ConSymbol = New System.Windows.Forms.RichTextBox()
        Me.Button_SSave = New System.Windows.Forms.Button()
        Me.Button_SOK = New System.Windows.Forms.Button()
        Me.Button_SC = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'ComboBox_Symbol
        '
        Me.ComboBox_Symbol.FormattingEnabled = True
        Me.ComboBox_Symbol.Location = New System.Drawing.Point(12, 12)
        Me.ComboBox_Symbol.Name = "ComboBox_Symbol"
        Me.ComboBox_Symbol.Size = New System.Drawing.Size(260, 20)
        Me.ComboBox_Symbol.Sorted = True
        Me.ComboBox_Symbol.TabIndex = 0
        '
        'ConSymbol
        '
        Me.ConSymbol.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.ConSymbol.Location = New System.Drawing.Point(12, 38)
        Me.ConSymbol.Name = "ConSymbol"
        Me.ConSymbol.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.ConSymbol.Size = New System.Drawing.Size(260, 185)
        Me.ConSymbol.TabIndex = 1
        Me.ConSymbol.Text = ""
        '
        'Button_SSave
        '
        Me.Button_SSave.Location = New System.Drawing.Point(12, 229)
        Me.Button_SSave.Name = "Button_SSave"
        Me.Button_SSave.Size = New System.Drawing.Size(82, 21)
        Me.Button_SSave.TabIndex = 2
        Me.Button_SSave.Text = "保存"
        Me.Button_SSave.UseVisualStyleBackColor = True
        '
        'Button_SOK
        '
        Me.Button_SOK.Location = New System.Drawing.Point(99, 229)
        Me.Button_SOK.Name = "Button_SOK"
        Me.Button_SOK.Size = New System.Drawing.Size(82, 21)
        Me.Button_SOK.TabIndex = 3
        Me.Button_SOK.Text = "插入"
        Me.Button_SOK.UseVisualStyleBackColor = True
        '
        'Button_SC
        '
        Me.Button_SC.Location = New System.Drawing.Point(187, 229)
        Me.Button_SC.Name = "Button_SC"
        Me.Button_SC.Size = New System.Drawing.Size(82, 21)
        Me.Button_SC.TabIndex = 4
        Me.Button_SC.Text = "取消"
        Me.Button_SC.UseVisualStyleBackColor = True
        '
        'Symbol_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.Button_SC)
        Me.Controls.Add(Me.Button_SOK)
        Me.Controls.Add(Me.Button_SSave)
        Me.Controls.Add(Me.ConSymbol)
        Me.Controls.Add(Me.ComboBox_Symbol)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Symbol_Form"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "特殊符号"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ComboBox_Symbol As System.Windows.Forms.ComboBox
    Friend WithEvents ConSymbol As System.Windows.Forms.RichTextBox
    Friend WithEvents Button_SSave As System.Windows.Forms.Button
    Friend WithEvents Button_SOK As System.Windows.Forms.Button
    Friend WithEvents Button_SC As System.Windows.Forms.Button
End Class
