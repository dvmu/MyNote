﻿Imports System.Data.OleDb

Public Class Symbol_Form

    Private Sub Button_SC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SC.Click
        Me.Close()
    End Sub

    Private Sub Symbol_Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        SaveSetting("MyNote", "Symbol", "Selected", Me.ComboBox_Symbol.SelectedIndex)
    End Sub

    Private Sub Symbol_Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim AccessString As String = "SELECT DISTINCT 分类 FROM 符号 "
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            ComboBox_Symbol.Items.Clear()
            For i As Integer = 0 To TempDataSet.Tables(0).Rows.Count - 1
                ComboBox_Symbol.Items.Add(TempDataSet.Tables(0).Rows(i).Item(0))
            Next
            ComboBox_Symbol.SelectedIndex = GetSetting("MyNote", "Symbol", "Selected", "0")
        Catch AccessException As Exception
            MsgBox("读取符号分类出错" & vbCrLf & "信息：" & AccessException.Message, , "符号")
        End Try
    End Sub

    Private Sub ComboBox_Symbol_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_Symbol.SelectedIndexChanged
        Try
            Dim AccessString As String = "SELECT 内容 FROM 符号 WHERE 分类=" & "'" & ComboBox_Symbol.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
            ConSymbol.Rtf = TempDataSet.Tables(0).Rows(0).Item(0).ToString.Replace("双引号", """")
        Catch AccessException As Exception
            MsgBox("读取符号分类出错" & vbCrLf & "信息：" & AccessException.Message, , "符号")
        End Try
    End Sub

    Sub AddClass(ByVal ClassName As String)
        Try
            Dim AccessString As String = "INSERT INTO 符号(分类) VALUES(" & "'" & ClassName & "'" & ")"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "添加符号分类")
        End Try

        ComboBox_Symbol.Items.Add(ClassName)
    End Sub

    Private Sub Button_SSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SSave.Click
        If ConSymbol.Text = "" Then
            If MsgBox("当前符号集为空，保存操作将删除该分类！是否继续？", MsgBoxStyle.YesNo, "警告") = MsgBoxResult.Yes Then
                Try
                    Dim AccessString As String = "DELETE FROM 符号 WHERE 分类=" & "'" & ComboBox_Symbol.Text & "'"
                    Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
                    AccessConn.Open()
                    Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
                    AccessCmd.ExecuteNonQuery()
                    AccessConn.Close()
                Catch AccessException As Exception
                    MsgBox(AccessException.Message, , "删除符号分类")
                End Try
                ComboBox_Symbol.Items.Remove(ComboBox_Symbol.SelectedItem)
                ComboBox_Symbol.SelectedIndex = 0
                Exit Sub
            Else
                Exit Sub
            End If
        End If

        Try
            Dim AccessString As String = "SELECT * FROM 符号 WHERE 分类=" & "'" & ComboBox_Symbol.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then
                AddClass(ComboBox_Symbol.Text)
            End If
        Catch AccessException As Exception
            MsgBox("读取符号分类出错" & vbCrLf & "信息：" & AccessException.Message, , "符号")
        End Try

        Try
            Dim AccessString As String = "UPDATE 符号 SET 内容=" & """" & ConSymbol.Rtf.Replace("""", "双引号") & """" & " WHERE 分类=" & "'" & ComboBox_Symbol.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存符号出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
    End Sub

    Private Sub ConSymbol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ConSymbol.Click
        ConSymbol.SelectionLength = 1
    End Sub

    Private Sub Button_SOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SOK.Click
        Main_Form.ConText.SelectedText = ConSymbol.SelectedText
        If ConSymbol.SelectedText = "" Then Return
        Dim SelfAdd As New RichTextBox
        Try
            Dim AccessString As String = "SELECT 内容 FROM 符号 WHERE 分类=" & "'" & "常用符号" & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then
                AddClass("常用符号")
            Else
                SelfAdd.Rtf = TempDataSet.Tables(0).Rows(0).Item(0).ToString.Replace("双引号", """")
            End If
        Catch AccessException As Exception
            MsgBox("读取符号分类出错" & vbCrLf & "信息：" & AccessException.Message, , "符号")
        End Try
        SelfAdd.Text = ConSymbol.SelectedText & SelfAdd.Text.Replace(ConSymbol.SelectedText, "")
        Try
            Dim AccessString As String = "UPDATE 符号 SET 内容=" & """" & SelfAdd.Rtf.Replace("""", "双引号") & """" & " WHERE 分类=" & "'" & "常用符号" & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存符号出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try

    End Sub

End Class