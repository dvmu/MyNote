﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UpDate_Form
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button_Up = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox_Cur = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox_New = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox_Up = New System.Windows.Forms.TextBox()
        Me.Button_Exit = New System.Windows.Forms.Button()
        Me.Label_Link = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Button_Up
        '
        Me.Button_Up.Location = New System.Drawing.Point(182, 12)
        Me.Button_Up.Name = "Button_Up"
        Me.Button_Up.Size = New System.Drawing.Size(85, 21)
        Me.Button_Up.TabIndex = 1
        Me.Button_Up.Text = "更新"
        Me.Button_Up.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 12)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "当前版本:"
        '
        'TextBox_Cur
        '
        Me.TextBox_Cur.Location = New System.Drawing.Point(77, 12)
        Me.TextBox_Cur.Name = "TextBox_Cur"
        Me.TextBox_Cur.Size = New System.Drawing.Size(86, 21)
        Me.TextBox_Cur.TabIndex = 0
        Me.TextBox_Cur.Text = "正在读取……"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 12)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "最新版本:"
        '
        'TextBox_New
        '
        Me.TextBox_New.Location = New System.Drawing.Point(77, 42)
        Me.TextBox_New.Name = "TextBox_New"
        Me.TextBox_New.Size = New System.Drawing.Size(86, 21)
        Me.TextBox_New.TabIndex = 2
        Me.TextBox_New.Text = "正在读取……"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 12)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "更新内容:"
        '
        'TextBox_Up
        '
        Me.TextBox_Up.Location = New System.Drawing.Point(14, 84)
        Me.TextBox_Up.Multiline = True
        Me.TextBox_Up.Name = "TextBox_Up"
        Me.TextBox_Up.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox_Up.Size = New System.Drawing.Size(253, 75)
        Me.TextBox_Up.TabIndex = 4
        Me.TextBox_Up.Text = "正在读取……"
        '
        'Button_Exit
        '
        Me.Button_Exit.Location = New System.Drawing.Point(182, 42)
        Me.Button_Exit.Name = "Button_Exit"
        Me.Button_Exit.Size = New System.Drawing.Size(85, 21)
        Me.Button_Exit.TabIndex = 3
        Me.Button_Exit.Text = "取消"
        Me.Button_Exit.UseVisualStyleBackColor = True
        '
        'Label_Link
        '
        Me.Label_Link.AutoSize = True
        Me.Label_Link.Location = New System.Drawing.Point(77, 69)
        Me.Label_Link.Name = "Label_Link"
        Me.Label_Link.Size = New System.Drawing.Size(0, 12)
        Me.Label_Link.TabIndex = 6
        Me.Label_Link.Visible = False
        '
        'UpDate_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(279, 171)
        Me.Controls.Add(Me.Label_Link)
        Me.Controls.Add(Me.Button_Exit)
        Me.Controls.Add(Me.TextBox_Up)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBox_New)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBox_Cur)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button_Up)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "UpDate_Form"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "检查更新"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button_Up As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox_Cur As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox_New As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox_Up As System.Windows.Forms.TextBox
    Friend WithEvents Button_Exit As System.Windows.Forms.Button
    Friend WithEvents Label_Link As System.Windows.Forms.Label
End Class
