﻿Imports System.IO

Public Class MulitText_Form

    Private Sub File_List_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles File_List.DoubleClick
        FolderBrowserDialog1.RootFolder = Environment.SpecialFolder.Desktop
        FolderBrowserDialog1.ShowNewFolderButton = False
        If FolderBrowserDialog1.ShowDialog() <> DialogResult.Cancel Then
            File_List.Items.Clear()
            Dim AllFile() As String = Directory.GetFiles(FolderBrowserDialog1.SelectedPath)
            For i As Integer = 0 To AllFile.Length - 1
                File_List.Items.Add(Path.GetFileName(AllFile(i)))
            Next
        End If

    End Sub

    Private Sub File_List_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles File_List.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Right Then
            If File_List.SelectedItem IsNot Nothing Then File_List.Items.Remove(File_List.SelectedItem)
        End If
    End Sub

    Private Sub File_List_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles File_List.SelectedIndexChanged
        If File_List.SelectedItem IsNot Nothing Then TextBox_Pr.Text = File.ReadAllText(FolderBrowserDialog1.SelectedPath & "\" & File_List.SelectedItem.ToString, System.Text.Encoding.Default)
    End Sub


    Private Sub Button_AR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_AR.Click
        If File_List.Items.Count = 0 Then Exit Sub
        If TextBox_Old.Text = "" Then
            MsgBox("要替换的文本为空！", Microsoft.VisualBasic.MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        Dim AllFile() As String = Directory.GetFiles(FolderBrowserDialog1.SelectedPath)
        ProgressBar1.Maximum = AllFile.Length
        For i As Integer = 0 To AllFile.Length - 1
            ProgressBar1.Value = i
            Dim TempText As String = File.ReadAllText(AllFile(i), System.Text.Encoding.Default)
            TempText = TempText.Replace(TextBox_Old.Text, TextBox_New.Text)
            File.WriteAllText(AllFile(i), TempText, System.Text.Encoding.Default)
        Next
        ProgressBar1.Value = 0
    End Sub

    Private Sub Button_ZH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_ZH.Click
        If File_List.Items.Count = 0 Then Exit Sub
        Dim TempText As String = ""
        Dim TempDir As String = FolderBrowserDialog1.SelectedPath & "\"
        ProgressBar1.Maximum = File_List.Items.Count - 1
        For i As Integer = 0 To File_List.Items.Count - 1
            ProgressBar1.Value = i
            Dim OneText As String = File.ReadAllText(TempDir & File_List.Items(i).ToString, System.Text.Encoding.Default)
            If CheckBox_FN.Checked Then
                OneText = Path.GetFileName(TempDir & File_List.Items(i).ToString) & vbCrLf & OneText
            End If
            If CheckBox_ID.Checked Then
                OneText = i & " " & OneText
            End If
            If CheckBox_Sl.Checked Then
                OneText = OneText & vbCrLf & TextBox_Sl.Text
            End If
            If CheckBox_YL.Checked Then
                TextBox_Pr.AppendText(vbCrLf & OneText)
            End If

            TempText = TempText & vbCrLf & OneText
        Next
        File.WriteAllText(FolderBrowserDialog1.SelectedPath & "\" & "AllText.txt", TempText, System.Text.Encoding.Default)
        ProgressBar1.Value = 0
    End Sub
End Class