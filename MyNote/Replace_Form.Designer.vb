﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Replace_Form
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Replace_Form))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.查找 = New System.Windows.Forms.ToolStripButton()
        Me.Separator = New System.Windows.Forms.ToolStripSeparator()
        Me.替换 = New System.Windows.Forms.ToolStripButton()
        Me.Button_FNext = New System.Windows.Forms.Button()
        Me.Button_R = New System.Windows.Forms.Button()
        Me.Button_RA = New System.Windows.Forms.Button()
        Me.TextBox_R = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBox_F = New System.Windows.Forms.ComboBox()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 12)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "查找内容："
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 12)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "替换为："
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.查找, Me.Separator, Me.替换})
        Me.ToolStrip1.Location = New System.Drawing.Point(3, 3)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(391, 25)
        Me.ToolStrip1.TabIndex = 7
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        '查找
        '
        Me.查找.CheckOnClick = True
        Me.查找.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.查找.Image = CType(resources.GetObject("查找.Image"), System.Drawing.Image)
        Me.查找.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.查找.Name = "查找"
        Me.查找.Size = New System.Drawing.Size(73, 22)
        Me.查找.Text = "查找文本(&F)"
        '
        'Separator
        '
        Me.Separator.Name = "Separator"
        Me.Separator.Size = New System.Drawing.Size(6, 25)
        '
        '替换
        '
        Me.替换.CheckOnClick = True
        Me.替换.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.替换.Image = CType(resources.GetObject("替换.Image"), System.Drawing.Image)
        Me.替换.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.替换.Name = "替换"
        Me.替换.Size = New System.Drawing.Size(76, 22)
        Me.替换.Text = "替换文本(&H)"
        '
        'Button_FNext
        '
        Me.Button_FNext.Location = New System.Drawing.Point(291, 115)
        Me.Button_FNext.Name = "Button_FNext"
        Me.Button_FNext.Size = New System.Drawing.Size(100, 21)
        Me.Button_FNext.TabIndex = 2
        Me.Button_FNext.Text = "查找(&N)"
        Me.Button_FNext.UseVisualStyleBackColor = True
        '
        'Button_R
        '
        Me.Button_R.Location = New System.Drawing.Point(185, 142)
        Me.Button_R.Name = "Button_R"
        Me.Button_R.Size = New System.Drawing.Size(100, 21)
        Me.Button_R.TabIndex = 3
        Me.Button_R.Text = "替换(&R)"
        Me.Button_R.UseVisualStyleBackColor = True
        '
        'Button_RA
        '
        Me.Button_RA.Location = New System.Drawing.Point(291, 142)
        Me.Button_RA.Name = "Button_RA"
        Me.Button_RA.Size = New System.Drawing.Size(100, 21)
        Me.Button_RA.TabIndex = 4
        Me.Button_RA.Text = "全部替换(&A)"
        Me.Button_RA.UseVisualStyleBackColor = True
        '
        'TextBox_R
        '
        Me.TextBox_R.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.TextBox_R.Location = New System.Drawing.Point(8, 88)
        Me.TextBox_R.Multiline = True
        Me.TextBox_R.Name = "TextBox_R"
        Me.TextBox_R.Size = New System.Drawing.Size(383, 21)
        Me.TextBox_R.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 119)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 12)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "等待操作......"
        '
        'ComboBox_F
        '
        Me.ComboBox_F.FormattingEnabled = True
        Me.ComboBox_F.Items.AddRange(New Object() {"X([^Y]*)Y", "X([^Y]+)Y", "[A-Za-z0-9_]", "[^A-Za-z0-9_]", "\S", "\s"})
        Me.ComboBox_F.Location = New System.Drawing.Point(8, 49)
        Me.ComboBox_F.Name = "ComboBox_F"
        Me.ComboBox_F.Size = New System.Drawing.Size(383, 20)
        Me.ComboBox_F.TabIndex = 0
        '
        'Replace_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(397, 169)
        Me.Controls.Add(Me.ComboBox_F)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBox_R)
        Me.Controls.Add(Me.Button_RA)
        Me.Controls.Add(Me.Button_R)
        Me.Controls.Add(Me.Button_FNext)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Replace_Form"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "查找和替换"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents Separator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Button_FNext As System.Windows.Forms.Button
    Friend WithEvents Button_R As System.Windows.Forms.Button
    Friend WithEvents Button_RA As System.Windows.Forms.Button
    Friend WithEvents 查找 As System.Windows.Forms.ToolStripButton
    Friend WithEvents 替换 As System.Windows.Forms.ToolStripButton
    Friend WithEvents TextBox_R As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_F As System.Windows.Forms.ComboBox
End Class
