﻿Imports System.Data.OleDb
Imports System.IO

Public Class Pass_Form
    Dim TestAccessConnectionString As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
            "Data Source=" & My.Application.Info.DirectoryPath & "\" & Main_Form.AccessDB & ".mdb;" & _
            "Persist Security Info=False"
    Dim AdminAccessConnectionString As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
            "Data Source=" & My.Application.Info.DirectoryPath & "\" & Main_Form.AccessDB & ".mdb;" & _
            "mode=12;Persist Security Info=False"
    Private Sub Button_Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Cancel.Click
        If Me.Text = "密码" Then
            End
        Else
            Me.Close()
        End If

    End Sub

#Region "密码"

    Sub PassWord()
        Try
            Select Case Me.Text
                Case "密码"
                    Try
                        Main_Form.AccessPassStr = ";jet oledb:database Password =" & TextBox_Pass.Text
                        Main_Form.AccessConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                            "Data Source=" & My.Application.Info.DirectoryPath & "\" & Main_Form.AccessDB & ".mdb;" & _
                            "Persist Security Info=False" & Main_Form.AccessPassStr
                        Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
                        AccessConn.Open()
                        AccessConn.Dispose()
                        Me.Close()
                    Catch AccessException As Exception
                        MsgBox(AccessException.Message, , "登录")
                        TextBox_Pass.Text = ""
                    End Try
                Case "修改密码"
                    Dim OldPw As String
                    Dim NewPw As String
                    If TextBox_NewPass.Visible = True Then
                        Try
                            Main_Form.AccessPassStr = ";jet oledb:database Password =" & TextBox_Pass.Text
                            Dim AccessConn As New OleDb.OleDbConnection(TestAccessConnectionString & Main_Form.AccessPassStr)
                            AccessConn.Open()
                            AccessConn.Dispose()
                        Catch AccessException As Exception
                            MsgBox(AccessException.Message, , "登录")
                            Me.Close()
                        End Try
                        If TextBox_NewPass.Text = "" Then
                            NewPw = "null"
                        Else
                            NewPw = TextBox_NewPass.Text
                        End If
                        If TextBox_Pass.Text = "" Then
                            OldPw = "null"
                        Else
                            OldPw = TextBox_Pass.Text
                        End If
                        Try
                            Dim AccessString As String = "ALTER DATABASE PASSWORD " & NewPw & " " & OldPw
                            Dim AccessConn As New OleDb.OleDbConnection(AdminAccessConnectionString & Main_Form.AccessPassStr)
                            AccessConn.Open()
                            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
                            AccessCmd.ExecuteNonQuery()
                            AccessConn.Dispose()
                            Main_Form.AccessPassStr = ";jet oledb:database Password =" & TextBox_NewPass.Text
                            Main_Form.AccessConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                                "Data Source=" & My.Application.Info.DirectoryPath & "\" & Main_Form.AccessDB & ".mdb;" & _
                                "Persist Security Info=False" & Main_Form.AccessPassStr
                            MsgBox("密码修改成功！")
                            Me.Close()
                        Catch AccessException As Exception
                            MsgBox(AccessException.Message)
                            Me.Close()
                        End Try
                    Else
                        If TextBox_Pass.Text = "" Then
                            NewPw = "null"
                        Else
                            NewPw = TextBox_Pass.Text
                        End If
                        Try
                            Dim AccessString As String = "ALTER DATABASE PASSWORD " & NewPw & " " & "null"
                            Dim AccessConn As New OleDb.OleDbConnection(AdminAccessConnectionString)
                            AccessConn.Open()
                            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
                            AccessCmd.ExecuteNonQuery()
                            AccessConn.Dispose()
                            Main_Form.AccessPassStr = ";jet oledb:database Password =" & TextBox_Pass.Text
                            Main_Form.AccessConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                                "Data Source=" & My.Application.Info.DirectoryPath & "\" & Main_Form.AccessDB & ".mdb;" & _
                                "Persist Security Info=False" & Main_Form.AccessPassStr
                            MsgBox("密码创建成功！")
                            Me.Close()
                        Catch AccessException As Exception
                            MsgBox(AccessException.Message)
                            Me.Close()
                        End Try
                    End If
                Case "清除密码"
                    Try
                        Dim NewPw As String
                        If TextBox_Pass.Text = "" Then
                            NewPw = "null"
                        Else
                            NewPw = TextBox_Pass.Text
                        End If
                        Dim AccessString As String = "ALTER DATABASE PASSWORD " & "null" & " " & NewPw
                        Dim AccessConn As New OleDb.OleDbConnection(AdminAccessConnectionString & Main_Form.AccessPassStr)
                        AccessConn.Open()
                        Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
                        AccessCmd.ExecuteNonQuery()
                        AccessConn.Dispose()
                        Main_Form.AccessConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                            "Data Source=" & My.Application.Info.DirectoryPath & "\" & Main_Form.AccessDB & ".mdb;" & _
                            "Persist Security Info=False"
                        Main_Form.AccessPassStr = ";jet oledb:database Password ="
                        MsgBox("密码已经清除，当前密码为空")
                        Me.Close()
                    Catch AccessException As Exception
                        MsgBox(AccessException.Message, , "登录")
                    End Try
                Case Else
                    Try
                        Main_Form.AccessPassStr = ";jet oledb:database Password =" & TextBox_Pass.Text
                        Main_Form.AccessConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                            "Data Source=" & My.Application.Info.DirectoryPath & "\" & Main_Form.AccessDB & ".mdb;" & _
                            "Persist Security Info=False" & Main_Form.AccessPassStr
                        Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
                        AccessConn.Open()
                        AccessConn.Dispose()
                        Main_Form.TreeView_Cata.Nodes(0).Collapse()
                        Main_Form.TreeView_Cata.Nodes(0).Expand()
                        Me.Close()
                    Catch AccessException As Exception
                        MsgBox(AccessException.Message, , "登录")
                        TextBox_Pass.Text = ""
                    End Try
            End Select
        Catch AccessException As Exception
            MsgBox(AccessException.Message)
        End Try
    End Sub

    Private Sub Button_OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_OK.Click
       PassWord()
    End Sub

    Private Sub TextBox_Pass_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_Pass.KeyPress
        If e.KeyChar = CChar(ChrW(13)) Then
            PassWord()
        End If
    End Sub

    Private Sub TextBox_NewPass_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_NewPass.KeyPress
        If e.KeyChar = CChar(ChrW(13)) Then
            PassWord()
        End If
    End Sub

#End Region

    Private Sub Pass_Form_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DoubleClick
        If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\osk.exe") Then System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\osk.exe")
    End Sub

    Private Sub Pass_Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            TestAccessConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
        "Data Source=" & My.Application.Info.DirectoryPath & "\" & Main_Form.AccessDB & ".mdb;" & _
        "Persist Security Info=False"
            AdminAccessConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                    "Data Source=" & My.Application.Info.DirectoryPath & "\" & Main_Form.AccessDB & ".mdb;" & _
                    "mode=12;Persist Security Info=False"
            TextBox_Pass.Text = ""
            TextBox_Pass.Focus()
            TextBox_NewPass.Text = ""
            Select Case Me.Text
                Case "密码"
                    T1()
                Case "修改密码"
                    Try
                        Dim AccessConn As New OleDb.OleDbConnection(TestAccessConnectionString)
                        AccessConn.Open()
                        AccessConn.Dispose()
                        T1()
                        Me.Label1.Text = "当前密码为空，请输入新密码:"
                    Catch AccessException As Exception
                        T2()
                        Me.Label1.Text = "请输入旧密码:"
                    End Try
                Case "清除密码"
                    Try
                        Dim AccessConn As New OleDb.OleDbConnection(TestAccessConnectionString)
                        AccessConn.Open()
                        AccessConn.Dispose()
                        MsgBox("当前密码为空，无需清除")
                        Me.Close()
                    Catch AccessException As Exception
                        T1()
                        Me.Label1.Text = "请输入密码:"
                    End Try
            End Select
        Catch AccessException As Exception
            MsgBox(AccessException.Message)
        End Try
    End Sub

    Sub T1()
        Label2.Visible = False
        TextBox_NewPass.Visible = False
        TextBox_Pass.PasswordChar = "*"
        Button_OK.Top = TextBox_Pass.Top + TextBox_Pass.Height + 8
        Button_Cancel.Top = TextBox_Pass.Top + TextBox_Pass.Height + 8
        Me.Height = Button_OK.Top + Button_OK.Height + 40
    End Sub

    Sub T2()
        Label2.Visible = True
        TextBox_NewPass.Visible = True
        TextBox_Pass.PasswordChar = ""
        Button_OK.Top = TextBox_NewPass.Top + TextBox_NewPass.Height + 8
        Button_Cancel.Top = TextBox_NewPass.Top + TextBox_NewPass.Height + 8
        Me.Height = Button_OK.Top + Button_OK.Height + 40
    End Sub

End Class