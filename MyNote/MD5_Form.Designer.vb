﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MD5_Form
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBox_Key = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button_L = New System.Windows.Forms.Button()
        Me.Button_U = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'TextBox_Key
        '
        Me.TextBox_Key.Location = New System.Drawing.Point(59, 12)
        Me.TextBox_Key.Name = "TextBox_Key"
        Me.TextBox_Key.Size = New System.Drawing.Size(133, 21)
        Me.TextBox_Key.TabIndex = 0
        Me.TextBox_Key.UseSystemPasswordChar = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 12)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "密码："
        '
        'Button_L
        '
        Me.Button_L.Location = New System.Drawing.Point(14, 39)
        Me.Button_L.Name = "Button_L"
        Me.Button_L.Size = New System.Drawing.Size(81, 30)
        Me.Button_L.TabIndex = 2
        Me.Button_L.Text = "加密"
        Me.Button_L.UseVisualStyleBackColor = True
        '
        'Button_U
        '
        Me.Button_U.Location = New System.Drawing.Point(111, 39)
        Me.Button_U.Name = "Button_U"
        Me.Button_U.Size = New System.Drawing.Size(81, 30)
        Me.Button_U.TabIndex = 3
        Me.Button_U.Text = "解密"
        Me.Button_U.UseVisualStyleBackColor = True
        '
        'MD5_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(204, 81)
        Me.Controls.Add(Me.Button_U)
        Me.Controls.Add(Me.Button_L)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox_Key)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MD5_Form"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "文本加密解密"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBox_Key As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button_L As System.Windows.Forms.Button
    Friend WithEvents Button_U As System.Windows.Forms.Button
End Class
