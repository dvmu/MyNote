﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Text_Form
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Rich_O1 = New System.Windows.Forms.RichTextBox()
        Me.Rich_O2 = New System.Windows.Forms.RichTextBox()
        Me.Rich_O3 = New System.Windows.Forms.RichTextBox()
        Me.Radio_VK = New System.Windows.Forms.RadioButton()
        Me.Radio_CP = New System.Windows.Forms.RadioButton()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Button_O = New System.Windows.Forms.Button()
        Me.Button_C = New System.Windows.Forms.Button()
        Me.Button_Del = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Rich_O1
        '
        Me.Rich_O1.Location = New System.Drawing.Point(6, 20)
        Me.Rich_O1.Name = "Rich_O1"
        Me.Rich_O1.Size = New System.Drawing.Size(206, 181)
        Me.Rich_O1.TabIndex = 3
        Me.Rich_O1.Text = ""
        '
        'Rich_O2
        '
        Me.Rich_O2.Location = New System.Drawing.Point(6, 20)
        Me.Rich_O2.Name = "Rich_O2"
        Me.Rich_O2.Size = New System.Drawing.Size(201, 96)
        Me.Rich_O2.TabIndex = 6
        Me.Rich_O2.Text = ""
        '
        'Rich_O3
        '
        Me.Rich_O3.Location = New System.Drawing.Point(6, 20)
        Me.Rich_O3.Name = "Rich_O3"
        Me.Rich_O3.Size = New System.Drawing.Size(201, 102)
        Me.Rich_O3.TabIndex = 9
        Me.Rich_O3.Text = ""
        '
        'Radio_VK
        '
        Me.Radio_VK.AutoSize = True
        Me.Radio_VK.Checked = True
        Me.Radio_VK.Location = New System.Drawing.Point(6, 20)
        Me.Radio_VK.Name = "Radio_VK"
        Me.Radio_VK.Size = New System.Drawing.Size(71, 16)
        Me.Radio_VK.TabIndex = 1
        Me.Radio_VK.TabStop = True
        Me.Radio_VK.Text = "模拟按键"
        Me.Radio_VK.UseVisualStyleBackColor = True
        '
        'Radio_CP
        '
        Me.Radio_CP.AutoSize = True
        Me.Radio_CP.Location = New System.Drawing.Point(83, 20)
        Me.Radio_CP.Name = "Radio_CP"
        Me.Radio_CP.Size = New System.Drawing.Size(71, 16)
        Me.Radio_CP.TabIndex = 2
        Me.Radio_CP.Text = "复制粘贴"
        Me.Radio_CP.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(14, 12)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(153, 20)
        Me.ComboBox1.TabIndex = 0
        '
        'Button_O
        '
        Me.Button_O.Location = New System.Drawing.Point(294, 274)
        Me.Button_O.Name = "Button_O"
        Me.Button_O.Size = New System.Drawing.Size(80, 23)
        Me.Button_O.TabIndex = 11
        Me.Button_O.Text = "保存"
        Me.Button_O.UseVisualStyleBackColor = True
        '
        'Button_C
        '
        Me.Button_C.Location = New System.Drawing.Point(380, 274)
        Me.Button_C.Name = "Button_C"
        Me.Button_C.Size = New System.Drawing.Size(80, 23)
        Me.Button_C.TabIndex = 12
        Me.Button_C.Text = "取消"
        Me.Button_C.UseVisualStyleBackColor = True
        '
        'Button_Del
        '
        Me.Button_Del.Location = New System.Drawing.Point(173, 12)
        Me.Button_Del.Name = "Button_Del"
        Me.Button_Del.Size = New System.Drawing.Size(61, 20)
        Me.Button_Del.TabIndex = 13
        Me.Button_Del.Text = "删除"
        Me.Button_Del.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Radio_VK)
        Me.GroupBox1.Controls.Add(Me.Radio_CP)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 38)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(220, 45)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "发送模式"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Rich_O1)
        Me.GroupBox3.Location = New System.Drawing.Point(14, 89)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(218, 207)
        Me.GroupBox3.TabIndex = 16
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Text One"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Rich_O2)
        Me.GroupBox4.Location = New System.Drawing.Point(247, 12)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(213, 122)
        Me.GroupBox4.TabIndex = 17
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Text Two"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Rich_O3)
        Me.GroupBox5.Location = New System.Drawing.Point(247, 140)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(213, 128)
        Me.GroupBox5.TabIndex = 18
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Text Thr"
        '
        'Text_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(474, 308)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button_Del)
        Me.Controls.Add(Me.Button_C)
        Me.Controls.Add(Me.Button_O)
        Me.Controls.Add(Me.ComboBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Text_Form"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "常用短语"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Rich_O1 As System.Windows.Forms.RichTextBox
    Friend WithEvents Rich_O2 As System.Windows.Forms.RichTextBox
    Friend WithEvents Rich_O3 As System.Windows.Forms.RichTextBox
    Friend WithEvents Radio_VK As System.Windows.Forms.RadioButton
    Friend WithEvents Radio_CP As System.Windows.Forms.RadioButton
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Button_O As System.Windows.Forms.Button
    Friend WithEvents Button_C As System.Windows.Forms.Button
    Friend WithEvents Button_Del As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
End Class
