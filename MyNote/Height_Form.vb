﻿Imports System.Data.OleDb

Public Class Height_Form

    Private Sub Height_Form_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Main_Form.LoadYF()
    End Sub

    Sub AddClass(ByVal ClassName As String)
        Try
            Dim AccessString As String = "INSERT INTO 高亮(语言) VALUES(" & "'" & ClassName & "'" & ")"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
            ComboBox1.Items.Add(ClassName)
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "添加高亮语言分类")
        End Try
    End Sub

    Private Sub Button_Save_Click(sender As System.Object, e As System.EventArgs) Handles Button_Save.Click
        Try
            Dim AccessString As String = "SELECT * FROM 高亮 WHERE 语言=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then
                AddClass(ComboBox1.Text)
            End If
        Catch AccessException As Exception
            MsgBox("读取短语分类出错" & vbCrLf & "信息：" & AccessException.Message, , "短语")
        End Try

        Try
            Dim AccessString As String = "UPDATE 高亮 SET 蓝色 = " & """" & TextBox1.Text.Replace("""", "双引号") & """" & " WHERE 语言=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存蓝色出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 高亮 SET 绿色 = " & """" & TextBox2.Text.Replace("""", "双引号") & """" & " WHERE 语言=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存绿色出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 高亮 SET 蓝绿 = " & """" & TextBox3.Text.Replace("""", "双引号") & """" & " WHERE 语言=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存蓝绿出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 高亮 SET 红色 = " & """" & TextBox4.Text.Replace("""", "双引号") & """" & " WHERE 语言=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存红色出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 高亮 SET 灰色 = " & """" & TextBox5.Text.Replace("""", "双引号") & """" & " WHERE 语言=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存灰色出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 高亮 SET 紫色 = " & """" & TextBox6.Text.Replace("""", "双引号") & """" & " WHERE 语言=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存紫色出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 高亮 SET 青色 = " & """" & TextBox7.Text.Replace("""", "双引号") & """" & " WHERE 语言=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存青色出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 高亮 SET 加粗 = " & """" & TextBox8.Text.Replace("""", "双引号") & """" & " WHERE 语言=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存加粗出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 高亮 SET 划线 = " & """" & TextBox9.Text.Replace("""", "双引号") & """" & " WHERE 语言=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存划线出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 高亮 SET 倾斜 = " & """" & TextBox10.Text.Replace("""", "双引号") & """" & " WHERE 语言=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存倾斜出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        MsgBox("语法高亮保存成功", MsgBoxStyle.Information, "保存")
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Try
            Dim AccessString As String = "SELECT * FROM 高亮 WHERE 语言='" & sender.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
            TextBox1.Text = TempDataSet.Tables(0).Rows(0).Item(1).ToString.Replace("双引号", """").Trim
            TextBox2.Text = TempDataSet.Tables(0).Rows(0).Item(2).ToString.Replace("双引号", """").Trim
            TextBox3.Text = TempDataSet.Tables(0).Rows(0).Item(3).ToString.Replace("双引号", """").Trim
            TextBox4.Text = TempDataSet.Tables(0).Rows(0).Item(4).ToString.Replace("双引号", """").Trim
            TextBox5.Text = TempDataSet.Tables(0).Rows(0).Item(5).ToString.Replace("双引号", """").Trim
            TextBox6.Text = TempDataSet.Tables(0).Rows(0).Item(6).ToString.Replace("双引号", """").Trim
            TextBox7.Text = TempDataSet.Tables(0).Rows(0).Item(7).ToString.Replace("双引号", """").Trim
            TextBox8.Text = TempDataSet.Tables(0).Rows(0).Item(8).ToString.Replace("双引号", """").Trim
            TextBox9.Text = TempDataSet.Tables(0).Rows(0).Item(9).ToString.Replace("双引号", """").Trim
            TextBox10.Text = TempDataSet.Tables(0).Rows(0).Item(10).ToString.Replace("双引号", """").Trim
        Catch AccessException As Exception
            MsgBox("读取高亮信息出错" & vbCrLf & "信息：" & AccessException.Message, , "读取")
        End Try
    End Sub



End Class

