﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main_Form
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("烂笔头", 8, 8)
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main_Form))
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("搜索", 9, 9)
        Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("NULL")
        Dim TreeNode4 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("历史", 10, 10, New System.Windows.Forms.TreeNode() {TreeNode3})
        Me.FormBoder = New System.Windows.Forms.SplitContainer()
        Me.Tab_Tree = New System.Windows.Forms.TabControl()
        Me.Tab_CCata = New System.Windows.Forms.TabPage()
        Me.TreeView_Cata = New System.Windows.Forms.TreeView()
        Me.ContextMenuStrip_TreeCata = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.新建记录NToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.新建目录NToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.删除记录DToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.分割记录FToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.合并子级EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.添加照片AToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.清除照片DToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.其他功能RToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.改变图标TToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.导出图库AToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageList_TreeIcon = New System.Windows.Forms.ImageList(Me.components)
        Me.Com_DB = New System.Windows.Forms.ComboBox()
        Me.Tab_CSech = New System.Windows.Forms.TabPage()
        Me.TreeView_Seach = New System.Windows.Forms.TreeView()
        Me.TextBox_FindText = New System.Windows.Forms.TextBox()
        Me.Tab_CHsty = New System.Windows.Forms.TabPage()
        Me.TreeView_Mark = New System.Windows.Forms.TreeView()
        Me.Status_Tree = New System.Windows.Forms.StatusStrip()
        Me.当前ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.进度ToolStripProgressBar = New System.Windows.Forms.ToolStripProgressBar()
        Me.ConBoder = New System.Windows.Forms.SplitContainer()
        Me.ConText = New System.Windows.Forms.RichTextBox()
        Me.ContextMenuStrip_Context = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.已阅YCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.撤销UCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparatorC1 = New System.Windows.Forms.ToolStripSeparator()
        Me.剪切TCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.复制CCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.粘贴PCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.粘贴文本NCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.删除DCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparatorC2 = New System.Windows.Forms.ToolStripSeparator()
        Me.插入文本ICToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.符号ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.时间ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.日期ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.日期时间ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.自定义图片ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparatorC3 = New System.Windows.Forms.ToolStripSeparator()
        Me.全选ACToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.查找FCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.替换HCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparatorC4 = New System.Windows.Forms.ToolStripSeparator()
        Me.段落格式GCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.字体格式WCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureNode = New System.Windows.Forms.PictureBox()
        Me.ContextMenuStrip_Album = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.自动缩放ZToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.添加图片TToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.导出图片EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.全部导出AToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.删除图片DToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.清空图片CToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStrip_ConText = New System.Windows.Forms.ToolStrip()
        Me.新建NToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.打开OToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.保存SToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparatorT1 = New System.Windows.Forms.ToolStripSeparator()
        Me.撤销UToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.重做RToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparatorT2 = New System.Windows.Forms.ToolStripSeparator()
        Me.剪切UToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.复制CToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.粘贴PToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparatorT3 = New System.Windows.Forms.ToolStripSeparator()
        Me.格式字体ToolStripButton = New System.Windows.Forms.ToolStripSplitButton()
        Me.加粗字体ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.斜体字体ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.加粗倾斜ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.加下划线ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.加删除线ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.恢复默认ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.清除颜色ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.段落格式ToolStripButton = New System.Windows.Forms.ToolStripSplitButton()
        Me.自动换行ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.居中对齐ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.左对齐ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.右对齐ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparatorD1 = New System.Windows.Forms.ToolStripSeparator()
        Me.作为上标ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.作为下标ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparatorD2 = New System.Windows.Forms.ToolStripSeparator()
        Me.首行缩进ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.项目符号ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.格式刷ToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.清除格式ToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparatorT4 = New System.Windows.Forms.ToolStripSeparator()
        Me.帮助LToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.MenuStrip_MyNote = New System.Windows.Forms.MenuStrip()
        Me.文件FToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.新建NToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.打开OToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparatorF1 = New System.Windows.Forms.ToolStripSeparator()
        Me.保存SToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.另存为AToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparatorF2 = New System.Windows.Forms.ToolStripSeparator()
        Me.导出EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.批量导入ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparatorF3 = New System.Windows.Forms.ToolStripSeparator()
        Me.退出XToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.编辑EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.撤消UToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.重做RToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparatorE1 = New System.Windows.Forms.ToolStripSeparator()
        Me.剪切TToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.复制CToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.粘贴PToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.全选AToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparatorE2 = New System.Windows.Forms.ToolStripSeparator()
        Me.查找FToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.替换HToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.插入IToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.功能TToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.批量编辑MToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.便捷输入EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.桌面便签NToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MD5加密SToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.转换编码RToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.辅助ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.计算器CToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.命令行MToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.讲述人TToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.放大镜RToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.屏幕键盘KToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.语音识别YToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.截图工具PToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.阅读这节SToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.语法高亮HToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.系统SToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.修改密码XToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.清除密码QToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparatorS1 = New System.Windows.Forms.ToolStripSeparator()
        Me.压缩数据CToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparatorS3 = New System.Windows.Forms.ToolStripSeparator()
        Me.检查更新NToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.问题反馈AToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.使用评价PToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Status_ConText = New System.Windows.Forms.StatusStrip()
        Me.总字数ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.占位符ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.内容ToolStripProgressBar = New System.Windows.Forms.ToolStripProgressBar()
        Me.上一节ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.展开节点ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.下一节ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Timer_Author = New System.Windows.Forms.Timer(Me.components)
        Me.清除密码CM = New System.Windows.Forms.ToolStripMenuItem()
        Me.加密记录JM = New System.Windows.Forms.ToolStripMenuItem()
        Me.Timer_AutoSave = New System.Windows.Forms.Timer(Me.components)
        Me.Context_Notify = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.Notify_联系作者 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Notify_Separator = New System.Windows.Forms.ToolStripSeparator()
        Me.Notify_保持置顶 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Notify_隐藏窗口 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Notify_退出程序 = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotifyIcon_Key = New System.Windows.Forms.NotifyIcon(Me.components)
        CType(Me.FormBoder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FormBoder.Panel1.SuspendLayout()
        Me.FormBoder.Panel2.SuspendLayout()
        Me.FormBoder.SuspendLayout()
        Me.Tab_Tree.SuspendLayout()
        Me.Tab_CCata.SuspendLayout()
        Me.ContextMenuStrip_TreeCata.SuspendLayout()
        Me.Tab_CSech.SuspendLayout()
        Me.Tab_CHsty.SuspendLayout()
        Me.Status_Tree.SuspendLayout()
        CType(Me.ConBoder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConBoder.Panel1.SuspendLayout()
        Me.ConBoder.Panel2.SuspendLayout()
        Me.ConBoder.SuspendLayout()
        Me.ContextMenuStrip_Context.SuspendLayout()
        CType(Me.PictureNode, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip_Album.SuspendLayout()
        Me.ToolStrip_ConText.SuspendLayout()
        Me.MenuStrip_MyNote.SuspendLayout()
        Me.Status_ConText.SuspendLayout()
        Me.Context_Notify.SuspendLayout()
        Me.SuspendLayout()
        '
        'FormBoder
        '
        Me.FormBoder.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FormBoder.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.FormBoder.Location = New System.Drawing.Point(0, 0)
        Me.FormBoder.Name = "FormBoder"
        '
        'FormBoder.Panel1
        '
        Me.FormBoder.Panel1.Controls.Add(Me.Tab_Tree)
        Me.FormBoder.Panel1.Controls.Add(Me.Status_Tree)
        '
        'FormBoder.Panel2
        '
        Me.FormBoder.Panel2.Controls.Add(Me.ConBoder)
        Me.FormBoder.Panel2.Controls.Add(Me.ToolStrip_ConText)
        Me.FormBoder.Panel2.Controls.Add(Me.MenuStrip_MyNote)
        Me.FormBoder.Panel2.Controls.Add(Me.Status_ConText)
        Me.FormBoder.Size = New System.Drawing.Size(784, 561)
        Me.FormBoder.SplitterDistance = 188
        Me.FormBoder.TabIndex = 2
        '
        'Tab_Tree
        '
        Me.Tab_Tree.Controls.Add(Me.Tab_CCata)
        Me.Tab_Tree.Controls.Add(Me.Tab_CSech)
        Me.Tab_Tree.Controls.Add(Me.Tab_CHsty)
        Me.Tab_Tree.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tab_Tree.Location = New System.Drawing.Point(0, 0)
        Me.Tab_Tree.Name = "Tab_Tree"
        Me.Tab_Tree.SelectedIndex = 0
        Me.Tab_Tree.Size = New System.Drawing.Size(188, 541)
        Me.Tab_Tree.TabIndex = 2
        '
        'Tab_CCata
        '
        Me.Tab_CCata.Controls.Add(Me.TreeView_Cata)
        Me.Tab_CCata.Controls.Add(Me.Com_DB)
        Me.Tab_CCata.Location = New System.Drawing.Point(4, 22)
        Me.Tab_CCata.Name = "Tab_CCata"
        Me.Tab_CCata.Padding = New System.Windows.Forms.Padding(3)
        Me.Tab_CCata.Size = New System.Drawing.Size(180, 515)
        Me.Tab_CCata.TabIndex = 0
        Me.Tab_CCata.Text = "目录"
        Me.Tab_CCata.UseVisualStyleBackColor = True
        '
        'TreeView_Cata
        '
        Me.TreeView_Cata.AllowDrop = True
        Me.TreeView_Cata.ContextMenuStrip = Me.ContextMenuStrip_TreeCata
        Me.TreeView_Cata.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeView_Cata.HideSelection = False
        Me.TreeView_Cata.ImageIndex = 0
        Me.TreeView_Cata.ImageList = Me.ImageList_TreeIcon
        Me.TreeView_Cata.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.TreeView_Cata.LabelEdit = True
        Me.TreeView_Cata.Location = New System.Drawing.Point(3, 23)
        Me.TreeView_Cata.Name = "TreeView_Cata"
        TreeNode1.ImageIndex = 8
        TreeNode1.Name = "烂笔头"
        TreeNode1.SelectedImageIndex = 8
        TreeNode1.Text = "烂笔头"
        Me.TreeView_Cata.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode1})
        Me.TreeView_Cata.SelectedImageIndex = 1
        Me.TreeView_Cata.ShowNodeToolTips = True
        Me.TreeView_Cata.Size = New System.Drawing.Size(174, 489)
        Me.TreeView_Cata.TabIndex = 0
        '
        'ContextMenuStrip_TreeCata
        '
        Me.ContextMenuStrip_TreeCata.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.ContextMenuStrip_TreeCata.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.新建记录NToolStripMenuItem, Me.新建目录NToolStripMenuItem, Me.删除记录DToolStripMenuItem, Me.分割记录FToolStripMenuItem, Me.合并子级EToolStripMenuItem, Me.添加照片AToolStripMenuItem, Me.清除照片DToolStripMenuItem, Me.其他功能RToolStripMenuItem})
        Me.ContextMenuStrip_TreeCata.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip_TreeCata.Size = New System.Drawing.Size(137, 180)
        '
        '新建记录NToolStripMenuItem
        '
        Me.新建记录NToolStripMenuItem.Name = "新建记录NToolStripMenuItem"
        Me.新建记录NToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.新建记录NToolStripMenuItem.Text = "新建记录(&N)"
        '
        '新建目录NToolStripMenuItem
        '
        Me.新建目录NToolStripMenuItem.Name = "新建目录NToolStripMenuItem"
        Me.新建目录NToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.新建目录NToolStripMenuItem.Text = "新建目录(&M)"
        '
        '删除记录DToolStripMenuItem
        '
        Me.删除记录DToolStripMenuItem.Name = "删除记录DToolStripMenuItem"
        Me.删除记录DToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Delete), System.Windows.Forms.Keys)
        Me.删除记录DToolStripMenuItem.ShowShortcutKeys = False
        Me.删除记录DToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.删除记录DToolStripMenuItem.Text = "删除记录(&D)"
        '
        '分割记录FToolStripMenuItem
        '
        Me.分割记录FToolStripMenuItem.Name = "分割记录FToolStripMenuItem"
        Me.分割记录FToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.分割记录FToolStripMenuItem.Text = "分割记录(&F)"
        '
        '合并子级EToolStripMenuItem
        '
        Me.合并子级EToolStripMenuItem.Name = "合并子级EToolStripMenuItem"
        Me.合并子级EToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.合并子级EToolStripMenuItem.Text = "合并子级(&E)"
        '
        '添加照片AToolStripMenuItem
        '
        Me.添加照片AToolStripMenuItem.Name = "添加照片AToolStripMenuItem"
        Me.添加照片AToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.添加照片AToolStripMenuItem.Text = "添加照片(&P)"
        '
        '清除照片DToolStripMenuItem
        '
        Me.清除照片DToolStripMenuItem.Name = "清除照片DToolStripMenuItem"
        Me.清除照片DToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.清除照片DToolStripMenuItem.Text = "清除照片(&C)"
        '
        '其他功能RToolStripMenuItem
        '
        Me.其他功能RToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.改变图标TToolStripMenuItem, Me.导出图库AToolStripMenuItem})
        Me.其他功能RToolStripMenuItem.Name = "其他功能RToolStripMenuItem"
        Me.其他功能RToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.其他功能RToolStripMenuItem.Text = "其他功能(&R)"
        '
        '改变图标TToolStripMenuItem
        '
        Me.改变图标TToolStripMenuItem.Name = "改变图标TToolStripMenuItem"
        Me.改变图标TToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.改变图标TToolStripMenuItem.Text = "改变图标(&B)"
        '
        '导出图库AToolStripMenuItem
        '
        Me.导出图库AToolStripMenuItem.Name = "导出图库AToolStripMenuItem"
        Me.导出图库AToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.导出图库AToolStripMenuItem.Text = "导出图库(&A)"
        '
        'ImageList_TreeIcon
        '
        Me.ImageList_TreeIcon.ImageStream = CType(resources.GetObject("ImageList_TreeIcon.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList_TreeIcon.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList_TreeIcon.Images.SetKeyName(0, "tree_folder.png")
        Me.ImageList_TreeIcon.Images.SetKeyName(1, "tree_folder_open.png")
        Me.ImageList_TreeIcon.Images.SetKeyName(2, "tree_mydoc.png")
        Me.ImageList_TreeIcon.Images.SetKeyName(3, "tree_mymusic.png")
        Me.ImageList_TreeIcon.Images.SetKeyName(4, "tree_mypic.png")
        Me.ImageList_TreeIcon.Images.SetKeyName(5, "tree_mydoc.png")
        Me.ImageList_TreeIcon.Images.SetKeyName(6, "tree_mymusic.png")
        Me.ImageList_TreeIcon.Images.SetKeyName(7, "tree_mypic.png")
        Me.ImageList_TreeIcon.Images.SetKeyName(8, "tree_mycomputer.png")
        Me.ImageList_TreeIcon.Images.SetKeyName(9, "tree_mydesk.png")
        Me.ImageList_TreeIcon.Images.SetKeyName(10, "tree_mydoc.png")
        Me.ImageList_TreeIcon.Images.SetKeyName(11, "Lock.png")
        '
        'Com_DB
        '
        Me.Com_DB.Dock = System.Windows.Forms.DockStyle.Top
        Me.Com_DB.FormattingEnabled = True
        Me.Com_DB.Location = New System.Drawing.Point(3, 3)
        Me.Com_DB.Name = "Com_DB"
        Me.Com_DB.Size = New System.Drawing.Size(174, 20)
        Me.Com_DB.TabIndex = 1
        '
        'Tab_CSech
        '
        Me.Tab_CSech.Controls.Add(Me.TreeView_Seach)
        Me.Tab_CSech.Controls.Add(Me.TextBox_FindText)
        Me.Tab_CSech.Location = New System.Drawing.Point(4, 22)
        Me.Tab_CSech.Name = "Tab_CSech"
        Me.Tab_CSech.Padding = New System.Windows.Forms.Padding(3)
        Me.Tab_CSech.Size = New System.Drawing.Size(180, 514)
        Me.Tab_CSech.TabIndex = 1
        Me.Tab_CSech.Text = "搜索"
        Me.Tab_CSech.UseVisualStyleBackColor = True
        '
        'TreeView_Seach
        '
        Me.TreeView_Seach.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeView_Seach.HideSelection = False
        Me.TreeView_Seach.ImageIndex = 0
        Me.TreeView_Seach.ImageList = Me.ImageList_TreeIcon
        Me.TreeView_Seach.Location = New System.Drawing.Point(3, 24)
        Me.TreeView_Seach.Name = "TreeView_Seach"
        TreeNode2.ImageIndex = 9
        TreeNode2.Name = "搜索"
        TreeNode2.SelectedImageIndex = 9
        TreeNode2.Text = "搜索"
        Me.TreeView_Seach.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode2})
        Me.TreeView_Seach.SelectedImageIndex = 0
        Me.TreeView_Seach.Size = New System.Drawing.Size(174, 487)
        Me.TreeView_Seach.TabIndex = 3
        '
        'TextBox_FindText
        '
        Me.TextBox_FindText.Dock = System.Windows.Forms.DockStyle.Top
        Me.TextBox_FindText.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.TextBox_FindText.Location = New System.Drawing.Point(3, 3)
        Me.TextBox_FindText.Name = "TextBox_FindText"
        Me.TextBox_FindText.Size = New System.Drawing.Size(174, 21)
        Me.TextBox_FindText.TabIndex = 13
        Me.TextBox_FindText.Text = "键入您要搜索的内容："
        '
        'Tab_CHsty
        '
        Me.Tab_CHsty.Controls.Add(Me.TreeView_Mark)
        Me.Tab_CHsty.Location = New System.Drawing.Point(4, 22)
        Me.Tab_CHsty.Name = "Tab_CHsty"
        Me.Tab_CHsty.Padding = New System.Windows.Forms.Padding(3)
        Me.Tab_CHsty.Size = New System.Drawing.Size(180, 514)
        Me.Tab_CHsty.TabIndex = 2
        Me.Tab_CHsty.Text = "历史"
        Me.Tab_CHsty.UseVisualStyleBackColor = True
        '
        'TreeView_Mark
        '
        Me.TreeView_Mark.AllowDrop = True
        Me.TreeView_Mark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeView_Mark.HideSelection = False
        Me.TreeView_Mark.ImageIndex = 0
        Me.TreeView_Mark.ImageList = Me.ImageList_TreeIcon
        Me.TreeView_Mark.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.TreeView_Mark.LabelEdit = True
        Me.TreeView_Mark.Location = New System.Drawing.Point(3, 3)
        Me.TreeView_Mark.Name = "TreeView_Mark"
        TreeNode3.Name = "NULL"
        TreeNode3.Text = "NULL"
        TreeNode4.ImageIndex = 10
        TreeNode4.Name = "历史"
        TreeNode4.SelectedImageIndex = 10
        TreeNode4.Text = "历史"
        Me.TreeView_Mark.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode4})
        Me.TreeView_Mark.SelectedImageIndex = 0
        Me.TreeView_Mark.Size = New System.Drawing.Size(174, 508)
        Me.TreeView_Mark.TabIndex = 1
        '
        'Status_Tree
        '
        Me.Status_Tree.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.当前ToolStripStatusLabel, Me.进度ToolStripProgressBar})
        Me.Status_Tree.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me.Status_Tree.Location = New System.Drawing.Point(0, 541)
        Me.Status_Tree.Name = "Status_Tree"
        Me.Status_Tree.Size = New System.Drawing.Size(188, 20)
        Me.Status_Tree.SizingGrip = False
        Me.Status_Tree.TabIndex = 0
        Me.Status_Tree.Text = "状态栏"
        '
        '当前ToolStripStatusLabel
        '
        Me.当前ToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.当前ToolStripStatusLabel.Name = "当前ToolStripStatusLabel"
        Me.当前ToolStripStatusLabel.Size = New System.Drawing.Size(34, 15)
        Me.当前ToolStripStatusLabel.Text = "当前:"
        '
        '进度ToolStripProgressBar
        '
        Me.进度ToolStripProgressBar.AutoSize = False
        Me.进度ToolStripProgressBar.Name = "进度ToolStripProgressBar"
        Me.进度ToolStripProgressBar.Size = New System.Drawing.Size(80, 15)
        Me.进度ToolStripProgressBar.ToolTipText = "正在处理子节点......"
        Me.进度ToolStripProgressBar.Visible = False
        '
        'ConBoder
        '
        Me.ConBoder.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ConBoder.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.ConBoder.Location = New System.Drawing.Point(0, 49)
        Me.ConBoder.Name = "ConBoder"
        Me.ConBoder.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'ConBoder.Panel1
        '
        Me.ConBoder.Panel1.Controls.Add(Me.ConText)
        Me.ConBoder.Panel1MinSize = 0
        '
        'ConBoder.Panel2
        '
        Me.ConBoder.Panel2.Controls.Add(Me.PictureNode)
        Me.ConBoder.Panel2MinSize = 0
        Me.ConBoder.Size = New System.Drawing.Size(592, 490)
        Me.ConBoder.SplitterDistance = 346
        Me.ConBoder.TabIndex = 4
        '
        'ConText
        '
        Me.ConText.ContextMenuStrip = Me.ContextMenuStrip_Context
        Me.ConText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.ConText.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ConText.EnableAutoDragDrop = True
        Me.ConText.HideSelection = False
        Me.ConText.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.ConText.Location = New System.Drawing.Point(0, 0)
        Me.ConText.Name = "ConText"
        Me.ConText.Size = New System.Drawing.Size(592, 346)
        Me.ConText.TabIndex = 0
        Me.ConText.Text = ""
        '
        'ContextMenuStrip_Context
        '
        Me.ContextMenuStrip_Context.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.ContextMenuStrip_Context.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.已阅YCToolStripMenuItem, Me.撤销UCToolStripMenuItem, Me.ToolStripSeparatorC1, Me.剪切TCToolStripMenuItem, Me.复制CCToolStripMenuItem, Me.粘贴PCToolStripMenuItem, Me.粘贴文本NCToolStripMenuItem, Me.删除DCToolStripMenuItem, Me.ToolStripSeparatorC2, Me.插入文本ICToolStripMenuItem, Me.ToolStripSeparatorC3, Me.全选ACToolStripMenuItem, Me.查找FCToolStripMenuItem, Me.替换HCToolStripMenuItem, Me.ToolStripSeparatorC4, Me.段落格式GCToolStripMenuItem, Me.字体格式WCToolStripMenuItem})
        Me.ContextMenuStrip_Context.Name = "ContextMenuStrip2"
        Me.ContextMenuStrip_Context.Size = New System.Drawing.Size(137, 314)
        '
        '已阅YCToolStripMenuItem
        '
        Me.已阅YCToolStripMenuItem.Name = "已阅YCToolStripMenuItem"
        Me.已阅YCToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.已阅YCToolStripMenuItem.Text = "已阅(&Y)"
        '
        '撤销UCToolStripMenuItem
        '
        Me.撤销UCToolStripMenuItem.Name = "撤销UCToolStripMenuItem"
        Me.撤销UCToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.撤销UCToolStripMenuItem.Text = "撤销(&U)"
        '
        'ToolStripSeparatorC1
        '
        Me.ToolStripSeparatorC1.Name = "ToolStripSeparatorC1"
        Me.ToolStripSeparatorC1.Size = New System.Drawing.Size(133, 6)
        '
        '剪切TCToolStripMenuItem
        '
        Me.剪切TCToolStripMenuItem.Name = "剪切TCToolStripMenuItem"
        Me.剪切TCToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.剪切TCToolStripMenuItem.Text = "剪切(&T)"
        '
        '复制CCToolStripMenuItem
        '
        Me.复制CCToolStripMenuItem.Name = "复制CCToolStripMenuItem"
        Me.复制CCToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.复制CCToolStripMenuItem.Text = "复制(&C)"
        '
        '粘贴PCToolStripMenuItem
        '
        Me.粘贴PCToolStripMenuItem.Name = "粘贴PCToolStripMenuItem"
        Me.粘贴PCToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.粘贴PCToolStripMenuItem.Text = "粘贴(&P)"
        '
        '粘贴文本NCToolStripMenuItem
        '
        Me.粘贴文本NCToolStripMenuItem.Name = "粘贴文本NCToolStripMenuItem"
        Me.粘贴文本NCToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.粘贴文本NCToolStripMenuItem.Text = "粘贴文本(&N)"
        '
        '删除DCToolStripMenuItem
        '
        Me.删除DCToolStripMenuItem.Name = "删除DCToolStripMenuItem"
        Me.删除DCToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.删除DCToolStripMenuItem.Text = "删除(&D)"
        '
        'ToolStripSeparatorC2
        '
        Me.ToolStripSeparatorC2.Name = "ToolStripSeparatorC2"
        Me.ToolStripSeparatorC2.Size = New System.Drawing.Size(133, 6)
        '
        '插入文本ICToolStripMenuItem
        '
        Me.插入文本ICToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.符号ToolStripMenuItem, Me.时间ToolStripMenuItem, Me.日期ToolStripMenuItem, Me.日期时间ToolStripMenuItem, Me.自定义图片ToolStripMenuItem})
        Me.插入文本ICToolStripMenuItem.Name = "插入文本ICToolStripMenuItem"
        Me.插入文本ICToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.插入文本ICToolStripMenuItem.Text = "插入文本(&I)"
        '
        '符号ToolStripMenuItem
        '
        Me.符号ToolStripMenuItem.Name = "符号ToolStripMenuItem"
        Me.符号ToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.符号ToolStripMenuItem.Text = "符号(!@#$%^*...)"
        '
        '时间ToolStripMenuItem
        '
        Me.时间ToolStripMenuItem.Name = "时间ToolStripMenuItem"
        Me.时间ToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.时间ToolStripMenuItem.Text = "时间(05:02)"
        '
        '日期ToolStripMenuItem
        '
        Me.日期ToolStripMenuItem.Name = "日期ToolStripMenuItem"
        Me.日期ToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.日期ToolStripMenuItem.Text = "日期(13-05-17)"
        '
        '日期时间ToolStripMenuItem
        '
        Me.日期时间ToolStripMenuItem.Name = "日期时间ToolStripMenuItem"
        Me.日期时间ToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.日期时间ToolStripMenuItem.Text = "日期时间"
        '
        '自定义图片ToolStripMenuItem
        '
        Me.自定义图片ToolStripMenuItem.Name = "自定义图片ToolStripMenuItem"
        Me.自定义图片ToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.自定义图片ToolStripMenuItem.Text = "自定义图片"
        '
        'ToolStripSeparatorC3
        '
        Me.ToolStripSeparatorC3.Name = "ToolStripSeparatorC3"
        Me.ToolStripSeparatorC3.Size = New System.Drawing.Size(133, 6)
        '
        '全选ACToolStripMenuItem
        '
        Me.全选ACToolStripMenuItem.Name = "全选ACToolStripMenuItem"
        Me.全选ACToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.全选ACToolStripMenuItem.Text = "全选(&A)"
        '
        '查找FCToolStripMenuItem
        '
        Me.查找FCToolStripMenuItem.Name = "查找FCToolStripMenuItem"
        Me.查找FCToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.查找FCToolStripMenuItem.ShowShortcutKeys = False
        Me.查找FCToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.查找FCToolStripMenuItem.Text = "查找(&F)"
        '
        '替换HCToolStripMenuItem
        '
        Me.替换HCToolStripMenuItem.Name = "替换HCToolStripMenuItem"
        Me.替换HCToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.H), System.Windows.Forms.Keys)
        Me.替换HCToolStripMenuItem.ShowShortcutKeys = False
        Me.替换HCToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.替换HCToolStripMenuItem.Text = "替换(&H)"
        '
        'ToolStripSeparatorC4
        '
        Me.ToolStripSeparatorC4.Name = "ToolStripSeparatorC4"
        Me.ToolStripSeparatorC4.Size = New System.Drawing.Size(133, 6)
        '
        '段落格式GCToolStripMenuItem
        '
        Me.段落格式GCToolStripMenuItem.Name = "段落格式GCToolStripMenuItem"
        Me.段落格式GCToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.段落格式GCToolStripMenuItem.ShowShortcutKeys = False
        Me.段落格式GCToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.段落格式GCToolStripMenuItem.Text = "段落格式(&G)"
        '
        '字体格式WCToolStripMenuItem
        '
        Me.字体格式WCToolStripMenuItem.Name = "字体格式WCToolStripMenuItem"
        Me.字体格式WCToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.字体格式WCToolStripMenuItem.ShowShortcutKeys = False
        Me.字体格式WCToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.字体格式WCToolStripMenuItem.Text = "字体格式(&W)"
        '
        'PictureNode
        '
        Me.PictureNode.BackColor = System.Drawing.SystemColors.Window
        Me.PictureNode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureNode.ContextMenuStrip = Me.ContextMenuStrip_Album
        Me.PictureNode.Location = New System.Drawing.Point(2, 2)
        Me.PictureNode.Name = "PictureNode"
        Me.PictureNode.Size = New System.Drawing.Size(587, 136)
        Me.PictureNode.TabIndex = 0
        Me.PictureNode.TabStop = False
        '
        'ContextMenuStrip_Album
        '
        Me.ContextMenuStrip_Album.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.ContextMenuStrip_Album.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.自动缩放ZToolStripMenuItem, Me.添加图片TToolStripMenuItem, Me.导出图片EToolStripMenuItem, Me.全部导出AToolStripMenuItem, Me.删除图片DToolStripMenuItem, Me.清空图片CToolStripMenuItem})
        Me.ContextMenuStrip_Album.Name = "CM_Pic"
        Me.ContextMenuStrip_Album.Size = New System.Drawing.Size(137, 136)
        '
        '自动缩放ZToolStripMenuItem
        '
        Me.自动缩放ZToolStripMenuItem.Checked = True
        Me.自动缩放ZToolStripMenuItem.CheckOnClick = True
        Me.自动缩放ZToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.自动缩放ZToolStripMenuItem.Name = "自动缩放ZToolStripMenuItem"
        Me.自动缩放ZToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.自动缩放ZToolStripMenuItem.Text = "自动缩放(&Z)"
        '
        '添加图片TToolStripMenuItem
        '
        Me.添加图片TToolStripMenuItem.Name = "添加图片TToolStripMenuItem"
        Me.添加图片TToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.添加图片TToolStripMenuItem.Text = "添加图片(&T)"
        '
        '导出图片EToolStripMenuItem
        '
        Me.导出图片EToolStripMenuItem.Name = "导出图片EToolStripMenuItem"
        Me.导出图片EToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.导出图片EToolStripMenuItem.Text = "导出图片(&E)"
        '
        '全部导出AToolStripMenuItem
        '
        Me.全部导出AToolStripMenuItem.Name = "全部导出AToolStripMenuItem"
        Me.全部导出AToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.全部导出AToolStripMenuItem.Text = "全部导出(&A)"
        '
        '删除图片DToolStripMenuItem
        '
        Me.删除图片DToolStripMenuItem.Name = "删除图片DToolStripMenuItem"
        Me.删除图片DToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.删除图片DToolStripMenuItem.Text = "删除图片(&D)"
        '
        '清空图片CToolStripMenuItem
        '
        Me.清空图片CToolStripMenuItem.Name = "清空图片CToolStripMenuItem"
        Me.清空图片CToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.清空图片CToolStripMenuItem.Text = "清空图片(&C)"
        '
        'ToolStrip_ConText
        '
        Me.ToolStrip_ConText.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.新建NToolStripButton, Me.打开OToolStripButton, Me.保存SToolStripButton, Me.ToolStripSeparatorT1, Me.撤销UToolStripButton, Me.重做RToolStripButton, Me.ToolStripSeparatorT2, Me.剪切UToolStripButton, Me.复制CToolStripButton, Me.粘贴PToolStripButton, Me.ToolStripSeparatorT3, Me.格式字体ToolStripButton, Me.段落格式ToolStripButton, Me.格式刷ToolStripButton, Me.清除格式ToolStripButton, Me.ToolStripSeparatorT4, Me.帮助LToolStripButton})
        Me.ToolStrip_ConText.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip_ConText.Name = "ToolStrip_ConText"
        Me.ToolStrip_ConText.Size = New System.Drawing.Size(592, 25)
        Me.ToolStrip_ConText.TabIndex = 1
        '
        '新建NToolStripButton
        '
        Me.新建NToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.新建NToolStripButton.Image = CType(resources.GetObject("新建NToolStripButton.Image"), System.Drawing.Image)
        Me.新建NToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.新建NToolStripButton.Name = "新建NToolStripButton"
        Me.新建NToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.新建NToolStripButton.Text = "新建"
        Me.新建NToolStripButton.ToolTipText = "新建"
        '
        '打开OToolStripButton
        '
        Me.打开OToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.打开OToolStripButton.Image = CType(resources.GetObject("打开OToolStripButton.Image"), System.Drawing.Image)
        Me.打开OToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.打开OToolStripButton.Name = "打开OToolStripButton"
        Me.打开OToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.打开OToolStripButton.Text = "打开"
        '
        '保存SToolStripButton
        '
        Me.保存SToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.保存SToolStripButton.Image = CType(resources.GetObject("保存SToolStripButton.Image"), System.Drawing.Image)
        Me.保存SToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.保存SToolStripButton.Name = "保存SToolStripButton"
        Me.保存SToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.保存SToolStripButton.Text = "保存"
        '
        'ToolStripSeparatorT1
        '
        Me.ToolStripSeparatorT1.Name = "ToolStripSeparatorT1"
        Me.ToolStripSeparatorT1.Size = New System.Drawing.Size(6, 25)
        '
        '撤销UToolStripButton
        '
        Me.撤销UToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.撤销UToolStripButton.Image = CType(resources.GetObject("撤销UToolStripButton.Image"), System.Drawing.Image)
        Me.撤销UToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.撤销UToolStripButton.Name = "撤销UToolStripButton"
        Me.撤销UToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.撤销UToolStripButton.Text = "撤销"
        '
        '重做RToolStripButton
        '
        Me.重做RToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.重做RToolStripButton.Image = CType(resources.GetObject("重做RToolStripButton.Image"), System.Drawing.Image)
        Me.重做RToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.重做RToolStripButton.Name = "重做RToolStripButton"
        Me.重做RToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.重做RToolStripButton.Text = "重做"
        Me.重做RToolStripButton.ToolTipText = "重做"
        '
        'ToolStripSeparatorT2
        '
        Me.ToolStripSeparatorT2.Name = "ToolStripSeparatorT2"
        Me.ToolStripSeparatorT2.Size = New System.Drawing.Size(6, 25)
        '
        '剪切UToolStripButton
        '
        Me.剪切UToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.剪切UToolStripButton.Image = CType(resources.GetObject("剪切UToolStripButton.Image"), System.Drawing.Image)
        Me.剪切UToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.剪切UToolStripButton.Name = "剪切UToolStripButton"
        Me.剪切UToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.剪切UToolStripButton.Text = "剪切"
        '
        '复制CToolStripButton
        '
        Me.复制CToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.复制CToolStripButton.Image = CType(resources.GetObject("复制CToolStripButton.Image"), System.Drawing.Image)
        Me.复制CToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.复制CToolStripButton.Name = "复制CToolStripButton"
        Me.复制CToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.复制CToolStripButton.Text = "复制"
        '
        '粘贴PToolStripButton
        '
        Me.粘贴PToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.粘贴PToolStripButton.Image = CType(resources.GetObject("粘贴PToolStripButton.Image"), System.Drawing.Image)
        Me.粘贴PToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.粘贴PToolStripButton.Name = "粘贴PToolStripButton"
        Me.粘贴PToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.粘贴PToolStripButton.Text = "粘贴"
        '
        'ToolStripSeparatorT3
        '
        Me.ToolStripSeparatorT3.Name = "ToolStripSeparatorT3"
        Me.ToolStripSeparatorT3.Size = New System.Drawing.Size(6, 25)
        '
        '格式字体ToolStripButton
        '
        Me.格式字体ToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.格式字体ToolStripButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.加粗字体ToolStripMenuItem, Me.斜体字体ToolStripMenuItem, Me.加粗倾斜ToolStripMenuItem, Me.加下划线ToolStripMenuItem, Me.加删除线ToolStripMenuItem, Me.恢复默认ToolStripMenuItem, Me.清除颜色ToolStripMenuItem})
        Me.格式字体ToolStripButton.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.格式字体ToolStripButton.Image = CType(resources.GetObject("格式字体ToolStripButton.Image"), System.Drawing.Image)
        Me.格式字体ToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.格式字体ToolStripButton.Name = "格式字体ToolStripButton"
        Me.格式字体ToolStripButton.Size = New System.Drawing.Size(32, 22)
        Me.格式字体ToolStripButton.Text = "格式字体"
        '
        '加粗字体ToolStripMenuItem
        '
        Me.加粗字体ToolStripMenuItem.Name = "加粗字体ToolStripMenuItem"
        Me.加粗字体ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.加粗字体ToolStripMenuItem.Text = "加粗字体"
        '
        '斜体字体ToolStripMenuItem
        '
        Me.斜体字体ToolStripMenuItem.Name = "斜体字体ToolStripMenuItem"
        Me.斜体字体ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.斜体字体ToolStripMenuItem.Text = "倾斜字体"
        '
        '加粗倾斜ToolStripMenuItem
        '
        Me.加粗倾斜ToolStripMenuItem.Name = "加粗倾斜ToolStripMenuItem"
        Me.加粗倾斜ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.加粗倾斜ToolStripMenuItem.Text = "加粗倾斜"
        '
        '加下划线ToolStripMenuItem
        '
        Me.加下划线ToolStripMenuItem.Name = "加下划线ToolStripMenuItem"
        Me.加下划线ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.加下划线ToolStripMenuItem.Text = "加下划线"
        '
        '加删除线ToolStripMenuItem
        '
        Me.加删除线ToolStripMenuItem.Name = "加删除线ToolStripMenuItem"
        Me.加删除线ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.加删除线ToolStripMenuItem.Text = "加删除线"
        '
        '恢复默认ToolStripMenuItem
        '
        Me.恢复默认ToolStripMenuItem.Name = "恢复默认ToolStripMenuItem"
        Me.恢复默认ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.恢复默认ToolStripMenuItem.Text = "恢复默认"
        '
        '清除颜色ToolStripMenuItem
        '
        Me.清除颜色ToolStripMenuItem.Name = "清除颜色ToolStripMenuItem"
        Me.清除颜色ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.清除颜色ToolStripMenuItem.Text = "清除颜色"
        '
        '段落格式ToolStripButton
        '
        Me.段落格式ToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.段落格式ToolStripButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.自动换行ToolStripMenuItem, Me.居中对齐ToolStripMenuItem, Me.左对齐ToolStripMenuItem, Me.右对齐ToolStripMenuItem, Me.ToolStripSeparatorD1, Me.作为上标ToolStripMenuItem, Me.作为下标ToolStripMenuItem, Me.ToolStripSeparatorD2, Me.首行缩进ToolStripMenuItem, Me.项目符号ToolStripMenuItem})
        Me.段落格式ToolStripButton.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.段落格式ToolStripButton.Image = CType(resources.GetObject("段落格式ToolStripButton.Image"), System.Drawing.Image)
        Me.段落格式ToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.段落格式ToolStripButton.Name = "段落格式ToolStripButton"
        Me.段落格式ToolStripButton.Size = New System.Drawing.Size(32, 22)
        Me.段落格式ToolStripButton.Text = "段落格式"
        '
        '自动换行ToolStripMenuItem
        '
        Me.自动换行ToolStripMenuItem.Checked = True
        Me.自动换行ToolStripMenuItem.CheckOnClick = True
        Me.自动换行ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.自动换行ToolStripMenuItem.Name = "自动换行ToolStripMenuItem"
        Me.自动换行ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.自动换行ToolStripMenuItem.Text = "自动换行"
        '
        '居中对齐ToolStripMenuItem
        '
        Me.居中对齐ToolStripMenuItem.Name = "居中对齐ToolStripMenuItem"
        Me.居中对齐ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.居中对齐ToolStripMenuItem.Text = "居中对齐"
        '
        '左对齐ToolStripMenuItem
        '
        Me.左对齐ToolStripMenuItem.Name = "左对齐ToolStripMenuItem"
        Me.左对齐ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.左对齐ToolStripMenuItem.Text = "左对齐"
        '
        '右对齐ToolStripMenuItem
        '
        Me.右对齐ToolStripMenuItem.Name = "右对齐ToolStripMenuItem"
        Me.右对齐ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.右对齐ToolStripMenuItem.Text = "右对齐"
        '
        'ToolStripSeparatorD1
        '
        Me.ToolStripSeparatorD1.Name = "ToolStripSeparatorD1"
        Me.ToolStripSeparatorD1.Size = New System.Drawing.Size(115, 6)
        '
        '作为上标ToolStripMenuItem
        '
        Me.作为上标ToolStripMenuItem.Name = "作为上标ToolStripMenuItem"
        Me.作为上标ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.作为上标ToolStripMenuItem.Text = "作为上标"
        '
        '作为下标ToolStripMenuItem
        '
        Me.作为下标ToolStripMenuItem.Name = "作为下标ToolStripMenuItem"
        Me.作为下标ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.作为下标ToolStripMenuItem.Text = "作为下标"
        '
        'ToolStripSeparatorD2
        '
        Me.ToolStripSeparatorD2.Name = "ToolStripSeparatorD2"
        Me.ToolStripSeparatorD2.Size = New System.Drawing.Size(115, 6)
        '
        '首行缩进ToolStripMenuItem
        '
        Me.首行缩进ToolStripMenuItem.Name = "首行缩进ToolStripMenuItem"
        Me.首行缩进ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.首行缩进ToolStripMenuItem.Text = "首行缩进"
        '
        '项目符号ToolStripMenuItem
        '
        Me.项目符号ToolStripMenuItem.Name = "项目符号ToolStripMenuItem"
        Me.项目符号ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.项目符号ToolStripMenuItem.Text = "项目符号"
        '
        '格式刷ToolStripButton
        '
        Me.格式刷ToolStripButton.CheckOnClick = True
        Me.格式刷ToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.格式刷ToolStripButton.Image = CType(resources.GetObject("格式刷ToolStripButton.Image"), System.Drawing.Image)
        Me.格式刷ToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.格式刷ToolStripButton.Name = "格式刷ToolStripButton"
        Me.格式刷ToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.格式刷ToolStripButton.Text = "格式刷"
        '
        '清除格式ToolStripButton
        '
        Me.清除格式ToolStripButton.CheckOnClick = True
        Me.清除格式ToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.清除格式ToolStripButton.Image = CType(resources.GetObject("清除格式ToolStripButton.Image"), System.Drawing.Image)
        Me.清除格式ToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.清除格式ToolStripButton.Name = "清除格式ToolStripButton"
        Me.清除格式ToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.清除格式ToolStripButton.Text = "清除格式"
        '
        'ToolStripSeparatorT4
        '
        Me.ToolStripSeparatorT4.Name = "ToolStripSeparatorT4"
        Me.ToolStripSeparatorT4.Size = New System.Drawing.Size(6, 25)
        '
        '帮助LToolStripButton
        '
        Me.帮助LToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.帮助LToolStripButton.Image = CType(resources.GetObject("帮助LToolStripButton.Image"), System.Drawing.Image)
        Me.帮助LToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.帮助LToolStripButton.Name = "帮助LToolStripButton"
        Me.帮助LToolStripButton.Size = New System.Drawing.Size(23, 22)
        Me.帮助LToolStripButton.Text = "帮助"
        Me.帮助LToolStripButton.ToolTipText = "帮助"
        '
        'MenuStrip_MyNote
        '
        Me.MenuStrip_MyNote.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.文件FToolStripMenuItem, Me.编辑EToolStripMenuItem, Me.功能TToolStripMenuItem, Me.辅助ToolStripMenuItem, Me.系统SToolStripMenuItem})
        Me.MenuStrip_MyNote.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip_MyNote.Name = "MenuStrip_MyNote"
        Me.MenuStrip_MyNote.Size = New System.Drawing.Size(592, 24)
        Me.MenuStrip_MyNote.TabIndex = 0
        '
        '文件FToolStripMenuItem
        '
        Me.文件FToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.新建NToolStripMenuItem, Me.打开OToolStripMenuItem, Me.ToolStripSeparatorF1, Me.保存SToolStripMenuItem, Me.另存为AToolStripMenuItem, Me.ToolStripSeparatorF2, Me.导出EToolStripMenuItem, Me.批量导入ToolStripMenuItem, Me.ToolStripSeparatorF3, Me.退出XToolStripMenuItem})
        Me.文件FToolStripMenuItem.Name = "文件FToolStripMenuItem"
        Me.文件FToolStripMenuItem.Size = New System.Drawing.Size(57, 20)
        Me.文件FToolStripMenuItem.Text = "文件(&F)"
        '
        '新建NToolStripMenuItem
        '
        Me.新建NToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.新建NToolStripMenuItem.Name = "新建NToolStripMenuItem"
        Me.新建NToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.新建NToolStripMenuItem.ShowShortcutKeys = False
        Me.新建NToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.新建NToolStripMenuItem.Text = "新建(&N)"
        '
        '打开OToolStripMenuItem
        '
        Me.打开OToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.打开OToolStripMenuItem.Name = "打开OToolStripMenuItem"
        Me.打开OToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.打开OToolStripMenuItem.ShowShortcutKeys = False
        Me.打开OToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.打开OToolStripMenuItem.Text = "打开(&O)"
        '
        'ToolStripSeparatorF1
        '
        Me.ToolStripSeparatorF1.Name = "ToolStripSeparatorF1"
        Me.ToolStripSeparatorF1.Size = New System.Drawing.Size(134, 6)
        '
        '保存SToolStripMenuItem
        '
        Me.保存SToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.保存SToolStripMenuItem.Name = "保存SToolStripMenuItem"
        Me.保存SToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.保存SToolStripMenuItem.ShowShortcutKeys = False
        Me.保存SToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.保存SToolStripMenuItem.Text = "保存(&S)"
        '
        '另存为AToolStripMenuItem
        '
        Me.另存为AToolStripMenuItem.Name = "另存为AToolStripMenuItem"
        Me.另存为AToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.另存为AToolStripMenuItem.Text = "另存为(&A)"
        '
        'ToolStripSeparatorF2
        '
        Me.ToolStripSeparatorF2.Name = "ToolStripSeparatorF2"
        Me.ToolStripSeparatorF2.Size = New System.Drawing.Size(134, 6)
        '
        '导出EToolStripMenuItem
        '
        Me.导出EToolStripMenuItem.Name = "导出EToolStripMenuItem"
        Me.导出EToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.导出EToolStripMenuItem.Text = "批量导出(&E)"
        '
        '批量导入ToolStripMenuItem
        '
        Me.批量导入ToolStripMenuItem.Name = "批量导入ToolStripMenuItem"
        Me.批量导入ToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.批量导入ToolStripMenuItem.Text = "批量导入(&I)"
        '
        'ToolStripSeparatorF3
        '
        Me.ToolStripSeparatorF3.Name = "ToolStripSeparatorF3"
        Me.ToolStripSeparatorF3.Size = New System.Drawing.Size(134, 6)
        '
        '退出XToolStripMenuItem
        '
        Me.退出XToolStripMenuItem.Name = "退出XToolStripMenuItem"
        Me.退出XToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.退出XToolStripMenuItem.Text = "退出(&X)"
        '
        '编辑EToolStripMenuItem
        '
        Me.编辑EToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.撤消UToolStripMenuItem, Me.重做RToolStripMenuItem, Me.ToolStripSeparatorE1, Me.剪切TToolStripMenuItem, Me.复制CToolStripMenuItem, Me.粘贴PToolStripMenuItem, Me.全选AToolStripMenuItem, Me.ToolStripSeparatorE2, Me.查找FToolStripMenuItem, Me.替换HToolStripMenuItem, Me.插入IToolStripMenuItem})
        Me.编辑EToolStripMenuItem.Name = "编辑EToolStripMenuItem"
        Me.编辑EToolStripMenuItem.Size = New System.Drawing.Size(58, 20)
        Me.编辑EToolStripMenuItem.Text = "编辑(&E)"
        '
        '撤消UToolStripMenuItem
        '
        Me.撤消UToolStripMenuItem.Name = "撤消UToolStripMenuItem"
        Me.撤消UToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.撤消UToolStripMenuItem.Text = "撤消(&U)"
        '
        '重做RToolStripMenuItem
        '
        Me.重做RToolStripMenuItem.Name = "重做RToolStripMenuItem"
        Me.重做RToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.重做RToolStripMenuItem.Text = "重做(&R)"
        '
        'ToolStripSeparatorE1
        '
        Me.ToolStripSeparatorE1.Name = "ToolStripSeparatorE1"
        Me.ToolStripSeparatorE1.Size = New System.Drawing.Size(112, 6)
        '
        '剪切TToolStripMenuItem
        '
        Me.剪切TToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.剪切TToolStripMenuItem.Name = "剪切TToolStripMenuItem"
        Me.剪切TToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.剪切TToolStripMenuItem.Text = "剪切(&T)"
        '
        '复制CToolStripMenuItem
        '
        Me.复制CToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.复制CToolStripMenuItem.Name = "复制CToolStripMenuItem"
        Me.复制CToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.复制CToolStripMenuItem.Text = "复制(&C)"
        '
        '粘贴PToolStripMenuItem
        '
        Me.粘贴PToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.粘贴PToolStripMenuItem.Name = "粘贴PToolStripMenuItem"
        Me.粘贴PToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.粘贴PToolStripMenuItem.Text = "粘贴(&P)"
        '
        '全选AToolStripMenuItem
        '
        Me.全选AToolStripMenuItem.Name = "全选AToolStripMenuItem"
        Me.全选AToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.全选AToolStripMenuItem.Text = "全选(&A)"
        '
        'ToolStripSeparatorE2
        '
        Me.ToolStripSeparatorE2.Name = "ToolStripSeparatorE2"
        Me.ToolStripSeparatorE2.Size = New System.Drawing.Size(112, 6)
        '
        '查找FToolStripMenuItem
        '
        Me.查找FToolStripMenuItem.Name = "查找FToolStripMenuItem"
        Me.查找FToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.查找FToolStripMenuItem.Text = "查找(&F)"
        '
        '替换HToolStripMenuItem
        '
        Me.替换HToolStripMenuItem.Name = "替换HToolStripMenuItem"
        Me.替换HToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.替换HToolStripMenuItem.Text = "替换(&H)"
        '
        '插入IToolStripMenuItem
        '
        Me.插入IToolStripMenuItem.Name = "插入IToolStripMenuItem"
        Me.插入IToolStripMenuItem.Size = New System.Drawing.Size(115, 22)
        Me.插入IToolStripMenuItem.Text = "插入(&I)"
        '
        '功能TToolStripMenuItem
        '
        Me.功能TToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.批量编辑MToolStripMenuItem, Me.便捷输入EToolStripMenuItem, Me.桌面便签NToolStripMenuItem, Me.MD5加密SToolStripMenuItem, Me.转换编码RToolStripMenuItem})
        Me.功能TToolStripMenuItem.Name = "功能TToolStripMenuItem"
        Me.功能TToolStripMenuItem.Size = New System.Drawing.Size(58, 20)
        Me.功能TToolStripMenuItem.Text = "功能(&T)"
        '
        '批量编辑MToolStripMenuItem
        '
        Me.批量编辑MToolStripMenuItem.Name = "批量编辑MToolStripMenuItem"
        Me.批量编辑MToolStripMenuItem.Size = New System.Drawing.Size(145, 22)
        Me.批量编辑MToolStripMenuItem.Text = "批量编辑(&M)"
        '
        '便捷输入EToolStripMenuItem
        '
        Me.便捷输入EToolStripMenuItem.Name = "便捷输入EToolStripMenuItem"
        Me.便捷输入EToolStripMenuItem.Size = New System.Drawing.Size(145, 22)
        Me.便捷输入EToolStripMenuItem.Text = "便捷输入(&E)"
        '
        '桌面便签NToolStripMenuItem
        '
        Me.桌面便签NToolStripMenuItem.Name = "桌面便签NToolStripMenuItem"
        Me.桌面便签NToolStripMenuItem.Size = New System.Drawing.Size(145, 22)
        Me.桌面便签NToolStripMenuItem.Text = "桌面便签(&N)"
        '
        'MD5加密SToolStripMenuItem
        '
        Me.MD5加密SToolStripMenuItem.Name = "MD5加密SToolStripMenuItem"
        Me.MD5加密SToolStripMenuItem.Size = New System.Drawing.Size(145, 22)
        Me.MD5加密SToolStripMenuItem.Text = "MD5 加密(&S)"
        '
        '转换编码RToolStripMenuItem
        '
        Me.转换编码RToolStripMenuItem.Name = "转换编码RToolStripMenuItem"
        Me.转换编码RToolStripMenuItem.Size = New System.Drawing.Size(145, 22)
        Me.转换编码RToolStripMenuItem.Text = "转换编码(&R)"
        '
        '辅助ToolStripMenuItem
        '
        Me.辅助ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.计算器CToolStripMenuItem, Me.命令行MToolStripMenuItem, Me.讲述人TToolStripMenuItem, Me.放大镜RToolStripMenuItem, Me.屏幕键盘KToolStripMenuItem, Me.语音识别YToolStripMenuItem, Me.截图工具PToolStripMenuItem, Me.阅读这节SToolStripMenuItem, Me.语法高亮HToolStripMenuItem})
        Me.辅助ToolStripMenuItem.Name = "辅助ToolStripMenuItem"
        Me.辅助ToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.辅助ToolStripMenuItem.Text = "辅助(&H)"
        '
        '计算器CToolStripMenuItem
        '
        Me.计算器CToolStripMenuItem.Name = "计算器CToolStripMenuItem"
        Me.计算器CToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.计算器CToolStripMenuItem.Text = "计算器(&C)"
        '
        '命令行MToolStripMenuItem
        '
        Me.命令行MToolStripMenuItem.Name = "命令行MToolStripMenuItem"
        Me.命令行MToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.命令行MToolStripMenuItem.Text = "命令行(&M)"
        '
        '讲述人TToolStripMenuItem
        '
        Me.讲述人TToolStripMenuItem.Name = "讲述人TToolStripMenuItem"
        Me.讲述人TToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.讲述人TToolStripMenuItem.Text = "讲述人(&T)"
        '
        '放大镜RToolStripMenuItem
        '
        Me.放大镜RToolStripMenuItem.Name = "放大镜RToolStripMenuItem"
        Me.放大镜RToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.放大镜RToolStripMenuItem.Text = "放大镜(&R)"
        '
        '屏幕键盘KToolStripMenuItem
        '
        Me.屏幕键盘KToolStripMenuItem.Name = "屏幕键盘KToolStripMenuItem"
        Me.屏幕键盘KToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.屏幕键盘KToolStripMenuItem.Text = "屏幕键盘(&K)"
        '
        '语音识别YToolStripMenuItem
        '
        Me.语音识别YToolStripMenuItem.Name = "语音识别YToolStripMenuItem"
        Me.语音识别YToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.语音识别YToolStripMenuItem.Text = "语音识别(&Y)"
        '
        '截图工具PToolStripMenuItem
        '
        Me.截图工具PToolStripMenuItem.Name = "截图工具PToolStripMenuItem"
        Me.截图工具PToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.截图工具PToolStripMenuItem.Text = "截图工具(&P)"
        '
        '阅读这节SToolStripMenuItem
        '
        Me.阅读这节SToolStripMenuItem.Name = "阅读这节SToolStripMenuItem"
        Me.阅读这节SToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.阅读这节SToolStripMenuItem.Text = "阅读这节(&S)"
        '
        '语法高亮HToolStripMenuItem
        '
        Me.语法高亮HToolStripMenuItem.Name = "语法高亮HToolStripMenuItem"
        Me.语法高亮HToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.语法高亮HToolStripMenuItem.Text = "语法高亮(&H)"
        '
        '系统SToolStripMenuItem
        '
        Me.系统SToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.修改密码XToolStripMenuItem, Me.清除密码QToolStripMenuItem, Me.ToolStripSeparatorS1, Me.压缩数据CToolStripMenuItem, Me.ToolStripSeparatorS3, Me.检查更新NToolStripMenuItem, Me.问题反馈AToolStripMenuItem, Me.使用评价PToolStripMenuItem})
        Me.系统SToolStripMenuItem.Name = "系统SToolStripMenuItem"
        Me.系统SToolStripMenuItem.Size = New System.Drawing.Size(58, 20)
        Me.系统SToolStripMenuItem.Text = "系统(&S)"
        '
        '修改密码XToolStripMenuItem
        '
        Me.修改密码XToolStripMenuItem.Name = "修改密码XToolStripMenuItem"
        Me.修改密码XToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.修改密码XToolStripMenuItem.Text = "修改密码(&X)"
        '
        '清除密码QToolStripMenuItem
        '
        Me.清除密码QToolStripMenuItem.Name = "清除密码QToolStripMenuItem"
        Me.清除密码QToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.清除密码QToolStripMenuItem.Text = "清除密码(&Q)"
        '
        'ToolStripSeparatorS1
        '
        Me.ToolStripSeparatorS1.Name = "ToolStripSeparatorS1"
        Me.ToolStripSeparatorS1.Size = New System.Drawing.Size(137, 6)
        '
        '压缩数据CToolStripMenuItem
        '
        Me.压缩数据CToolStripMenuItem.Name = "压缩数据CToolStripMenuItem"
        Me.压缩数据CToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.压缩数据CToolStripMenuItem.Text = "压缩数据(&C)"
        '
        'ToolStripSeparatorS3
        '
        Me.ToolStripSeparatorS3.Name = "ToolStripSeparatorS3"
        Me.ToolStripSeparatorS3.Size = New System.Drawing.Size(137, 6)
        '
        '检查更新NToolStripMenuItem
        '
        Me.检查更新NToolStripMenuItem.Name = "检查更新NToolStripMenuItem"
        Me.检查更新NToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.检查更新NToolStripMenuItem.Text = "检查更新(&N)"
        '
        '问题反馈AToolStripMenuItem
        '
        Me.问题反馈AToolStripMenuItem.Name = "问题反馈AToolStripMenuItem"
        Me.问题反馈AToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.问题反馈AToolStripMenuItem.Text = "问题反馈(&A)"
        '
        '使用评价PToolStripMenuItem
        '
        Me.使用评价PToolStripMenuItem.Name = "使用评价PToolStripMenuItem"
        Me.使用评价PToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.使用评价PToolStripMenuItem.Text = "使用评价(&P)"
        Me.使用评价PToolStripMenuItem.Visible = False
        '
        'Status_ConText
        '
        Me.Status_ConText.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.总字数ToolStripStatusLabel, Me.占位符ToolStripStatusLabel, Me.内容ToolStripProgressBar, Me.上一节ToolStripStatusLabel, Me.展开节点ToolStripStatusLabel, Me.下一节ToolStripStatusLabel})
        Me.Status_ConText.Location = New System.Drawing.Point(0, 539)
        Me.Status_ConText.Name = "Status_ConText"
        Me.Status_ConText.Size = New System.Drawing.Size(592, 22)
        Me.Status_ConText.TabIndex = 0
        Me.Status_ConText.Text = "状态栏"
        '
        '总字数ToolStripStatusLabel
        '
        Me.总字数ToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.总字数ToolStripStatusLabel.Name = "总字数ToolStripStatusLabel"
        Me.总字数ToolStripStatusLabel.Size = New System.Drawing.Size(46, 17)
        Me.总字数ToolStripStatusLabel.Text = "总字数:"
        '
        '占位符ToolStripStatusLabel
        '
        Me.占位符ToolStripStatusLabel.Name = "占位符ToolStripStatusLabel"
        Me.占位符ToolStripStatusLabel.Size = New System.Drawing.Size(483, 17)
        Me.占位符ToolStripStatusLabel.Spring = True
        Me.占位符ToolStripStatusLabel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        '内容ToolStripProgressBar
        '
        Me.内容ToolStripProgressBar.AutoSize = False
        Me.内容ToolStripProgressBar.Name = "内容ToolStripProgressBar"
        Me.内容ToolStripProgressBar.Size = New System.Drawing.Size(200, 16)
        Me.内容ToolStripProgressBar.Visible = False
        '
        '上一节ToolStripStatusLabel
        '
        Me.上一节ToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.上一节ToolStripStatusLabel.Image = CType(resources.GetObject("上一节ToolStripStatusLabel.Image"), System.Drawing.Image)
        Me.上一节ToolStripStatusLabel.Name = "上一节ToolStripStatusLabel"
        Me.上一节ToolStripStatusLabel.Size = New System.Drawing.Size(16, 17)
        Me.上一节ToolStripStatusLabel.Text = "上一节"
        '
        '展开节点ToolStripStatusLabel
        '
        Me.展开节点ToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.展开节点ToolStripStatusLabel.Image = CType(resources.GetObject("展开节点ToolStripStatusLabel.Image"), System.Drawing.Image)
        Me.展开节点ToolStripStatusLabel.Name = "展开节点ToolStripStatusLabel"
        Me.展开节点ToolStripStatusLabel.Size = New System.Drawing.Size(16, 17)
        Me.展开节点ToolStripStatusLabel.Text = "展开节点"
        Me.展开节点ToolStripStatusLabel.ToolTipText = "展开节点"
        '
        '下一节ToolStripStatusLabel
        '
        Me.下一节ToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.下一节ToolStripStatusLabel.Image = CType(resources.GetObject("下一节ToolStripStatusLabel.Image"), System.Drawing.Image)
        Me.下一节ToolStripStatusLabel.LinkVisited = True
        Me.下一节ToolStripStatusLabel.Name = "下一节ToolStripStatusLabel"
        Me.下一节ToolStripStatusLabel.Size = New System.Drawing.Size(16, 17)
        Me.下一节ToolStripStatusLabel.Text = ">下一节"
        '
        'Timer_Author
        '
        Me.Timer_Author.Interval = 10000
        '
        '清除密码CM
        '
        Me.清除密码CM.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.加密记录JM})
        Me.清除密码CM.Name = "清除密码CM"
        Me.清除密码CM.Size = New System.Drawing.Size(181, 22)
        Me.清除密码CM.Text = "清除密码(&U)"
        Me.清除密码CM.Visible = False
        '
        '加密记录JM
        '
        Me.加密记录JM.Name = "加密记录JM"
        Me.加密记录JM.Size = New System.Drawing.Size(136, 22)
        Me.加密记录JM.Text = "加密记录(&L)"
        Me.加密记录JM.Visible = False
        '
        'Timer_AutoSave
        '
        Me.Timer_AutoSave.Interval = 180000
        '
        'Context_Notify
        '
        Me.Context_Notify.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Notify_联系作者, Me.Notify_Separator, Me.Notify_保持置顶, Me.Notify_隐藏窗口, Me.Notify_退出程序})
        Me.Context_Notify.Name = "Context_Notify"
        Me.Context_Notify.Size = New System.Drawing.Size(140, 98)
        '
        'Notify_联系作者
        '
        Me.Notify_联系作者.Name = "Notify_联系作者"
        Me.Notify_联系作者.Size = New System.Drawing.Size(139, 22)
        Me.Notify_联系作者.Text = "联系作者(&C)"
        '
        'Notify_Separator
        '
        Me.Notify_Separator.Name = "Notify_Separator"
        Me.Notify_Separator.Size = New System.Drawing.Size(136, 6)
        '
        'Notify_保持置顶
        '
        Me.Notify_保持置顶.CheckOnClick = True
        Me.Notify_保持置顶.Name = "Notify_保持置顶"
        Me.Notify_保持置顶.Size = New System.Drawing.Size(139, 22)
        Me.Notify_保持置顶.Text = "保持置顶(&T)"
        '
        'Notify_隐藏窗口
        '
        Me.Notify_隐藏窗口.CheckOnClick = True
        Me.Notify_隐藏窗口.Name = "Notify_隐藏窗口"
        Me.Notify_隐藏窗口.Size = New System.Drawing.Size(139, 22)
        Me.Notify_隐藏窗口.Text = "隐藏窗口(&H)"
        '
        'Notify_退出程序
        '
        Me.Notify_退出程序.Name = "Notify_退出程序"
        Me.Notify_退出程序.Size = New System.Drawing.Size(139, 22)
        Me.Notify_退出程序.Text = "退出程序(&X)"
        '
        'NotifyIcon_Key
        '
        Me.NotifyIcon_Key.ContextMenuStrip = Me.Context_Notify
        Me.NotifyIcon_Key.Icon = CType(resources.GetObject("NotifyIcon_Key.Icon"), System.Drawing.Icon)
        Me.NotifyIcon_Key.Text = "绿色笔记"
        '
        'Main_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 561)
        Me.Controls.Add(Me.FormBoder)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip_MyNote
        Me.Name = "Main_Form"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "绿色笔记"
        Me.FormBoder.Panel1.ResumeLayout(False)
        Me.FormBoder.Panel1.PerformLayout()
        Me.FormBoder.Panel2.ResumeLayout(False)
        Me.FormBoder.Panel2.PerformLayout()
        CType(Me.FormBoder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FormBoder.ResumeLayout(False)
        Me.Tab_Tree.ResumeLayout(False)
        Me.Tab_CCata.ResumeLayout(False)
        Me.ContextMenuStrip_TreeCata.ResumeLayout(False)
        Me.Tab_CSech.ResumeLayout(False)
        Me.Tab_CSech.PerformLayout()
        Me.Tab_CHsty.ResumeLayout(False)
        Me.Status_Tree.ResumeLayout(False)
        Me.Status_Tree.PerformLayout()
        Me.ConBoder.Panel1.ResumeLayout(False)
        Me.ConBoder.Panel2.ResumeLayout(False)
        CType(Me.ConBoder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConBoder.ResumeLayout(False)
        Me.ContextMenuStrip_Context.ResumeLayout(False)
        CType(Me.PictureNode, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip_Album.ResumeLayout(False)
        Me.ToolStrip_ConText.ResumeLayout(False)
        Me.ToolStrip_ConText.PerformLayout()
        Me.MenuStrip_MyNote.ResumeLayout(False)
        Me.MenuStrip_MyNote.PerformLayout()
        Me.Status_ConText.ResumeLayout(False)
        Me.Status_ConText.PerformLayout()
        Me.Context_Notify.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FormBoder As System.Windows.Forms.SplitContainer
    Friend WithEvents ContextMenuStrip_TreeCata As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents 新建记录NToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Status_Tree As System.Windows.Forms.StatusStrip
    Friend WithEvents 当前ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Status_ConText As System.Windows.Forms.StatusStrip
    Friend WithEvents 文件FToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 新建NToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparatorF1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 保存SToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 另存为AToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 退出XToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparatorE1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 总字数ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents 打开OToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 替换HCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparatorE2 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents 编辑EToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents 撤消UToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents 重做RToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents 剪切TToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents 复制CToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents 粘贴PToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents 查找FToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents 全选AToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents MenuStrip_MyNote As System.Windows.Forms.MenuStrip
    Friend WithEvents ConText As System.Windows.Forms.RichTextBox
    Friend WithEvents ImageList_TreeIcon As System.Windows.Forms.ImageList
    Friend WithEvents Tab_Tree As System.Windows.Forms.TabControl
    Friend WithEvents Tab_CCata As System.Windows.Forms.TabPage
    Friend WithEvents TreeView_Cata As System.Windows.Forms.TreeView
    Friend WithEvents Tab_CSech As System.Windows.Forms.TabPage
    Private WithEvents TextBox_FindText As System.Windows.Forms.TextBox
    Friend WithEvents TreeView_Seach As System.Windows.Forms.TreeView
    Friend WithEvents Tab_CHsty As System.Windows.Forms.TabPage
    Friend WithEvents 删除记录DToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 复制CCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 剪切TCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 粘贴PCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 新建目录NToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Timer_Author As System.Windows.Forms.Timer
    Friend WithEvents 全选ACToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConBoder As System.Windows.Forms.SplitContainer
    Friend WithEvents 添加照片AToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 清除照片DToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureNode As System.Windows.Forms.PictureBox
    Friend WithEvents ContextMenuStrip_Album As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents 自动缩放ZToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 导出图片EToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 删除图片DToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 替换HToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 查找FCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 全部导出AToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 清空图片CToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparatorC3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 段落格式GCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 字体格式WCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 清除密码CM As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 加密记录JM As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 系统SToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 修改密码XToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 清除密码QToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 合并子级EToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 上一节ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents 下一节ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents 展开节点ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStrip_ConText As System.Windows.Forms.ToolStrip
    Friend WithEvents 新建NToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents 打开OToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents 保存SToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparatorT1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 撤销UToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents 重做RToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparatorT2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 剪切UToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents 复制CToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents 粘贴PToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparatorT3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 清除格式ToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparatorS1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 压缩数据CToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 占位符ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents 内容ToolStripProgressBar As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents 进度ToolStripProgressBar As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents 撤销UCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparatorC1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 删除DCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparatorC4 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents ContextMenuStrip_Context As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents 添加图片TToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 段落格式ToolStripButton As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents 居中对齐ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 左对齐ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 右对齐ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparatorD1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 作为上标ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 作为下标ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparatorD2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 首行缩进ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 项目符号ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 帮助LToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparatorT4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparatorC2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 插入文本ICToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 符号ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 时间ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 日期ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 自定义图片ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 日期时间ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 检查更新NToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 辅助ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 计算器CToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 命令行MToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 讲述人TToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 放大镜RToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 屏幕键盘KToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 语音识别YToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 截图工具PToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Timer_AutoSave As System.Windows.Forms.Timer
    Friend WithEvents ToolStripSeparatorS3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Com_DB As System.Windows.Forms.ComboBox
    Friend WithEvents 格式刷ToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents 已阅YCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 粘贴文本NCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 导出EToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 阅读这节SToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparatorF2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 批量导入ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 格式字体ToolStripButton As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents 加粗字体ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 斜体字体ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 加粗倾斜ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 加下划线ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 加删除线ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 恢复默认ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 其他功能RToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 导出图库AToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 问题反馈AToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 使用评价PToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 清除颜色ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 自动换行ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 分割记录FToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 语法高亮HToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 改变图标TToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 插入IToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 功能TToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 批量编辑MToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 便捷输入EToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 桌面便签NToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MD5加密SToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 转换编码RToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TreeView_Mark As System.Windows.Forms.TreeView
    Friend WithEvents ToolStripSeparatorF3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Context_Notify As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents Notify_联系作者 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Notify_Separator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Notify_保持置顶 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Notify_隐藏窗口 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Notify_退出程序 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotifyIcon_Key As System.Windows.Forms.NotifyIcon

End Class
