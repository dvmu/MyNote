﻿Imports System.IO

Public Class UpDate_Form

    Private Sub Button_Exit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Exit.Click
        Me.Close()
    End Sub

    Private Sub Button_Up_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Up.Click
        Try
            Dim FileName As String
            Dim OneSaveFileDialog As New SaveFileDialog
            OneSaveFileDialog.FileName = Path.GetFileName(Label_Link.Text)
            OneSaveFileDialog.Title = "保存"
            OneSaveFileDialog.Filter = "所有文件|*.*"
            If OneSaveFileDialog.ShowDialog() <> DialogResult.Cancel Then
                FileName = OneSaveFileDialog.FileName
                My.Computer.Network.DownloadFile(Label_Link.Text, FileName, "", "", True, 10000, True)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


End Class