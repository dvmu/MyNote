﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Diagnostics
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Security.Cryptography
Imports System.Text.RegularExpressions

Public Class Main_Form

#Region "Define Global Var"
    Public AccessDB As String = "Note"
    Public AccessPassStr As String = ";jet oledb:database Password ="
    Public AccessConnectionString As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                                              "Data Source=" & My.Application.Info.DirectoryPath & "\" & AccessDB & ".mdb;" & _
                                              "Persist Security Info=False"
    '读取记录文本
    Dim PicCStr As String = ""
    '记录最后选择的节点的完整路径
    Public Remember As String = ""
    Public MarkRemember As String = ""
    '文本是否被更改
    Dim MyPage As String
    '#主页网址：http://shop106659604.taobao.com#
    Public NewVerUrl As String = "http://bcs.duapp.com/cedtry/MyNote/cednote.ini"
    Dim OneOpenFileDialog As New OpenFileDialog
    Dim OneFontDialog As New FontDialog
    Dim OneSaveFileDialog As New SaveFileDialog
    Dim OneFolderBrowser As New FolderBrowserDialog
#End Region

#Region "Define Global Function"
    Private Declare Auto Function SetProcessWorkingSetSize Lib "kernel32.dll" (ByVal procHandle As IntPtr, ByVal min As Int32, ByVal max As Int32) As Boolean
    Public Sub SetProcessWorkingSetSize()
        Try
            Dim Mem As Process
            Mem = Process.GetCurrentProcess()
            SetProcessWorkingSetSize(Mem.Handle, -1, -1)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Sub AddNode(ByVal Dir As String, ByVal Node As String, ByVal FormatCheck As Boolean, ByVal Text As String, ByVal Pic As Integer, ByVal SPic As Integer)
        Try
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessString As String = "INSERT INTO 记录(目录,节点,格式,文本,图标,标记) VALUES(" & "'" & Dir & "'" & "," & "'" & Node & "'" & "," & FormatCheck & "," & "?" & "," & Pic & "," & SPic & ")"
            Dim AccessCmd = New OleDb.OleDbCommand(AccessString, AccessConn)
            AccessCmd.Parameters.Add(New OleDb.OleDbParameter)
            AccessCmd.Parameters(0).Value = Text
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "保存")
        End Try
    End Sub

    Sub ReadNode(ByVal SNodeID As String)
        If SNodeID = "烂笔头" Or SNodeID = "" Then
            ConText.Text = ""
            Exit Sub
        End If

        PicIndex = 0
        PicCStr = ""
        Try
            Dim AccessString As String = "SELECT * FROM 记录 WHERE 编号=" & SNodeID
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
            If TempDataSet.Tables(0).Rows(0).Item(2) Then
                清除格式ToolStripButton.CheckState = CheckState.Checked
                ConText.Text = TempDataSet.Tables(0).Rows(0).Item(3).ToString.Replace("双引号", """")
            Else
                清除格式ToolStripButton.CheckState = CheckState.Unchecked
                ConText.Rtf = TempDataSet.Tables(0).Rows(0).Item(3).ToString.Replace("双引号", """")
            End If
            If TempDataSet.Tables(0).Rows(0).Item(7) Then
                Try
                    Dim AccessString2 As String = "SELECT * FROM 图库 WHERE 节点=" & "'" & SNodeID & "'"
                    Dim AccessConn2 As New OleDb.OleDbConnection(AccessConnectionString)
                    AccessConn2.Open()
                    Dim AccessAdapter2 As OleDbDataAdapter = New OleDbDataAdapter(AccessString2, AccessConn2)
                    AccessConn2.Close()
                    Dim TempDataSet2 As New DataSet
                    AccessAdapter2.Fill(TempDataSet2)
                    If TempDataSet2.Tables(0).Rows.Count = 0 Then Exit Try
                    PicID = TempDataSet2.Tables(0).Rows(0).Item(0)
                    Dim Picturebyte = TempDataSet2.Tables(0).Rows(0).Item(2)
                    PictureNode.Image = Bitmap.FromStream(New IO.MemoryStream(Picturebyte, True), True)
                    If ConBoder.SplitterDistance > ConBoder.Height - 25 Then ConBoder.SplitterDistance = ConBoder.Height * 1 / 3
                Catch AccessException As Exception
                    MsgBox("读取图片出错" & vbCrLf & "信息：" & AccessException.Message, , "读取")
                End Try
            Else
                ConBoder.SplitterDistance = ConBoder.Height
                PictureNode.Image = Nothing
            End If
            If TempDataSet.Tables(0).Rows(0).Item(8) Then
                PictureNode.SizeMode = PictureBoxSizeMode.Zoom
                自动缩放ZToolStripMenuItem.Checked = True
            Else
                PictureNode.SizeMode = PictureBoxSizeMode.CenterImage
                自动缩放ZToolStripMenuItem.Checked = False
            End If
        Catch AccessException As Exception
            MsgBox("读取记录出错" & vbCrLf & "信息：" & AccessException.Message, , "读取")
        End Try
        Dim PicCount As Integer
        Try
            Dim AccessString As String = "SELECT * FROM 图库 WHERE 节点=" & "'" & TreeView_Cata.SelectedNode.Name & "'"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            PicCount = TempDataSet.Tables(0).Rows.Count
            If PicCount = 0 Then Exit Try
            PicCStr = "," & PicCount.ToString
        Catch AccessException As Exception
            MsgBox("读取图片数量出错" & vbCrLf & "信息：" & AccessException.Message, , "读取")
        End Try
        SetProcessWorkingSetSize()
        保存SToolStripButton.Enabled = False
        If Tab_Tree.SelectedIndex = 0 Then AddMark(Format(Now(), "yyyy/MM/dd"), TreeView_Cata.SelectedNode.Text, TreeView_Cata.SelectedNode.Name, 2, 5)
    End Sub

#End Region

#Region "TreeView Content Editor "

    Private Sub TreeView_Cata_AfterLabelEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.NodeLabelEditEventArgs) Handles TreeView_Cata.AfterLabelEdit
        If (e.Node.Text = e.Label) Or (e.Label = "") Or (e.Node.Parent Is Nothing) Then
            e.CancelEdit = True
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor
        进度ToolStripProgressBar.Width = Status_Tree.Width - 4
        当前ToolStripStatusLabel.Visible = False
        进度ToolStripProgressBar.Visible = True

        Dim NodeID As String
        NodeID = e.Node.Name

        Try
            Dim AccessString As String = "UPDATE 记录 SET 节点=" & "'" & e.Label & "'" & "WHERE 编号=" & NodeID
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            Me.Cursor = Cursors.Default
            MsgBox(AccessException.Message, , sender.Text)
        End Try

        Me.Refresh()

        Me.Cursor = Cursors.Default
        TreeView_Cata.SelectedNode = e.Node
        进度ToolStripProgressBar.Value = 0
        进度ToolStripProgressBar.Visible = False
        当前ToolStripStatusLabel.Visible = True
        当前ToolStripStatusLabel.Text = 当前ToolStripStatusLabel.Text.Replace(e.Node.Text, e.Label)
    End Sub

    Private Sub TreeView_Cata_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles TreeView_Cata.ItemDrag
        If My.Computer.Keyboard.CtrlKeyDown Then
            TreeView_Cata.DoDragDrop(TreeView_Cata.SelectedNode, DragDropEffects.Move)
        Else
            当前ToolStripStatusLabel.Text = "按下Ctrl键以拖动！"
            TreeView_Cata.DoDragDrop(TreeView_Cata.SelectedNode, DragDropEffects.None)
        End If
    End Sub

    Private Sub TreeView_Cata_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TreeView_Cata.DragEnter
        If TreeView_Cata.Focused Then e.Effect = DragDropEffects.Move
    End Sub

    Private Sub TreeView_Cata_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TreeView_Cata.DragDrop
        '返回鼠标在控件中的位置
        Dim p As Point = TreeView_Cata.PointToClient(New Point(e.X, e.Y))
        '把鼠标放置位置转换为项

        Dim dragToNode As TreeNode = TreeView_Cata.GetNodeAt(p.X, p.Y)
        If dragToNode Is TreeView_Cata.SelectedNode Then Return
        If dragToNode Is TreeView_Cata.SelectedNode.Parent Then Return
        If TreeView_Cata.SelectedNode Is Nothing Then Return
        If dragToNode Is Nothing Then Return
        Me.Cursor = Cursors.WaitCursor
        Dim InsertIndex As Integer
        Dim InsertNode As TreeNode
        '内部节点操作

        '检查是不是把父节点拖放到子节点,不允许父节点拖放到它之下的子节点
        Dim CheckNode As TreeNode = dragToNode.Parent
        Do Until CheckNode Is Nothing
            If CheckNode Is TreeView_Cata.SelectedNode Then
                Me.Cursor = Cursors.Default
                Return
            End If
            CheckNode = CheckNode.Parent
        Loop
        InsertNode = CType(TreeView_Cata.SelectedNode.Clone, TreeNode)
        '在目标位置插入拖动的项
        InsertIndex = dragToNode.Index
        '检查目标位置和拖动位置是否相同目录下的
        dragToNode.Expand()

        dragToNode.Nodes.Insert(InsertIndex, InsertNode)
        Dim NodeID As String = TreeView_Cata.SelectedNode.Name
        Dim NewDir As String = dragToNode.Name

        进度ToolStripProgressBar.Width = Status_Tree.Width - 4
        当前ToolStripStatusLabel.Visible = False
        进度ToolStripProgressBar.Visible = True
        Me.Refresh()
        NodeMove(NodeID, NewDir)
        TreeView_Cata.SelectedNode.Expand()
        TreeView_Cata.SelectedNode.Remove()
        dragToNode.Expand()
        TreeView_Cata.SelectedNode = InsertNode
        Me.Cursor = Cursors.Default
        进度ToolStripProgressBar.Value = 0
        进度ToolStripProgressBar.Visible = False
        当前ToolStripStatusLabel.Visible = True
    End Sub

    Sub NodeMove(ByVal NodeID As String, ByVal NewDir As String)
        Try
            Dim AccessString As String = "UPDATE 记录 SET 目录=" & "'" & NewDir & "'" & "WHERE 编号=" & NodeID
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "NodeMove")
        End Try
    End Sub

    Private Sub TreeView_Cata_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView_Cata.AfterSelect
        Remember = e.Node.Name
        ReadNode(e.Node.Name)
        当前ToolStripStatusLabel.Text = "当前:" & e.Node.Text & " (" & e.Node.Nodes.Count() & PicCStr & ")"
    End Sub

    Private Sub 新建记录NToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 新建记录NToolStripMenuItem.Click, 新建NToolStripMenuItem.Click, 新建NToolStripButton.Click
        Dim NodeDir As String
        If TreeView_Cata.SelectedNode.Parent Is Nothing Then
            NodeDir = "烂笔头"
        Else
            NodeDir = TreeView_Cata.SelectedNode.Name
        End If

        Dim Node As String = "新记录"
        Dim Path As String = NodeDir & "\" & Node
        Dim FormatCheck As Boolean = False
        Dim TempRichText As New RichTextBox
        TempRichText.Text = ""
        Dim Text As String = TempRichText.Rtf
        AddNode(NodeDir, Node, FormatCheck, Text, 2, 5)
        TreeView_Cata.SelectedNode.Nodes.Add(Node)
        If TreeView_Cata.SelectedNode.IsExpanded Then TreeView_Cata.SelectedNode.Collapse()
        TreeView_Cata.SelectedNode.Expand()
        For Each OneNode As TreeNode In TreeView_Cata.SelectedNode.Nodes
            If OneNode.Text = Node Then
                TreeView_Cata.SelectedNode = OneNode
                OneNode.BeginEdit()
                Exit For
            End If
        Next

    End Sub

    Sub CIcon2()
        Try
            Dim AccessString As String = "UPDATE 记录 SET 图标= 2 WHERE 编号=" & TreeView_Cata.SelectedNode.Name
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("更改图标出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 记录 SET 标记= 5 WHERE 编号=" & TreeView_Cata.SelectedNode.Name
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("更改标记出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
    End Sub

    Sub CIcon0()
        Try
            Dim AccessString As String = "UPDATE 记录 SET 图标= 0 WHERE 编号=" & TreeView_Cata.SelectedNode.Name
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("更改图标出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 记录 SET 标记= 1 WHERE 编号=" & TreeView_Cata.SelectedNode.Name
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("更改标记出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
    End Sub

    Private Sub 改变图标TToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 改变图标TToolStripMenuItem.Click
        Try
            Dim AccessString As String = "SELECT 图标 FROM 记录 WHERE 编号=" & TreeView_Cata.SelectedNode.Name
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Try
            If TempDataSet.Tables(0).Rows(0).Item(0).ToString = "0" Then
                CIcon2()
                TreeView_Cata.SelectedNode.ImageIndex = 2
                TreeView_Cata.SelectedNode.SelectedImageIndex = 5
            Else
                CIcon0()
                TreeView_Cata.SelectedNode.ImageIndex = 0
                TreeView_Cata.SelectedNode.SelectedImageIndex = 1
            End If

        Catch AccessException As Exception
            MsgBox("读取图片数量出错" & vbCrLf & "信息：" & AccessException.Message, , "读取")
        End Try

    End Sub

    Private Sub 新建目录NToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 新建目录NToolStripMenuItem.Click
        Dim NodeDir As String
        If TreeView_Cata.SelectedNode.Parent Is Nothing Then
            NodeDir = "烂笔头"
        Else
            NodeDir = TreeView_Cata.SelectedNode.Name
        End If
        Dim Node As String = "新目录"
        Dim FormatCheck As Boolean = True
        Dim TempRichText As New RichTextBox
        Dim Text As String = ""
        AddNode(NodeDir, Node, FormatCheck, Text, 0, 1)
        TreeView_Cata.SelectedNode.Nodes.Add(Node)
        If TreeView_Cata.SelectedNode.IsExpanded Then TreeView_Cata.SelectedNode.Collapse()
        TreeView_Cata.SelectedNode.Expand()
        For Each OneNode As TreeNode In TreeView_Cata.SelectedNode.Nodes
            If OneNode.Text = Node Then
                TreeView_Cata.SelectedNode = OneNode
                OneNode.BeginEdit()
                Exit For
            End If
        Next
    End Sub
    '写入文本记录
    Private Sub TreeView_Cata_BeforeSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles TreeView_Cata.BeforeSelect
        If TreeView_Cata.SelectedNode Is Nothing Then Exit Sub
        If TreeView_Cata.SelectedNode.Level = 0 Then Exit Sub
        Dim NodeID As String = TreeView_Cata.SelectedNode.Name
        Dim NewText As String
        Dim AccessString As String
        Try
            If 保存SToolStripButton.Enabled = False Then Exit Sub
            If 清除格式ToolStripButton.Checked Then
                NewText = ConText.Text.Replace("""", "双引号")
            Else
                NewText = ConText.Rtf.Replace("""", "双引号")
            End If
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            AccessString = "UPDATE 记录 SET 文本=? WHERE 编号=" & TreeView_Cata.SelectedNode.Name
            Dim AccessCmd = New OleDb.OleDbCommand(AccessString, AccessConn)
            AccessCmd.Parameters.Add(New OleDb.OleDbParameter)
            AccessCmd.Parameters(0).Value = NewText
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "节点保存")
        End Try
        Try
            AccessString = "UPDATE 记录 SET 格式=" & 清除格式ToolStripButton.Checked & " WHERE 编号=" & NodeID
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存记录格式出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
    End Sub
    '读取目录
    Private Sub TreeView_Cata_BeforeExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles TreeView_Cata.BeforeExpand
        Dim NodePath As String = e.Node.Name
        e.Node.Nodes.Clear()
        Try
            Dim AccessString As String = "SELECT * FROM 记录 WHERE 目录=" & "'" & NodePath & "'"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            AccessConn.Close()
            If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
            For i As Integer = 0 To TempDataSet.Tables(0).Rows.Count - 1
                Dim OneNode As New TreeNode
                OneNode.Name = TempDataSet.Tables(0).Rows(i).Item(0)
                OneNode.Text = TempDataSet.Tables(0).Rows(i).Item(1)
                OneNode.ImageIndex = TempDataSet.Tables(0).Rows(i).Item(5)
                OneNode.SelectedImageIndex = TempDataSet.Tables(0).Rows(i).Item(6)
                OneNode.Nodes.Add("Null")
                e.Node.Nodes.Add(OneNode)
            Next
        Catch AccessException As Exception
            MsgBox("读取子节点出错" & vbCrLf & "信息：" & AccessException.Message, , "读取")
        End Try
    End Sub

    Private Sub 批量导入ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 批量导入ToolStripMenuItem.Click
        If Tab_Tree.SelectedIndex <> 0 Then Return
        If TreeView_Cata.SelectedNode Is Nothing Then Return
        OneFolderBrowser.RootFolder = Environment.SpecialFolder.Desktop
        OneFolderBrowser.ShowNewFolderButton = False
        If OneFolderBrowser.ShowDialog() <> DialogResult.Cancel Then
            进度ToolStripProgressBar.Width = Status_Tree.Width - 4
            当前ToolStripStatusLabel.Visible = False
            进度ToolStripProgressBar.Visible = True
            进度ToolStripProgressBar.Maximum = Directory.GetFiles(OneFolderBrowser.SelectedPath).Length + 1
            Me.Refresh()
            进度ToolStripProgressBar.Value = 0
            For Each i In Directory.GetFiles(OneFolderBrowser.SelectedPath)
                进度ToolStripProgressBar.Value = 进度ToolStripProgressBar.Value + 1
                If Path.GetExtension(i).ToLower = ".txt" Then
                    AddNode(TreeView_Cata.SelectedNode.Name, Path.GetFileNameWithoutExtension(i), True, File.ReadAllText(i, System.Text.Encoding.Default), 2, 5)
                End If
            Next
            TreeView_Cata.SelectedNode.Nodes.Add("NULL")
            TreeView_Cata.SelectedNode.Collapse()
            TreeView_Cata.SelectedNode.Expand()
            当前ToolStripStatusLabel.Visible = True
            进度ToolStripProgressBar.Visible = False
        End If
    End Sub

#End Region

#Region "The Search Panel"
    Dim SearchStr As String

    Sub FindNode(ByVal NodeID As String)
        OpenNode(GetNodePath(NodeID))
    End Sub

    Function GetNodePath(ByVal NodeID As String) As String
        If NodeID = "烂笔头" Then
            Return "烂笔头"
            Exit Function
        End If
        GetNodePath = NodeID
        Dim ParentNode As String
        Dim AccessString As String = "SELECT * FROM 记录 WHERE 编号=" & NodeID
        Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
        AccessConn.Open()
        Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
        AccessConn.Close()
        Dim TempDataSet As New DataSet
        AccessAdapter.Fill(TempDataSet)
        If TempDataSet.Tables(0).Rows.Count = 0 Then Return "烂笔头"
        ParentNode = TempDataSet.Tables(0).Rows(0).Item(4)
        If ParentNode <> "烂笔头" Then
            GetNodePath = GetNodePath(ParentNode) & "\" & NodeID
        Else
            GetNodePath = "烂笔头" & "\" & GetNodePath
        End If
        Return GetNodePath
    End Function

    Sub OpenNode(ByVal NodePath As String)
        Try
            If InStr(NodePath, "\") Then
                Dim NodeText() As String
                NodeText = NodePath.Split("\")
                Dim AllNode As TreeNodeCollection = TreeView_Cata.Nodes.Item(0).Nodes
                Dim OneNode As TreeNode = TreeView_Cata.Nodes.Item(0)
                For i As Integer = 1 To NodeText.Count - 1
                    For Each OneNode In AllNode
                        If OneNode.Name = NodeText(i) Then
                            OneNode.Expand()
                            AllNode = OneNode.Nodes
                            Exit For
                        End If
                    Next
                Next
                TreeView_Cata.SelectedNode = OneNode
            Else
                TreeView_Cata.SelectedNode = TreeView_Cata.Nodes.Item(0)
            End If
            Tab_Tree.SelectedIndex = 0
            TreeView_Cata.Focus()
            SetProcessWorkingSetSize()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Search_TreeView_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView_Seach.AfterSelect
        If TreeView_Seach.SelectedNode.Level = 0 Then Return
        ReadNode(e.Node.Name)
        当前ToolStripStatusLabel.Text = "当前:" & e.Node.Text & " (" & e.Node.Nodes.Count() & PicCStr & ")"
    End Sub

    Private Sub Search_TreeView_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeView_Seach.NodeMouseDoubleClick
        If e.Node Is TreeView_Seach.Nodes.Item(0) Then Exit Sub
        FindNode(e.Node.Name)
    End Sub
    Dim PicFileRoot As String
    Private Sub TextBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox_FindText.Click
        TextBox_FindText.SelectAll()
    End Sub

    Private Sub TextBox_FindText_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_FindText.KeyPress
        If e.KeyChar = CChar(ChrW(13)) Then
            If TextBox_FindText.Text.Trim = "" Then
                TreeView_Seach.Nodes.Item(0).Nodes.Clear()
                Return
            End If
            SearchStr = TextBox_FindText.Text
            TreeView_Seach.Nodes.Item(0).Nodes.Clear()
            Me.Cursor = Cursors.WaitCursor
            进度ToolStripProgressBar.Width = Status_Tree.Width - 4
            当前ToolStripStatusLabel.Visible = False
            进度ToolStripProgressBar.Visible = True
            Me.Refresh()
            Try
                Dim AccessString As String = "SELECT * FROM 记录"
                Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                AccessConn.Open()
                Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
                Dim TempDataSet As New DataSet
                AccessAdapter.Fill(TempDataSet)
                If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
                Dim TempRich As New RichTextBox
                进度ToolStripProgressBar.Maximum = TempDataSet.Tables(0).Rows.Count - 1
                Me.Refresh()
                For i As Integer = 0 To TempDataSet.Tables(0).Rows.Count - 1
                    If i <= 进度ToolStripProgressBar.Maximum Then 进度ToolStripProgressBar.Value = i
                    Dim FormatCheck As Boolean
                    Dim FindText As String
                    FormatCheck = TempDataSet.Tables(0).Rows(i).Item(2)
                    If FormatCheck Then
                        FindText = TempDataSet.Tables(0).Rows(i).Item(3) & TempDataSet.Tables(0).Rows(i).Item(1)
                    Else
                        TempRich.Rtf = TempDataSet.Tables(0).Rows(i).Item(3)
                        FindText = TempRich.Text & TempDataSet.Tables(0).Rows(i).Item(1)
                    End If
                    If InStr(FindText.ToLower, SearchStr.ToLower) Then
                        Dim OneNode As New TreeNode
                        OneNode.Name = TempDataSet.Tables(0).Rows(i).Item(0)
                        OneNode.Text = TempDataSet.Tables(0).Rows(i).Item(1)
                        OneNode.ImageIndex = TempDataSet.Tables(0).Rows(i).Item(5)
                        OneNode.SelectedImageIndex = TempDataSet.Tables(0).Rows(i).Item(6)
                        TreeView_Seach.Nodes.Item(0).Nodes.Add(OneNode)
                        TreeView_Seach.Nodes.Item(0).Expand()
                    Else
                        Continue For
                    End If
                Next
                AccessConn.Close()
            Catch AccessException As Exception
                Me.Cursor = Cursors.Default
                MsgBox(AccessException.Message, , sender.Text)
            End Try
        End If
        Me.Cursor = Cursors.Default
        进度ToolStripProgressBar.Value = 0
        进度ToolStripProgressBar.Visible = False
        当前ToolStripStatusLabel.Visible = True
        当前ToolStripStatusLabel.Text = "共发现" & TreeView_Seach.GetNodeCount(True) - 1 & "项"
        SetProcessWorkingSetSize()
    End Sub

#End Region

#Region "Teh Auto Update Module"

#Region "ReadIni"
    '声明INI配置文件读写API函数
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Int32, ByVal lpFileName As String) As Int32
    Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Int32
    '定义读取配置文件函数
    Public Function GetINI(ByVal Section As String, ByVal AppName As String, ByVal DefaultStr As String, ByVal FileName As String) As String
        Dim Str As String = ""
        Str = LSet(Str, 256)
        GetPrivateProfileString(Section, AppName, DefaultStr, Str, Len(Str), FileName)
        Return Microsoft.VisualBasic.Left(Str, InStr(Str, Chr(0)) - 1)
    End Function
#End Region

    Sub AutoUpDate(ByVal SourceUrlStr As String, ByVal VoiceTip As Boolean, ByVal ShowForm As Boolean)
        Dim VerFilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.InternetCache) & "\Ver.ini"
        Try
            My.Computer.Network.DownloadFile(SourceUrlStr, VerFilePath, "", "", False, 10000, True)
        Catch ex As Exception
            总字数ToolStripStatusLabel.Text = "获取更新失败,检查网络连接......"
        End Try

        Try
            Me.Cursor = Cursors.WaitCursor
            Dim CurVer As String = My.Application.Info.Version.ToString
            Dim NewVer As String = My.Application.Info.Version.ToString
            NewVer = GetINI("版本更新", "最新版本", "", VerFilePath)
            If NewVer <> "" Then
                UpDate_Form.TextBox_Cur.Text = CurVer
                UpDate_Form.TextBox_New.Text = NewVer
                UpDate_Form.TextBox_Up.Text = GetINI("版本更新", "更新内容", "暂无内容", VerFilePath)
                UpDate_Form.Label_Link.Text = GetINI("版本更新", "更新地址", "http://yun.baidu.com/share/home?uk=1778788236", VerFilePath)
                Email_Form.UserStr = GetINI("系统配置", "默认账户", "dvmu_00@163.com", VerFilePath)
                Email_Form.PswdStr = GetINI("系统配置", "默认密码", "988B4AF72E3F2DD1D4C88F59C6A14E4F", VerFilePath)
                MyPage = GetINI("系统配置", "主页网址", "http://dvmu.taobao.com", VerFilePath)
                If VoiceTip And NewVer <> CurVer Then
                    Dim TempStr As String = CheckPath("发现新版本，请及时更新！")
                    TempStr = TempStr.Replace(vbLf, """" & " & vbCrLf & " & """")
                    Dim TempText As String = "CreateObject(" & """" & "SAPI.SpVoice" & """" & ").Speak" & """" & TempStr & """" & vbCrLf & _
                                             "Set ObjFSO = CreateObject(" & """" & "Scripting.FileSystemObject" & _
                                             """" & ")" & vbCrLf & "ObjFSO.DeleteFile Wscript.ScriptFullName"
                    Dim TempPath As String = Environment.GetFolderPath(Environment.SpecialFolder.Templates) & "\NewFound.vbs"
                    FileIO.FileSystem.WriteAllText(TempPath, TempText, False, System.Text.Encoding.Default)
                    System.Diagnostics.Process.Start(TempPath)
                End If
            Else
                UpDate_Form.TextBox_New.Text = "获取版本失败……"
                UpDate_Form.TextBox_Up.Text = "获取更新信息失败……"
            End If
            If ShowForm Then UpDate_Form.ShowDialog()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("获取更新失败，请联系作者", MsgBoxStyle.Critical, "错误")
            Help_Form.ShowDialog()
        End Try
    End Sub


#End Region

#Region "The Author Module"

    Private Sub RichTextStatu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 总字数ToolStripStatusLabel.Click
        If ADLink Is Nothing Then Exit Sub
        If ADLink.Split(vbCrLf)(ADIndex) = "" Then
            System.Diagnostics.Process.Start("http://dvmu.taobao.com/")
        Else
            System.Diagnostics.Process.Start("http://item.taobao.com/item.htm?" & ADLink.Split(vbCrLf)(ADIndex))
        End If
    End Sub

    Dim ADLink As String
    Dim ADText As String
    Dim ADIndex As Integer
    Private Sub AD_Timer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer_Author.Tick
        Try
            ADIndex = ADIndex + 1
            If ADIndex = ADLink.Split(vbCrLf).Count Then
                ADIndex = 0
            End If
            总字数ToolStripStatusLabel.Text = ADText.Split(vbCrLf)(ADIndex).Trim
        Catch ex As Exception
            总字数ToolStripStatusLabel.Text = "感谢使用陈恩点笔记软件！"
        End Try

    End Sub
    Private Const C_REGULAR_Link = "id=([0-9]{11})\b"
    Private Const C_REGULAR_Text = "(陈恩点|苗玉珠) ([^""""<]+)"

    Sub GetUrl(ByVal MainPage As String)
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim CurVer As String = My.Application.Info.Version.ToString
            Dim url As String = MainPage  ' 网页地址
            Dim httpReq As System.Net.HttpWebRequest
            Dim httpResp As System.Net.HttpWebResponse
            Dim httpURL As New System.Uri(url)
            httpReq = CType(Net.HttpWebRequest.Create(httpURL), Net.HttpWebRequest)
            httpReq.Method = "GET"
            httpResp = CType(httpReq.GetResponse(), Net.HttpWebResponse)
            httpReq.KeepAlive = False ' 获取或设置一个值，该值指示是否与Internet资源建立持久连接。
            Dim reader As IO.StreamReader = New IO.StreamReader(httpResp.GetResponseStream, System.Text.Encoding.Default)    '用GB2312字符
            Dim respHTML As String = reader.ReadToEnd() 'respHTML就是网页源代码 
            If respHTML <> "" Then
                Dim rgx As New System.Text.RegularExpressions.Regex(C_REGULAR_Link, RegexOptions.IgnoreCase)
                Dim matches As MatchCollection = rgx.Matches(respHTML)
                If matches.Count > 0 Then
                    For Each match As Match In matches
                        If InStr(ADLink, match.Value) = False Then ADLink = ADLink & vbCrLf & match.Value
                    Next
                End If
                Dim rgxt As New System.Text.RegularExpressions.Regex(C_REGULAR_Text, RegexOptions.IgnoreCase)
                Dim matchest As MatchCollection = rgxt.Matches(respHTML)
                If matchest.Count > 0 Then
                    For Each match As Match In matchest
                        Dim TMStr As String = match.Value
                        If InStr(ADText, TMStr) = False Then ADText = ADText & vbCrLf & TMStr
                    Next
                    If ADText.Length > 0 Then
                        ADText = ADText.Trim()
                        ADLink = ADLink.Trim()
                        Timer_Author.Enabled = True
                    End If
                    SetProcessWorkingSetSize()
                End If
            End If
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region "The Database Selected"

    Sub GetAllDB()
        Dim AllDB() As String = Directory.GetFiles(My.Application.Info.DirectoryPath, "*.mdb")
        Com_DB.Items.Clear()
        For i As Integer = 0 To AllDB.Length - 1
            Com_DB.Items.Add(Path.GetFileNameWithoutExtension(AllDB(i)))
        Next
        Dim DBName As String = GetSetting("MyNote", "DBSel", "DBName", "Note")

        If File.Exists(My.Application.Info.DirectoryPath & "\" & DBName & ".mdb") Then
            Com_DB.Text = DBName
            AccessDB = DBName
            AccessConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                                              "Data Source=" & My.Application.Info.DirectoryPath & "\" & AccessDB & ".mdb;" & _
                                              "Persist Security Info=False"
        Else
            Com_DB.SelectedIndex = 0
            AccessDB = Com_DB.Text
            AccessConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                                              "Data Source=" & My.Application.Info.DirectoryPath & "\" & AccessDB & ".mdb;" & _
                                              "Persist Security Info=False"
        End If
    End Sub

    Private Sub Com_DB_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If File.Exists(My.Application.Info.DirectoryPath & "\" & Com_DB.Text & ".mdb") Then
                Dim TempAccStr As String = AccessDB
                AccessDB = Com_DB.Text
                AccessConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                                         "Data Source=" & My.Application.Info.DirectoryPath & "\" & AccessDB & ".mdb;" & _
                                         "Persist Security Info=False"
                '检查密码
                Try
                    Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                    AccessConn.Open()
                    AccessConn.Close()
                    TreeView_Cata.Nodes(0).Collapse()
                    TreeView_Cata.Nodes(0).Expand()
                Catch AccessException As Exception
                    Pass_Form.T1()
                    Pass_Form.Label1.Text = "请输入密码："
                    Pass_Form.Text = AccessDB
                    Pass_Form.ShowDialog()
                End Try

                Try
                    Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                    AccessConn.Open()
                    AccessConn.Close()
                Catch AccessException As Exception
                    Com_DB.Text = TempAccStr
                    AccessDB = TempAccStr
                    AccessConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                                             "Data Source=" & My.Application.Info.DirectoryPath & "\" & AccessDB & ".mdb;" & _
                                             "Persist Security Info=False"
                    Return
                End Try
            End If
        Catch AccessException As Exception
            MsgBox("错误：" & AccessException.Message, MsgBoxStyle.OkOnly, AccessException.Source)
        End Try
        AddText()
        LoadYF()

        '清空内容
        ConText.Clear()
        PictureNode.Image = Nothing
        ConBoder.SplitterDistance = ConBoder.Height
    End Sub

#End Region

#Region "Loading Main Form"

    Function ReadCfg(ByVal ItemStr As String) As String
        Try
            Dim AccessString As String = "SELECT 设置 FROM 配置 WHERE 项目='" & ItemStr & "'"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then
                Return ""
            Else
                Return TempDataSet.Tables(0).Rows(0).Item(0).ToString
            End If
        Catch AccessException As Exception
            Return ""
        End Try
    End Function

    Sub SaveCfg(ByVal ItemStr As String, ByVal CfgStr As String)
        Try
            Dim AccessString As String = "SELECT 设置 FROM 配置 WHERE 项目='" & ItemStr & "'"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then
                AddCfgItem(ItemStr, CfgStr)
            End If
        Catch AccessException As Exception

        End Try

        Try
            Dim AccessString As String = "UPDATE 配置 SET 设置='" & CfgStr & "' WHERE 项目='" & ItemStr & "'"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            Me.Cursor = Cursors.Default
            MsgBox(AccessException.Message)
        End Try
    End Sub

    Sub AddCfgItem(ByVal ItemStr As String, ByVal CfgStr As String)
        Try
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessString As String = "INSERT INTO 配置(项目,设置) VALUES('" & ItemStr & "','" & CfgStr & "')"
            Dim AccessCmd = New OleDb.OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "保存")
        End Try
    End Sub

    Sub SaveReg()
        SaveSetting("MyNote", "TextMark", "LastText", ConText.SelectionStart)
        SaveSetting("MyNote", "FormTop", "TopMost", Me.TopMost)
        If Me.WindowState = FormWindowState.Normal Then
            SaveSetting("MyNote", "FormSize", "Width", Me.Width)
            SaveSetting("MyNote", "FormSize", "Height", Me.Height)
        End If
        SaveSetting("MyNote", "PicNode", "Height", ConBoder.SplitterDistance)
        SaveSetting("MyNote", "TreeNode", "Selected", Remember)
        SaveSetting("MyNote", "TreeNode", "MarkSelected", MarkRemember)
        SaveSetting("MyNote", "DBSel", "DBName", Com_DB.Text)
    End Sub

    Private Sub Main_Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        SaveReg()
        Notify_隐藏窗口.Checked = True
        保存SToolStripButton_Click(sender, e)
        Me.Visible = False
        SetProcessWorkingSetSize()
        e.Cancel = True
    End Sub

    Sub PassWordTest()
        Try
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            AccessConn.Close()
        Catch AccessException As Exception
            Pass_Form.T1()
            Pass_Form.Label1.Text = "请输入密码："
            Pass_Form.Text = "密码"
            Pass_Form.ShowDialog()
        End Try
    End Sub

    Sub PassWordCheck()
        Try
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            AccessConn.Close()
        Catch AccessException As Exception
            End
        End Try
    End Sub

    Sub YFHandle(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim AccessString As String = "SELECT * FROM 高亮 WHERE 语言='" & sender.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
            Dim RegStr1 As String = TempDataSet.Tables(0).Rows(0).Item(1).ToString.Replace("双引号", """").Trim
            Dim RegStr2 As String = TempDataSet.Tables(0).Rows(0).Item(2).ToString.Replace("双引号", """").Trim
            Dim RegStr3 As String = TempDataSet.Tables(0).Rows(0).Item(3).ToString.Replace("双引号", """").Trim
            Dim RegStr4 As String = TempDataSet.Tables(0).Rows(0).Item(4).ToString.Replace("双引号", """").Trim
            Dim RegStr5 As String = TempDataSet.Tables(0).Rows(0).Item(5).ToString.Replace("双引号", """").Trim
            Dim RegStr6 As String = TempDataSet.Tables(0).Rows(0).Item(6).ToString.Replace("双引号", """").Trim
            Dim RegStr7 As String = TempDataSet.Tables(0).Rows(0).Item(7).ToString.Replace("双引号", """").Trim
            Dim RegStr8 As String = TempDataSet.Tables(0).Rows(0).Item(8).ToString.Replace("双引号", """").Trim
            Dim RegStr9 As String = TempDataSet.Tables(0).Rows(0).Item(9).ToString.Replace("双引号", """").Trim
            Dim RegStr10 As String = TempDataSet.Tables(0).Rows(0).Item(10).ToString.Replace("双引号", """").Trim
            Try
                If RegStr1.Trim = "" Then Exit Try
                Dim RegStrCol1 As New System.Text.RegularExpressions.Regex(RegStr1, RegexOptions.IgnoreCase)
                Dim MatcheCols1 As MatchCollection = RegStrCol1.Matches(ConText.Text)
                For Each RegStr In MatcheCols1
                    ConText.Select(RegStr.Index, RegStr.Length)
                    ConText.SelectionColor = Color.Blue
                Next
            Catch ex As Exception
            End Try
            Try
                If RegStr2.Trim = "" Then Exit Try
                Dim RegStrCol2 As New System.Text.RegularExpressions.Regex(RegStr2, RegexOptions.IgnoreCase)
                Dim MatcheCols2 As MatchCollection = RegStrCol2.Matches(ConText.Text)
                For Each RegStr In MatcheCols2
                    ConText.Select(RegStr.Index, RegStr.Length)
                    ConText.SelectionColor = Color.Green
                Next
            Catch ex As Exception
            End Try
            Try
                If RegStr3.Trim = "" Then Exit Try
                Dim RegStrCol3 As New System.Text.RegularExpressions.Regex(RegStr3, RegexOptions.IgnoreCase)
                Dim MatcheCols3 As MatchCollection = RegStrCol3.Matches(ConText.Text)
                For Each RegStr In MatcheCols3
                    ConText.Select(RegStr.Index, RegStr.Length)
                    ConText.SelectionColor = Color.Teal
                Next
            Catch ex As Exception
            End Try
            Try
                If RegStr4.Trim = "" Then Exit Try
                Dim RegStrCol4 As New System.Text.RegularExpressions.Regex(RegStr4, RegexOptions.IgnoreCase)
                Dim MatcheCols4 As MatchCollection = RegStrCol4.Matches(ConText.Text)
                For Each RegStr In MatcheCols4
                    ConText.Select(RegStr.Index, RegStr.Length)
                    ConText.SelectionColor = Color.Maroon
                Next
            Catch ex As Exception
            End Try
            Try
                If RegStr5.Trim = "" Then Exit Try
                Dim RegStrCol5 As New System.Text.RegularExpressions.Regex(RegStr5, RegexOptions.IgnoreCase)
                Dim MatcheCols5 As MatchCollection = RegStrCol5.Matches(ConText.Text)
                For Each RegStr In MatcheCols5
                    ConText.Select(RegStr.Index, RegStr.Length)
                    ConText.SelectionColor = Color.Gray
                Next
            Catch ex As Exception
            End Try
            Try
                If RegStr6.Trim = "" Then Exit Try
                Dim RegStrCol6 As New System.Text.RegularExpressions.Regex(RegStr6, RegexOptions.IgnoreCase)
                Dim MatcheCols6 As MatchCollection = RegStrCol6.Matches(ConText.Text)
                For Each RegStr In MatcheCols6
                    ConText.Select(RegStr.Index, RegStr.Length)
                    ConText.SelectionColor = Color.Purple
                Next
            Catch ex As Exception
            End Try
            Try
                If RegStr7.Trim = "" Then Exit Try
                Dim RegStrCol7 As New System.Text.RegularExpressions.Regex(RegStr7, RegexOptions.IgnoreCase)
                Dim MatcheCols7 As MatchCollection = RegStrCol7.Matches(ConText.Text)
                For Each RegStr In MatcheCols7
                    ConText.Select(RegStr.Index, RegStr.Length)
                    ConText.SelectionColor = Color.Cyan
                Next
            Catch ex As Exception
            End Try
            Try
                If RegStr8.Trim = "" Then Exit Try
                Dim RegStrCol8 As New System.Text.RegularExpressions.Regex(RegStr8, RegexOptions.IgnoreCase)
                Dim MatcheCols8 As MatchCollection = RegStrCol8.Matches(ConText.Text)
                For Each RegStr In MatcheCols8
                    ConText.Select(RegStr.Index, RegStr.Length)
                    ConText.SelectionFont = New Font(ConText.SelectionFont.Name, ConText.SelectionFont.Size, CType(FontStyle.Bold, FontStyle))
                Next
            Catch ex As Exception
            End Try
            Try
                If RegStr9.Trim = "" Then Exit Try
                Dim RegStrCol9 As New System.Text.RegularExpressions.Regex(RegStr9, RegexOptions.IgnoreCase)
                Dim MatcheCols9 As MatchCollection = RegStrCol9.Matches(ConText.Text)
                For Each RegStr In MatcheCols9
                    ConText.Select(RegStr.Index, RegStr.Length)
                    ConText.SelectionFont = New Font(ConText.SelectionFont.Name, ConText.SelectionFont.Size, CType(FontStyle.Underline, FontStyle))
                Next
            Catch ex As Exception
            End Try
            Try
                If RegStr10.Trim = "" Then Exit Try
                Dim RegStrCol10 As New System.Text.RegularExpressions.Regex(RegStr10, RegexOptions.IgnoreCase)
                Dim MatcheCols10 As MatchCollection = RegStrCol10.Matches(ConText.Text)
                For Each RegStr In MatcheCols10
                    ConText.Select(RegStr.Index, RegStr.Length)
                    ConText.SelectionFont = New Font(ConText.SelectionFont.Name, ConText.SelectionFont.Size, CType(FontStyle.Italic, FontStyle))
                Next
            Catch ex As Exception
            End Try

        Catch AccessException As Exception
            MsgBox("读取高亮信息出错" & vbCrLf & "信息：" & AccessException.Message, , "读取")
        End Try
    End Sub

    Sub LoadYF()
        Try
            Dim AccessString As String = "SELECT 语言 FROM 高亮"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
            Height_Form.ComboBox1.Items.Clear()
            语法高亮HToolStripMenuItem.DropDownItems.Clear()
            For i As Integer = 0 To TempDataSet.Tables(0).Rows.Count - 1
                Height_Form.ComboBox1.Items.Add(TempDataSet.Tables(0).Rows(i).Item(0))
                Dim OneDropItem As New ToolStripMenuItem
                OneDropItem.Name = TempDataSet.Tables(0).Rows(i).Item(0) & "ToolStripMenuItem"
                OneDropItem.Text = TempDataSet.Tables(0).Rows(i).Item(0)
                语法高亮HToolStripMenuItem.DropDownItems.Add(OneDropItem)
                AddHandler OneDropItem.Click, AddressOf YFHandle
            Next
        Catch AccessException As Exception
            MsgBox("读取高亮信息出错" & vbCrLf & "信息：" & AccessException.Message, , "读取")
        End Try

    End Sub
    Public Shared Function RunningInstance() As Boolean
        Dim process As Process
        Dim current As Process = process.GetCurrentProcess()
        Dim processes As Process() = process.GetProcessesByName(current.ProcessName)
        'Loop through the running processes in with the same name 
        For Each process In processes
            'Ignore the current process 
            If process.Id <> current.Id Then
                'Make sure that the process is running from the exe file. 
                If Assembly.GetExecutingAssembly().Location.Replace("/", "\") = current.MainModule.FileName Then
                    'Return the other process instance. 
                    'Return process
                    Return True
                    Exit Function
                End If
            End If
        Next process
        'No other instance was found, return null. 
        Return Nothing
    End Function 'RunningInstance 

    Private Sub Form_GetKill_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If RunningInstance() = True Then
            Dim TempText As String = "Dim WshShell" & vbCrLf & "Set WshShell = WScript.CreateObject(" & _
                """" & "wscript.shell" & """" & ") " & vbCrLf & "  WshShell.SendKeys( " & """" & "{F4}" & _
                """" & ")" & vbCrLf & "Set ObjFSO = CreateObject(" & """" & "Scripting.FileSystemObject" & _
                """" & ")" & vbCrLf & "ObjFSO.DeleteFile Wscript.ScriptFullName"
            Dim TempPath As String = Environment.GetFolderPath(Environment.SpecialFolder.Templates) & "\Temp.vbs"
            FileIO.FileSystem.WriteAllText(TempPath, TempText, False, System.Text.Encoding.Default)
            System.Diagnostics.Process.Start(TempPath)
            End
        End If
    End Sub
    Public Sub Main_Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            GetAllDB()
            PassWordTest()
            PassWordCheck()
            AutoUpDate(NewVerUrl, True, False)
            GetUrl(MyPage)
            Notify_隐藏窗口.Checked = False
            NotifyIcon_Key.Visible = True
            PictureNode.AllowDrop = True
            ConBoder.SplitterDistance = GetSetting("MyNote", "PicNode", "Height", ConBoder.Height)
            Me.Height = Val(GetSetting("MyNote", "FormSize", "Height", "600"))
            Me.Width = Val(GetSetting("MyNote", "FormSize", "Width", "800"))
            Me.Refresh()
            Tab_Tree.SelectedIndex = 2
            AddText()
            LoadYF()
            Tab_Tree.SelectedIndex = 0
            TreeView_Cata.Sort()
            TreeView_Cata.Nodes.Item(0).Nodes.Add("NULL")
            TreeView_Cata.Nodes.Item(0).Expand()
            '定位目录节点
            Dim FullPath As String = GetSetting("MyNote", "TreeNode", "Selected", "")
            If FullPath <> "" Then
                FindNode(FullPath)
            End If
            ConText.SelectionStart = GetSetting("MyNote", "TextMark", "LastText", "0")
            Me.TopMost = GetSetting("MyNote", "FormTop", "TopMost", "0")
            AddHandler Com_DB.SelectedIndexChanged, AddressOf Com_DB_SelectedIndexChanged
        Catch ex As Exception
            MsgBox("请尝试重新运行程序,或联系作者。", MsgBoxStyle.Information, "出错了")
            Help_Form.ShowDialog()
            End
        End Try
    End Sub

    Sub AddText()
        Try
            Dim AccessString As String = "SELECT 分类 FROM 短语"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
            Text_Form.ComboBox1.Items.Clear()
            For i As Integer = 0 To TempDataSet.Tables(0).Rows.Count - 1
                Text_Form.ComboBox1.Items.Add(TempDataSet.Tables(0).Rows(i).Item(0))
            Next
        Catch AccessException As Exception
            MsgBox("读取短语信息出错" & vbCrLf & "信息：" & AccessException.Message, , "读取")
        End Try
        Text_Form.ComboBox1.SelectedIndex = GetSetting("MyNote", "Text_Form", "Text_Class", "0")
    End Sub

#End Region

#Region "The RichText Editor Module"

    Private Sub 已阅_Tool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 已阅YCToolStripMenuItem.Click
        Dim TempLen As Integer = ConText.SelectionStart
        ConText.SelectionStart = 0
        ConText.SelectionLength = TempLen
        ConText.Cut()
    End Sub

    Private Sub 退出XToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 退出XToolStripMenuItem.Click
        SaveReg()
        NotifyIcon_Key.Visible = False
        保存SToolStripButton_Click(sender, e)
        End
    End Sub

    Private Sub 格式字体_ToolStrip_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles 字体格式WCToolStripMenuItem.Click, 格式字体ToolStripButton.ButtonClick
        ConText.HideSelection = False
        OneFontDialog.ShowColor = True
        OneFontDialog.Font = ConText.SelectionFont
        OneFontDialog.Color = ConText.SelectionColor
        If OneFontDialog.ShowDialog() <> DialogResult.Cancel Then
            ConText.SelectionFont = OneFontDialog.Font
            ConText.SelectionColor = OneFontDialog.Color
        End If
    End Sub

    Private Sub 段落格式_ToolStrip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 段落格式GCToolStripMenuItem.Click, 段落格式ToolStripButton.ButtonClick
        ConText.HideSelection = False
        Part_Form.Show()
    End Sub

    Private Sub 清除格式_ToolStrip_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles 清除格式ToolStripButton.CheckedChanged
        If 清除格式ToolStripButton.Checked Then
            Dim TempRich As New RichTextBox
            TempRich.Text = ConText.Text
            ConText.Rtf = TempRich.Rtf
            保存SToolStripButton.Enabled = True
        End If
    End Sub

    Private Sub 帮助LToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 帮助LToolStripButton.Click
        Help_Form.ShowDialog()
    End Sub

    Private Sub ConText_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles ConText.DragDrop
        Try
            Dim DragFileName As String
            Dim DragFile() As String
            DragFile = e.Data.GetData(DataFormats.FileDrop)
            If DragFile Is Nothing Then
                e.Effect = DragDropEffects.Move
                Return
            End If
            e.Effect = DragDropEffects.None
            DragFileName = DragFile.GetValue(0).ToString()
            OpenFile(DragFileName)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ConText_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkClickedEventArgs) Handles ConText.LinkClicked
        System.Diagnostics.Process.Start(e.LinkText)
    End Sub

    Private Sub 保存SToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 保存SToolStripMenuItem.Click, 保存SToolStripButton.Click
        If TreeView_Cata.SelectedNode Is Nothing Then Exit Sub
        If Tab_Tree.SelectedIndex <> 0 Or TreeView_Cata.SelectedNode.Name = "烂笔头" Then 总字数ToolStripStatusLabel.Text = "不可编辑！" : Exit Sub
        If 总字数ToolStripStatusLabel.ForeColor = Color.Red Then Return
        Dim NewText As String
        Dim AccessString As String
        Try
            If PictureNode.SizeMode = PictureBoxSizeMode.Zoom Then
                AccessString = "UPDATE 记录 SET 自动缩放=True" & " WHERE 编号=" & TreeView_Cata.SelectedNode.Name
            Else
                AccessString = "UPDATE 记录 SET 自动缩放=False" & " WHERE 编号=" & TreeView_Cata.SelectedNode.Name
            End If
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , sender.Text)
        End Try
        Try
            AccessString = "UPDATE 记录 SET 格式=" & 清除格式ToolStripButton.Checked & " WHERE 编号=" & TreeView_Cata.SelectedNode.Name
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , sender.Text)
        End Try
        Try
            If 保存SToolStripButton.Enabled = False Then Exit Sub
            If 清除格式ToolStripButton.Checked Then
                NewText = ConText.Text.Replace("""", "双引号")
            Else
                NewText = ConText.Rtf.Replace("""", "双引号")
            End If
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            AccessString = "UPDATE 记录 SET 文本=? WHERE 编号=" & TreeView_Cata.SelectedNode.Name
            Dim AccessCmd = New OleDb.OleDbCommand(AccessString, AccessConn)
            AccessCmd.Parameters.Add(New OleDb.OleDbParameter)
            AccessCmd.Parameters(0).Value = NewText
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "保存")
        End Try

        总字数ToolStripStatusLabel.Text = "已保存！"
        保存SToolStripButton.Enabled = False
    End Sub

    Dim TempFont As Font
    Dim TempColor As Color
    Private Sub 格式刷_ToolStrip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 格式刷ToolStripButton.Click
        ConText.SelectionLength = 0
        TempColor = ConText.SelectionColor
        TempFont = ConText.SelectionFont
    End Sub

    Private Sub ConText_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ConText.MouseUp

        If 格式刷ToolStripButton.Checked Then
            ConText.SelectionColor = TempColor
            ConText.SelectionFont = TempFont
            格式刷ToolStripButton.Checked = False
        End If

    End Sub

    Private Sub AutoSave_Timer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer_AutoSave.Tick
        保存SToolStripButton_Click(sender, e)
        If 总字数ToolStripStatusLabel.Text = "已保存！" Then 总字数ToolStripStatusLabel.Text = "自动保存！"
        Timer_AutoSave.Enabled = False
    End Sub

    Private Sub ConText_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ConText.TextChanged
        If Tab_Tree.SelectedIndex = 0 Then 保存SToolStripButton.Enabled = True
        总字数ToolStripStatusLabel.Text = "总字数:" & Microsoft.VisualBasic.Left(Format(ConText.Text.Length, "Standard"), Format(ConText.Text.Length, "Standard").Length - 3)
        If Timer_AutoSave.Enabled = False Then Timer_AutoSave.Enabled = True
    End Sub

    Sub SaveFile(ByVal FileName As String)
        Try
            Me.Cursor = Cursors.WaitCursor
            Try
                Dim FileNameExtension As String
                FileNameExtension = System.IO.Path.GetExtension(FileName).ToLower
                If FileNameExtension = ".rtf" Then
                    ConText.SaveFile(FileName)
                Else
                    IO.File.WriteAllText(FileName, ConText.Text.Replace(vbLf, vbCrLf), System.Text.Encoding.Default)
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            Me.Cursor = Cursors.Arrow
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub OpenFile(ByVal FileName As String)

        Me.Cursor = Cursors.WaitCursor
        Try
            Dim FileNameExtension As String
            FileNameExtension = System.IO.Path.GetExtension(FileName).ToLower
            If FileNameExtension = ".rtf" Then
                ConText.LoadFile(FileName)
            ElseIf InStr(".bmp.jpg.png.gif", FileNameExtension) Then
                Try
                    Dim InsImg As Image = Image.FromFile(FileName)
                    Clipboard.SetDataObject(InsImg)
                    Dim myFormat As DataFormats.Format = DataFormats.GetFormat(DataFormats.Bitmap)
                    ConText.Paste(myFormat)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                Clipboard.Clear()
            Else
                ConText.Text = IO.File.ReadAllText(FileName, System.Text.Encoding.Default)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Me.Cursor = Cursors.Arrow
    End Sub

    Private Sub 粘贴VCToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 粘贴PToolStripMenuItem.Click, 粘贴PCToolStripMenuItem.Click, 粘贴PToolStripButton.Click
        ConText.Paste()
    End Sub

    Private Sub 粘贴文本_Tool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 粘贴文本NCToolStripMenuItem.Click
        ConText.SelectedText = Clipboard.GetText
    End Sub

    Private Sub 复制CCToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 复制CToolStripMenuItem.Click, 复制CCToolStripMenuItem.Click, 复制CToolStripButton.Click
        ConText.Copy()
    End Sub

    Private Sub 全选ACToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 全选AToolStripMenuItem.Click, 全选ACToolStripMenuItem.Click
        ConText.SelectAll()
    End Sub

    Private Sub 剪切XCToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 剪切TToolStripMenuItem.Click, 剪切TCToolStripMenuItem.Click, 剪切UToolStripButton.Click
        ConText.Cut()
    End Sub

    Private Sub 替换_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 替换HCToolStripMenuItem.Click, 替换HToolStripMenuItem.Click
        Replace_Form.替换.Checked = True
        Replace_Form.查找.Checked = False
        Replace_Form.Show(Me)
    End Sub

    Private Sub 查找_Tool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 查找FCToolStripMenuItem.Click, 查找FToolStripMenuItem.Click
        Replace_Form.查找.Checked = True
        Replace_Form.替换.Checked = False
        Replace_Form.Show(Me)
    End Sub

    Private Sub 删除_Tool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 删除DCToolStripMenuItem.Click
        ConText.SelectedText = ""
    End Sub

    Private Sub 打开OToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 打开OToolStripMenuItem.Click, 打开OToolStripButton.Click
        Try
            Tab_Tree.SelectTab(0)
            If TreeView_Cata.SelectedNode Is Nothing Then
                TreeView_Cata.SelectedNode = TreeView_Cata.Nodes.Item(0)
            End If
            Dim FileName As String

            If Dir(OneOpenFileDialog.FileName) = "" Then OneOpenFileDialog.FileName = ""
            OneOpenFileDialog.Title = "打开"
            OneOpenFileDialog.Filter = "文本文档|*.txt|Rtf文档|*.rtf|图片文件|*.jpg;*.gif;*.png;*.bmp|所有文件|*.*"
            If OneOpenFileDialog.ShowDialog() <> DialogResult.Cancel Then
                FileName = OneOpenFileDialog.FileName
                OpenFile(FileName)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub 另存为AToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 另存为AToolStripMenuItem.Click
        Try
            Dim FileName As String
            OneSaveFileDialog.FileName = CheckPath(TreeView_Cata.SelectedNode.Text)
            OneSaveFileDialog.Title = "保存"
            OneSaveFileDialog.Filter = "文本文档|*.txt|Rtf文档|*.rtf"
            If OneSaveFileDialog.ShowDialog() <> DialogResult.Cancel Then
                FileName = OneSaveFileDialog.FileName
                SaveFile(FileName)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub 撤销_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 撤消UToolStripMenuItem.Click, 撤销UToolStripButton.Click, 撤销UCToolStripMenuItem.Click
        ConText.Undo()
    End Sub

    Private Sub 重做_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 重做RToolStripMenuItem.Click, 重做RToolStripButton.Click
        ConText.Redo()
    End Sub

    Private Sub NodePre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 上一节ToolStripStatusLabel.Click
        If Tab_Tree.SelectedIndex <> 0 Then Return
        If TreeView_Cata.SelectedNode.PrevVisibleNode IsNot Nothing Then TreeView_Cata.SelectedNode = TreeView_Cata.SelectedNode.PrevVisibleNode
    End Sub

    Private Sub NodeFro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 下一节ToolStripStatusLabel.Click
        If Tab_Tree.SelectedIndex <> 0 Then Return
        If TreeView_Cata.SelectedNode.NextVisibleNode IsNot Nothing Then TreeView_Cata.SelectedNode = TreeView_Cata.SelectedNode.NextVisibleNode
    End Sub

    Private Sub NodeExp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 展开节点ToolStripStatusLabel.Click
        If Tab_Tree.SelectedIndex <> 0 Then Return
        If TreeView_Cata.SelectedNode.IsExpanded Then
            TreeView_Cata.SelectedNode.Collapse()
        Else
            TreeView_Cata.SelectedNode.Expand()
        End If

    End Sub

    Private Sub 导出EToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 导出EToolStripMenuItem.Click
        If TreeView_Cata.SelectedNode Is Nothing Then Exit Sub
        TreeView_Cata.SelectedNode.Expand()
        TreeView_Cata.SelectedNode.Collapse()
        If TreeView_Cata.SelectedNode.Nodes.Count <= 1 Then
            If MsgBox("当前记录的子记录数量较少，确定导出？", MsgBoxStyle.OkCancel, "警告") = MsgBoxResult.Cancel Then
                Exit Sub
            End If
        End If
        OneFolderBrowser.RootFolder = Environment.SpecialFolder.Desktop
        OneFolderBrowser.ShowNewFolderButton = False
        If OneFolderBrowser.ShowDialog() <> DialogResult.Cancel Then
            TreeView_Cata.Visible = False
            进度ToolStripProgressBar.Visible = True
            Me.Cursor = Cursors.WaitCursor
            ExText(TreeView_Cata.SelectedNode, OneFolderBrowser.SelectedPath)
            TreeView_Cata.Visible = True
            进度ToolStripProgressBar.Visible = False
            Me.Cursor = Cursors.Default
        End If
    End Sub

    Sub ExText(ByVal AllNodes As TreeNode, ByVal PathStr As String)

        进度ToolStripProgressBar.Value = 0
        For Each OneNode As TreeNode In AllNodes.Nodes

            当前ToolStripStatusLabel.Text = "当前：" & OneNode.Text
            Me.Refresh()
            进度ToolStripProgressBar.Maximum = AllNodes.Nodes.Count
            If 进度ToolStripProgressBar.Value < 进度ToolStripProgressBar.Maximum Then 进度ToolStripProgressBar.Value += 1
            Try
                Dim AccessString As String = "SELECT * FROM 记录 WHERE 编号=" & OneNode.Name
                Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                AccessConn.Open()
                Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
                AccessConn.Close()
                Dim TempDataSet As New DataSet
                AccessAdapter.Fill(TempDataSet)
                If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
                If TempDataSet.Tables(0).Rows(0).Item(2) Then
                    Dim ExtStr As String = TempDataSet.Tables(0).Rows(0).Item(3).ToString.Replace("双引号", """")
                    If ExtStr.Trim <> "" Then
                        Dim TempPath As String = PathStr & "\" & OneNode.FullPath & ".txt"
                        If Directory.Exists(Path.GetDirectoryName(TempPath)) = False Then Directory.CreateDirectory(Path.GetDirectoryName(TempPath))
                        File.WriteAllText(PathStr & "\" & CheckPath(OneNode.FullPath) & ".txt", ExtStr.Replace(vbLf, vbCrLf), System.Text.Encoding.Default)
                    End If
                Else
                    Dim ExtRtf As New RichTextBox
                    ExtRtf.Rtf = TempDataSet.Tables(0).Rows(0).Item(3).ToString.Replace("双引号", """")
                    If ExtRtf.Text.Trim <> "" Then
                        Dim TempPath As String = PathStr & "\" & OneNode.FullPath & ".rtf"
                        If Directory.Exists(Path.GetDirectoryName(TempPath)) = False Then Directory.CreateDirectory(Path.GetDirectoryName(TempPath))
                        ExtRtf.SaveFile(TempPath)
                    End If
                End If
            Catch ex As Exception
                MsgBox("导出失败：" & vbCrLf & ex.Message & vbCrLf & "可以尝试另存为")
            End Try
            OneNode.Expand()
            If OneNode.Nodes.Count > 0 Then ExText(OneNode, PathStr)
        Next

    End Sub

    Private Sub Tab_Tree_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tab_Tree.SelectedIndexChanged
        保存SToolStripButton.Enabled = False
        Select Case Tab_Tree.SelectedIndex
            Case 0
                If TreeView_Cata.SelectedNode IsNot Nothing Then ReadNode(TreeView_Cata.SelectedNode.Name)
            Case 1
                If TreeView_Seach.SelectedNode Is Nothing Then Exit Select
                If TreeView_Seach.SelectedNode.Level <> 1 Then Exit Select
                ReadNode(TreeView_Seach.SelectedNode.Name)
                当前ToolStripStatusLabel.Text = "当前:" & TreeView_Seach.SelectedNode.Text & " (" & TreeView_Seach.SelectedNode.Nodes.Count() & PicCStr & ")"
            Case 2
                TreeView_Mark.Nodes.Item(0).Collapse()
                TreeView_Mark.Nodes.Item(0).Expand()
        End Select
    End Sub
#End Region

#Region "The Tools Menu Sub Region"

    Private Sub 插入IToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 插入IToolStripMenuItem.Click
        Symbol_Form.Show(Me)
    End Sub

    Private Sub 批量编辑MToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 批量编辑MToolStripMenuItem.Click
        MulitText_Form.Show(Me)
    End Sub

    Private Sub 便捷输入EToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 便捷输入EToolStripMenuItem.Click
        Text_Form.Show(Me)
    End Sub

    Private Sub MD5加密SToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MD5加密SToolStripMenuItem.Click
        MD5_Form.Show(Me)
    End Sub

    Private Sub 转换编码RToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 转换编码RToolStripMenuItem.Click
        If ConText.Text.StartsWith("{\rtf1") Then
            ConText.Rtf = ConText.Text
        Else
            ConText.Text = ConText.Rtf
        End If
    End Sub


#End Region

#Region "Right Click Menus"

    Private Sub 合并子级EToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 合并子级EToolStripMenuItem.Click
        Try
            内容ToolStripProgressBar.Width = Status_ConText.Width
            占位符ToolStripStatusLabel.Visible = False
            内容ToolStripProgressBar.Visible = True
            内容ToolStripProgressBar.Maximum = TreeView_Cata.SelectedNode.Nodes.Count
            Dim AccessString As String = "SELECT * FROM 记录 WHERE 目录=" & "'" & TreeView_Cata.SelectedNode.Name & "'"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            AccessConn.Close()
            If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
            Me.Refresh()
            For i As Integer = 0 To TempDataSet.Tables(0).Rows.Count - 1
                If i <= 内容ToolStripProgressBar.Maximum Then 内容ToolStripProgressBar.Value = i
                If TempDataSet.Tables(0).Rows(i).Item(2) Then
                    ConText.Text = ConText.Text & vbLf & TempDataSet.Tables(0).Rows(i).Item(1) & vbLf & TempDataSet.Tables(0).Rows(i).Item(3).ToString.Replace("双引号", """")
                Else
                    Dim TempRichText As New RichTextBox
                    TempRichText.Rtf = TempDataSet.Tables(0).Rows(i).Item(3).ToString.Replace("双引号", """")
                    ConText.Text = ConText.Text & vbLf & TempDataSet.Tables(0).Rows(i).Item(1) & vbLf & TempRichText.Text
                End If
            Next
            内容ToolStripProgressBar.Value = 0
            内容ToolStripProgressBar.Visible = False
            占位符ToolStripStatusLabel.Visible = True
        Catch AccessException As Exception
            MsgBox("合并节点出错：" & vbCrLf & "信息：" & AccessException.Message, , "读取")
        End Try
    End Sub

    Private Sub TreeView_Cata_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TreeView_Cata.KeyDown
        If e.KeyCode = Keys.Delete Then
            删除记录DToolStripMenuItem_Click(sender, e)
        End If
    End Sub

    Private Sub 删除记录DToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 删除记录DToolStripMenuItem.Click
        If TreeView_Cata.SelectedNode Is TreeView_Cata.Nodes.Item(0) Then Exit Sub
        If MsgBox("删除后记录将不可恢复！是否继续？", MsgBoxStyle.YesNo, "警告") = MsgBoxResult.No Then
            Exit Sub
        End If
        '::'删除记录
        Me.Cursor = Cursors.WaitCursor
        Dim NodeID As String = TreeView_Cata.SelectedNode.Name
        进度ToolStripProgressBar.Width = Status_Tree.Width - 4
        当前ToolStripStatusLabel.Visible = False
        进度ToolStripProgressBar.Visible = True
        Me.Refresh()
        DelNode(NodeID)
        进度ToolStripProgressBar.Value = 0
        进度ToolStripProgressBar.Visible = False
        当前ToolStripStatusLabel.Visible = True
        ConText.Text = ""
        TreeView_Cata.SelectedNode.Remove()
        Me.Cursor = Cursors.Default
    End Sub

    Sub DelNode(ByVal NodeID As String)
        Try
            Dim AccessString As String = "DELETE FROM 记录 WHERE 编号=" & NodeID
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "DelNode")
        End Try
        '::'删除书签
        Try
            Dim AccessString As String = "DELETE FROM 书签 WHERE 节点=" & "'" & NodeID & "'"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "DelNodeMark")
        End Try
        '::'删除图库
        Try
            Dim AccessString As String = "DELETE FROM 图库 WHERE 节点=" & "'" & NodeID & "'"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "DelNodePic")
        End Try
        '::'删除子记录
        Try
            Dim AccessString As String = "SELECT * FROM 记录 WHERE 目录=" & "'" & NodeID & "'"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
            进度ToolStripProgressBar.Maximum = TempDataSet.Tables(0).Rows.Count - 1
            Me.Refresh()
            For i As Integer = 0 To TempDataSet.Tables(0).Rows.Count - 1
                If i <= 进度ToolStripProgressBar.Maximum Then 进度ToolStripProgressBar.Value = i
                DelNode(TempDataSet.Tables(0).Rows(i).Item(0))
            Next
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "DelNodeChild")
        End Try
    End Sub

    Private Sub 分割记录FToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 分割记录FToolStripMenuItem.Click
        If Tab_Tree.SelectedIndex <> 0 Then Return
        Cut_Form.Show(Me)
    End Sub

#End Region

#Region "The Image Processing"

    Private Sub PictureNode_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles PictureNode.DragEnter
        e.Effect = DragDropEffects.Move
    End Sub

    Private Sub PictureNode_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles PictureNode.DragDrop
        Try
            Dim DragFile() As String
            DragFile = e.Data.GetData(DataFormats.FileDrop)
            If DragFile Is Nothing Then
                e.Effect = DragDropEffects.Move
                Return
            End If
            e.Effect = DragDropEffects.None
            AddPic(DragFile, TreeView_Cata.SelectedNode.Name)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub 添加照片AP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 添加照片AToolStripMenuItem.Click
        Dim FileName() As String
        If Dir(OneOpenFileDialog.FileName) = "" Then OneOpenFileDialog.FileName = ""
        OneOpenFileDialog.Title = "打开"
        OneOpenFileDialog.Multiselect = True
        OneOpenFileDialog.Filter = "图片文件|*.jpg;*.gif;*.png;*.bmp|所有文件|*.*"
        If OneOpenFileDialog.ShowDialog() <> DialogResult.Cancel Then
            FileName = OneOpenFileDialog.FileNames
            内容ToolStripProgressBar.Width = Status_ConText.Width
            总字数ToolStripStatusLabel.Visible = False
            内容ToolStripProgressBar.Visible = True
            Me.Refresh()
            AddPic(FileName, TreeView_Cata.SelectedNode.Name)
            内容ToolStripProgressBar.Value = 0
            总字数ToolStripStatusLabel.Visible = True
            内容ToolStripProgressBar.Visible = False
        End If
        保存SToolStripButton_Click(sender, e)
        ReadNode(TreeView_Cata.SelectedNode.Name)
    End Sub

    Sub AddPic(ByVal FileName() As String, ByVal NodeID As String)
        内容ToolStripProgressBar.Maximum = FileName.Count - 1
        Me.Refresh()
        For i As Integer = 0 To FileName.Count - 1
            If i <= 内容ToolStripProgressBar.Maximum Then 内容ToolStripProgressBar.Value = i
            Try
                Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                AccessConn.Open()
                Dim PicturePath As String = FileName(i)
                Dim Picturebyte = My.Computer.FileSystem.ReadAllBytes(PicturePath)
                Dim AccessString As String = "INSERT INTO 图库(节点,图片) VALUES('" & NodeID & "'," & "?)"
                Dim AccessCmd = New OleDb.OleDbCommand(AccessString, AccessConn)
                AccessCmd.Parameters.Add(New OleDb.OleDbParameter)
                AccessCmd.Parameters(0).Value = Picturebyte
                AccessCmd.ExecuteNonQuery()
                AccessConn.Close()
            Catch AccessException As Exception
                MsgBox(AccessException.Message, , "AddPic")
            End Try
        Next

        Try
            Dim AccessString = "UPDATE 记录 SET 照片=1 WHERE 编号=" & NodeID
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "AddPic")
        End Try

    End Sub

    Sub DelPic(ByVal NodePath As String)
        Try
            Dim AccessString = "UPDATE 记录 SET 照片=0 WHERE 编号=" & NodePath
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "DelPic")
        End Try
    End Sub

    Private Sub 清除照片DP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 清除照片DToolStripMenuItem.Click
        If MsgBox("确定删除？", MsgBoxStyle.OkCancel, "警告") = MsgBoxResult.Cancel Then
            Exit Sub
        End If
        Dim NodePath As String = TreeView_Cata.SelectedNode.Name
        Try
            Dim AccessString = "DELETE FROM 图库 WHERE 节点=" & "'" & NodePath & "'"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , sender.Text)
        End Try
        Try
            Dim AccessString = "UPDATE 记录 SET 照片=0 WHERE 编号=" & NodePath
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , sender.Text)
        End Try
        ReadNode(NodePath)
    End Sub

    Dim PicIndex As Integer
    Dim PicID As Integer
    Private Sub PictureNode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureNode.Click

        Dim NodePath As String = TreeView_Cata.SelectedNode.Name
        If sender.Cursor = Cursors.PanWest Then
            Try
                Dim AccessString As String = "SELECT * FROM 图库 WHERE 节点=" & "'" & NodePath & "'"
                Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                AccessConn.Open()
                Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
                Dim TempDataSet As New DataSet
                AccessAdapter.Fill(TempDataSet)
                If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
                PicIndex = PicIndex + 1
                If PicIndex = TempDataSet.Tables(0).Rows.Count Then
                    PicIndex = 0
                End If
                PicID = TempDataSet.Tables(0).Rows(PicIndex).Item(0)
                Dim Picturebyte = TempDataSet.Tables(0).Rows(PicIndex).Item(2)
                PictureNode.Image = Bitmap.FromStream(New IO.MemoryStream(Picturebyte, True), True)
                AccessConn.Close()
            Catch AccessException As Exception
                MsgBox(AccessException.Message, , sender.Text)
            End Try
        ElseIf sender.Cursor = Cursors.PanEast Then
            Try
                Dim AccessString As String = "SELECT * FROM 图库 WHERE 节点=" & "'" & NodePath & "'"
                Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                AccessConn.Open()
                Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
                Dim TempDataSet As New DataSet
                AccessAdapter.Fill(TempDataSet)
                If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
                PicIndex = PicIndex - 1
                If PicIndex < 0 Then
                    PicIndex = TempDataSet.Tables(0).Rows.Count - 1
                End If
                PicID = TempDataSet.Tables(0).Rows(PicIndex).Item(0)
                Dim Picturebyte = TempDataSet.Tables(0).Rows(PicIndex).Item(2)
                PictureNode.Image = Bitmap.FromStream(New IO.MemoryStream(Picturebyte, True), True)
                AccessConn.Close()
            Catch AccessException As Exception
                MsgBox(AccessException.Message, , sender.Text)
            End Try
        End If
        SetProcessWorkingSetSize()
    End Sub

    Private Sub PictureNode_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureNode.MouseMove
        If e.Button = MouseButtons.Left Then Return
        If PictureNode.Width > ConBoder.Panel2.Width Or PictureNode.Height > ConBoder.Panel2.Height Then Exit Sub
        If e.X < sender.width / 3 Then
            sender.Cursor = Cursors.PanWest
        ElseIf e.X > 2 * sender.width / 3 Then
            sender.Cursor = Cursors.PanEast
        Else
            sender.Cursor = Cursors.Default
        End If
    End Sub

    Private Sub 自动缩放AS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 自动缩放ZToolStripMenuItem.Click
        If 自动缩放ZToolStripMenuItem.Checked Then
            PictureNode.SizeMode = PictureBoxSizeMode.Zoom
            PictureNode.Top = 0
            PictureNode.Left = 0
            PictureNode.Height = ConBoder.Panel2.Height
            PictureNode.Width = ConBoder.Panel2.Width
        Else
            PictureNode.SizeMode = PictureBoxSizeMode.CenterImage
            PictureNode.Top = 0
            PictureNode.Left = 0
            PictureNode.Height = ConBoder.Panel2.Height
            PictureNode.Width = ConBoder.Panel2.Width
        End If

        Try
            If Tab_Tree.SelectedIndex <> 0 Then Return
            Dim AccessString As String
            Dim NodePath As String = TreeView_Cata.SelectedNode.Name
            If PictureNode.SizeMode = PictureBoxSizeMode.Zoom Then
                AccessString = "UPDATE 记录 SET 自动缩放=True" & " WHERE 编号=" & NodePath
            Else
                AccessString = "UPDATE 记录 SET 自动缩放=False" & " WHERE 编号=" & NodePath
            End If
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存图片出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
    End Sub

    Private Sub 导出图片EP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 导出图片EToolStripMenuItem.Click
        Try
            Dim FileName As String
            OneSaveFileDialog.FileName = CheckPath(TreeView_Cata.SelectedNode.Text)
            OneSaveFileDialog.Title = "保存"
            OneSaveFileDialog.Filter = "图片文件|*.jpg;*.gif;*.png;*.bmp|所有文件|*.*"
            If OneSaveFileDialog.ShowDialog() <> DialogResult.Cancel Then
                FileName = OneSaveFileDialog.FileName
                PictureNode.Image.Save(FileName)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Function CheckPath(ByVal FileNameStr As String) As String
        FileNameStr = FileNameStr.Replace("/", "")
        FileNameStr = FileNameStr.Replace(":", "")
        FileNameStr = FileNameStr.Replace("?", "")
        FileNameStr = FileNameStr.Replace("*", "")
        FileNameStr = FileNameStr.Replace("|", "")
        FileNameStr = FileNameStr.Replace("<", "")
        FileNameStr = FileNameStr.Replace(">", "")
        FileNameStr = FileNameStr.Replace("""", "")
        Return FileNameStr
    End Function

    Private Sub 全部导出AP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 全部导出AToolStripMenuItem.Click
        Try
            Dim FileName As String
            OneFolderBrowser.RootFolder = Environment.SpecialFolder.Desktop
            OneFolderBrowser.ShowNewFolderButton = True
            OneFolderBrowser.Description = "选择保存的路径"
            If OneFolderBrowser.ShowDialog() <> DialogResult.Cancel Then
                FileName = OneFolderBrowser.SelectedPath
                Try
                    Dim NodePath As String = TreeView_Cata.SelectedNode.Name
                    Dim AccessString As String = "SELECT * FROM 图库 WHERE 节点=" & "'" & NodePath & "'"
                    Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                    AccessConn.Open()
                    Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
                    AccessConn.Close()
                    Dim TempDataSet As New DataSet
                    AccessAdapter.Fill(TempDataSet)
                    内容ToolStripProgressBar.Width = Status_ConText.Width
                    总字数ToolStripStatusLabel.Visible = False
                    内容ToolStripProgressBar.Visible = True
                    Me.Refresh()
                    If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
                    内容ToolStripProgressBar.Maximum = TempDataSet.Tables(0).Rows.Count - 1
                    For i As Integer = 0 To TempDataSet.Tables(0).Rows.Count - 1
                        If i <= 内容ToolStripProgressBar.Maximum Then 内容ToolStripProgressBar.Value = i
                        Dim Picturebyte = TempDataSet.Tables(0).Rows(i).Item(2)
                        Bitmap.FromStream(New IO.MemoryStream(Picturebyte, True), True).Save(FileName & "\" & CheckPath(TreeView_Cata.SelectedNode.Text) & i & ".jpg")
                    Next
                    内容ToolStripProgressBar.Value = 0
                    总字数ToolStripStatusLabel.Visible = True
                    内容ToolStripProgressBar.Visible = False
                Catch AccessException As Exception
                    MsgBox(AccessException.Message, , sender.Text)
                End Try
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub 导出图库AToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 导出图库AToolStripMenuItem.Click
        Try
            Dim FileName As String
            OneFolderBrowser.RootFolder = Environment.SpecialFolder.Desktop
            OneFolderBrowser.ShowNewFolderButton = True
            OneFolderBrowser.Description = "选择保存的路径"
            If OneFolderBrowser.ShowDialog() <> DialogResult.Cancel Then
                FileName = OneFolderBrowser.SelectedPath
                Try
                    Dim NodePath As String = TreeView_Cata.SelectedNode.Name
                    Dim AccessString As String = "SELECT * FROM 图库"
                    Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                    AccessConn.Open()
                    Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
                    AccessConn.Close()
                    Dim TempDataSet As New DataSet
                    AccessAdapter.Fill(TempDataSet)
                    内容ToolStripProgressBar.Width = Status_ConText.Width
                    总字数ToolStripStatusLabel.Visible = False
                    内容ToolStripProgressBar.Visible = True
                    Me.Refresh()
                    If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
                    内容ToolStripProgressBar.Maximum = TempDataSet.Tables(0).Rows.Count - 1
                    For i As Integer = 0 To TempDataSet.Tables(0).Rows.Count - 1
                        If i <= 内容ToolStripProgressBar.Maximum Then 内容ToolStripProgressBar.Value = i
                        Dim Picturebyte = TempDataSet.Tables(0).Rows(i).Item(2)
                        Bitmap.FromStream(New IO.MemoryStream(Picturebyte, True), True).Save(FileName & "\" & CheckPath(TreeView_Cata.SelectedNode.Text) & i & ".jpg")
                    Next
                    内容ToolStripProgressBar.Value = 0
                    总字数ToolStripStatusLabel.Visible = True
                    内容ToolStripProgressBar.Visible = False
                Catch AccessException As Exception
                    MsgBox(AccessException.Message, , sender.Text)
                End Try
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub 添加图片AT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 添加图片TToolStripMenuItem.Click
        添加照片AP_Click(sender, e)
    End Sub

    Private Sub 删除图片DP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 删除图片DToolStripMenuItem.Click
        Try
            Dim AccessString = "DELETE FROM 图库 WHERE 编号=" & PicID
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , sender.Text)
        End Try
        Try
            Dim AccessString As String = "SELECT * FROM 图库 WHERE 节点=" & "'" & TreeView_Cata.SelectedNode.Name & "'"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then
                DelPic(TreeView_Cata.SelectedNode.Name)
            End If
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , sender.Text)
        End Try
        ReadNode(TreeView_Cata.SelectedNode.Name)
    End Sub

    Private Sub 清空图片DA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 清空图片CToolStripMenuItem.Click
        清除照片DP_Click(sender, e)
    End Sub
#End Region

#Region "Teh Picture Zoom & Drag"
    Dim PX, PY As Integer
    Private Sub PictureNode_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureNode.MouseDown
        PX = e.X
        PY = e.Y
    End Sub

    Private Sub PictureNode_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureNode.DoubleClick
        If sender.Cursor = Cursors.Default Then
            If ConBoder.SplitterDistance < 25 Then
                ConBoder.SplitterDistance = ConBoder.Height * 1 / 3
            Else
                ConBoder.SplitterDistance = 0
            End If
        End If
    End Sub

    Private Sub PictureNodeR_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureNode.MouseMove
        If Not e.Button = MouseButtons.Left Then Return
        If PictureNode.Width <= ConBoder.Panel2.Width And PictureNode.Height <= ConBoder.Panel2.Height Then Exit Sub

        If PictureNode.Width + PictureNode.Left - (PX - e.X) < ConBoder.Panel2.Width Then
            PictureNode.Left = ConBoder.Panel2.Width - PictureNode.Width
        Else
            If PictureNode.Left - (PX - e.X) < 0 Then
                PictureNode.Left = PictureNode.Left - (PX - e.X)
                If Math.Abs(PictureNode.Left) < 10 Then PictureNode.Left = 0
            Else
                PictureNode.Left = 0
            End If
        End If
        If PictureNode.Height + PictureNode.Top - (PY - e.Y) < ConBoder.Panel2.Height Then
            PictureNode.Top = ConBoder.Panel2.Height - PictureNode.Height
        Else
            If PictureNode.Top - (PY - e.Y) < 0 Then
                PictureNode.Top = PictureNode.Top - (PY - e.Y)
                If Math.Abs(PictureNode.Top) < 10 Then PictureNode.Top = 0
            Else
                PictureNode.Top = 0
            End If
        End If
    End Sub

    Private Sub PictureNode_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureNode.MouseWheel
        sender.Cursor = Cursors.Default
        If PictureNode.SizeMode <> PictureBoxSizeMode.Zoom Then PictureNode.SizeMode = PictureBoxSizeMode.Zoom
        If 自动缩放ZToolStripMenuItem.Checked Then 自动缩放ZToolStripMenuItem.Checked = False
        If e.Delta > 0 Then
            For i As Integer = 0 To 10
                If PictureNode.Height > PictureNode.Image.Height * 5 Then Return
                PictureNode.Height = PictureNode.Height * 1.01
                PictureNode.Width = PictureNode.Width * 1.01
                PictureNode.Top = PictureNode.Top - PictureNode.Height * 0.005
                PictureNode.Left = PictureNode.Left - PictureNode.Width * 0.005
            Next
        Else
            For i As Integer = 0 To 10
                If PictureNode.Height / 1.01 + PictureNode.Top > ConBoder.Panel2.Height Then PictureNode.Height = PictureNode.Height / 1.01
                If PictureNode.Top + PictureNode.Height * 0.005 <= 0 Then PictureNode.Top = PictureNode.Top + PictureNode.Height * 0.005
                If PictureNode.Width / 1.01 + PictureNode.Left > ConBoder.Panel2.Width Then PictureNode.Width = PictureNode.Width / 1.01
                If PictureNode.Left + PictureNode.Width * 0.005 <= 0 Then PictureNode.Left = PictureNode.Left + PictureNode.Width * 0.005
                If Math.Abs(PictureNode.Height - ConBoder.Panel2.Height) < 10 Then
                    PictureNode.Height = ConBoder.Panel2.Height
                    PictureNode.Top = 0
                End If
                If Math.Abs(PictureNode.Width - ConBoder.Panel2.Width) < 10 Then
                    PictureNode.Width = ConBoder.Panel2.Width
                    PictureNode.Left = 0
                End If
            Next
        End If

    End Sub

    Private Sub PictureNodeR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureNode.Click
        PictureNode.Focus()
    End Sub



    Private Sub MainFormR_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        PictureNode.Top = 0
        PictureNode.Left = 0
        PictureNode.Height = ConBoder.Panel2.Height
        PictureNode.Width = ConBoder.Panel2.Width
    End Sub

    Private Sub Main_Form_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        PictureNode.Top = 0
        PictureNode.Left = 0
        PictureNode.Height = ConBoder.Panel2.Height
        PictureNode.Width = ConBoder.Panel2.Width
    End Sub

    Private Sub ConBoder_SplitterMoved(ByVal sender As Object, ByVal e As System.Windows.Forms.SplitterEventArgs) Handles ConBoder.SplitterMoved, FormBoder.SplitterMoved
        PictureNode.Top = 0
        PictureNode.Left = 0
        PictureNode.Height = ConBoder.Panel2.Height
        PictureNode.Width = ConBoder.Panel2.Width
    End Sub

#End Region

#Region "The System Management"

    Sub CloseDB()
        Do Until File.Exists(My.Application.Info.DirectoryPath & "\Note.ldb") = False
            Try
                Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                AccessConn.Open()
                AccessConn.Dispose()
                OleDbConnection.ReleaseObjectPool()
            Catch AccessException As Exception
                MsgBox(AccessException.Message)
            End Try
        Loop
    End Sub

    Private Sub 修改密码Tool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 修改密码XToolStripMenuItem.Click
        CloseDB()
        Pass_Form.Text = "修改密码"
        Pass_Form.ShowDialog()
    End Sub

    Private Sub 清除密码Tool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 清除密码QToolStripMenuItem.Click
        CloseDB()
        Pass_Form.Text = "清除密码"
        Pass_Form.ShowDialog()
    End Sub

    Private Sub 压缩数据Tool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 压缩数据CToolStripMenuItem.Click
        Me.Visible = False
        CloseDB()
        Dim AccessDBApp As New Microsoft.Office.Interop.Access.Application
        Try

            Dim OldSize As Double = FileSystem.FileLen(My.Application.Info.DirectoryPath & "\" & AccessDB & ".mdb") / 1000000
            Dim TempPath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            If File.Exists(My.Application.Info.DirectoryPath & "\AccessTempDB.mdb") Then DeleteToRecycleBin(My.Application.Info.DirectoryPath & "\AccessTempDB.mdb")
            Me.Cursor = Cursors.WaitCursor
            AccessDBApp.CompactRepair(My.Application.Info.DirectoryPath & "\" & AccessDB & ".mdb", My.Application.Info.DirectoryPath & "\AccessTempDB.mdb", False)
            AccessDBApp.Quit()

            AccessDBApp = Nothing
            DeleteToRecycleBin(My.Application.Info.DirectoryPath & "\" & AccessDB & ".mdb")
            File.Move(My.Application.Info.DirectoryPath & "\AccessTempDB.mdb", My.Application.Info.DirectoryPath & "\" & AccessDB & ".mdb")
            Dim NewSize As Double = FileSystem.FileLen(My.Application.Info.DirectoryPath & "\" & AccessDB & ".mdb") / 1000000
            Me.Cursor = Cursors.Default
            MsgBox("你将原数据库从" & Format(OldSize, ".00") & "M" & "压缩到了" & Format(NewSize, ".00") & "M", , "压缩成功")
        Catch ex As Exception
            AccessDBApp.Quit()
            AccessDBApp = Nothing
            Me.Cursor = Cursors.Default
            MsgBox(ex.Message)
        End Try
        Me.Visible = True
    End Sub

    Private Sub 问题反馈AToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 问题反馈AToolStripMenuItem.Click
        Email_Form.Show(Me)
    End Sub

    Private Sub 使用评价PToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 使用评价PToolStripMenuItem.Click
        Board_Form.Show(Me)
    End Sub

    Private Sub 检查更新_Tool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 检查更新NToolStripMenuItem.Click
        AutoUpDate(NewVerUrl, False, True)
    End Sub

#End Region

#Region "To Set Paragraph Formats"

    Private Sub 自动换行ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 自动换行ToolStripMenuItem.Click
        If 自动换行ToolStripMenuItem.Checked = False Then
            ConText.WordWrap = False
        Else
            ConText.WordWrap = True
        End If
    End Sub

    Private Sub 居中对齐ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 居中对齐ToolStripMenuItem.Click
        ConText.SelectionAlignment = HorizontalAlignment.Center
    End Sub

    Private Sub 左对齐ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 左对齐ToolStripMenuItem.Click
        ConText.SelectionAlignment = HorizontalAlignment.Left
    End Sub

    Private Sub 右对齐ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 右对齐ToolStripMenuItem.Click
        ConText.SelectionAlignment = HorizontalAlignment.Right
    End Sub

    Private Sub 作为上标ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 作为上标ToolStripMenuItem.Click
        ConText.SelectionCharOffset = 5
    End Sub

    Private Sub 作为下标ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 作为下标ToolStripMenuItem.Click
        ConText.SelectionCharOffset = -5
    End Sub

    Private Sub 首行缩进ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 首行缩进ToolStripMenuItem.Click
        ConText.SelectionIndent = 24
        ConText.SelectionHangingIndent = -24
    End Sub

    Private Sub 项目符号ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 项目符号ToolStripMenuItem.Click
        ConText.SelectionBullet = True
    End Sub

    Private Sub 加粗字体ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 加粗字体ToolStripMenuItem.Click
        ConText.SelectionFont = New Font(ConText.SelectionFont.Name, ConText.SelectionFont.Size, FontStyle.Bold)
    End Sub

    Private Sub 斜体字体ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 斜体字体ToolStripMenuItem.Click
        ConText.SelectionFont = New Font(ConText.SelectionFont.Name, ConText.SelectionFont.Size, FontStyle.Italic)
    End Sub

    Private Sub 加粗倾斜ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 加粗倾斜ToolStripMenuItem.Click
        ConText.SelectionFont = New Font(ConText.SelectionFont.Name, ConText.SelectionFont.Size, FontStyle.Italic Or FontStyle.Bold)
    End Sub

    Private Sub 加下划线ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 加下划线ToolStripMenuItem.Click
        ConText.SelectionFont = New Font(ConText.SelectionFont.Name, ConText.SelectionFont.Size, FontStyle.Underline)
    End Sub

    Private Sub 加删除线ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 加删除线ToolStripMenuItem.Click
        ConText.SelectionFont = New Font(ConText.SelectionFont.Name, ConText.SelectionFont.Size, FontStyle.Strikeout)
    End Sub

    Private Sub 恢复默认ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 恢复默认ToolStripMenuItem.Click
        ConText.SelectionFont = New Font("宋体", 9, FontStyle.Regular)
    End Sub

    Private Sub 清除颜色ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 清除颜色ToolStripMenuItem.Click
        ConText.SelectionColor = Color.Black
    End Sub
#End Region

#Region "To Insert Text"

    Private Sub 符号ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 符号ToolStripMenuItem.Click
        Symbol_Form.Show()
    End Sub

    Private Sub 时间ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 时间ToolStripMenuItem.Click
        ConText.SelectedText = Format(Now, "hh:mm")
    End Sub

    Private Sub 日期ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 日期ToolStripMenuItem.Click
        ConText.SelectedText = Format(Now, "yy-MM-dd")
    End Sub

    Private Sub 日期时间ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 日期时间ToolStripMenuItem.Click
        ConText.SelectedText = Format(Now, "yy-MM-dd hh:mm")
    End Sub

    Private Sub 自定义图片ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 自定义图片ToolStripMenuItem.Click
        Try
            Tab_Tree.SelectTab(0)
            If TreeView_Cata.SelectedNode Is Nothing Then
                TreeView_Cata.SelectedNode = TreeView_Cata.Nodes.Item(0)
            End If
            Dim FileName As String
            If Dir(OneOpenFileDialog.FileName) = "" Then OneOpenFileDialog.FileName = ""
            OneOpenFileDialog.Title = "插入"
            OneOpenFileDialog.Filter = "图片文件|*.jpg;*.gif;*.png;*.bmp|所有文件|*.*"
            If OneOpenFileDialog.ShowDialog() <> DialogResult.Cancel Then
                FileName = OneOpenFileDialog.FileName
                OpenFile(FileName)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

#End Region

#Region "The Auxiliary Tools"

    Private Sub 计算器CToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 计算器CToolStripMenuItem.Click
        If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\calc.exe") Then System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\calc.exe")
    End Sub

    Private Sub 命令行MToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 命令行MToolStripMenuItem.Click
        If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\cmd.exe") Then System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\cmd.exe")
    End Sub

    Private Sub 讲述人TToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 讲述人TToolStripMenuItem.Click
        If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\narrator.exe") Then System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\narrator.exe")
    End Sub

    Private Sub 放大镜RToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 放大镜RToolStripMenuItem.Click
        If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\magnify.exe") Then System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\magnify.exe")
    End Sub

    Private Sub 屏幕键盘KToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 屏幕键盘KToolStripMenuItem.Click
        If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\osk.exe") Then System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\osk.exe")
    End Sub

    Private Sub 语音识别YToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 语音识别YToolStripMenuItem.Click
        If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Windows) & "\Speech\Common\sapisvr.exe") Then System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.Windows) & "\Speech\Common\sapisvr.exe", "-SpeechUX")
    End Sub

    Private Sub 截图工具PToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 截图工具PToolStripMenuItem.Click
        If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\SnippingTool.exe") Then System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) & "\SnippingTool.exe")
    End Sub

    Private Sub 语法高亮ToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 语法高亮HToolStripMenuItem.Click
        Height_Form.Show(Me)
    End Sub

    Private Sub 阅读这节SToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 阅读这节SToolStripMenuItem.Click
        Dim TempStr As String = CheckPath(ConText.Text)
        TempStr = TempStr.Replace(vbLf, """" & " & vbCrLf & " & """")
        Dim TempText As String = "CreateObject(" & """" & "SAPI.SpVoice" & """" & ").Speak" & """" & TempStr & """" & vbCrLf & _
                                 "Set ObjFSO = CreateObject(" & """" & "Scripting.FileSystemObject" & _
                                 """" & ")" & vbCrLf & "ObjFSO.DeleteFile Wscript.ScriptFullName"
        Dim TempPath As String = Environment.GetFolderPath(Environment.SpecialFolder.Templates) & "\" & TreeView_Cata.SelectedNode.Name & ".vbs"
        FileIO.FileSystem.WriteAllText(TempPath, TempText, False, System.Text.Encoding.Default)
        System.Diagnostics.Process.Start(TempPath)
    End Sub
#End Region

#Region "The History Of MyNote"

    Private Sub TreeView_Mark_BeforeExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles TreeView_Mark.BeforeExpand
        e.Node.Nodes.Clear()
        If e.Node.Level = 0 Then

            For i As Integer = 0 To 2
                Dim OneNode As New TreeNode
                OneNode.Name = Format(DateAdd(DateInterval.Day, -i, Now()), "yyyy/MM/dd")
                Select Case i
                    Case 0
                        OneNode.Text = "今天"
                    Case 1
                        OneNode.Text = "昨天"
                    Case 2
                        OneNode.Text = "前天"
                End Select
                OneNode.Nodes.Add("NULL")
                e.Node.Nodes.Add(OneNode)
            Next
            Dim LastNode As New TreeNode
            LastNode.Text = "更早"
            LastNode.Name = "更早"
            LastNode.Nodes.Add("NULL")
            e.Node.Nodes.Add(LastNode)
        ElseIf e.Node.Level = 1 Then
            Try
                Dim AccessString As String
                If e.Node.Name = "更早" Then
                    AccessString = "SELECT * FROM 书签 WHERE DATEDIFF('d',日期,Date()) >= 3 ORDER BY 编号 DESC"
                Else
                    AccessString = "SELECT * FROM 书签 WHERE 日期 = #" & e.Node.Name & "# ORDER BY 编号 DESC"
                End If
                Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                AccessConn.Open()
                Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
                Dim TempDataSet As New DataSet
                AccessAdapter.Fill(TempDataSet)
                If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
                For i As Integer = 0 To TempDataSet.Tables(0).Rows.Count - 1
                    Dim OneNode As New TreeNode
                    OneNode.Name = TempDataSet.Tables(0).Rows(i).Item(2)
                    OneNode.Text = TempDataSet.Tables(0).Rows(i).Item(3)
                    OneNode.ImageIndex = TempDataSet.Tables(0).Rows(i).Item(4)
                    OneNode.SelectedImageIndex = TempDataSet.Tables(0).Rows(i).Item(5)
                    e.Node.Nodes.Add(OneNode)
                Next
                AccessConn.Close()
            Catch AccessException As Exception
                MsgBox(AccessException.Message, , sender.Text)
            End Try
        End If
    End Sub

    Private Sub TreeView_Mark_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TreeView_Mark.KeyDown
        If e.KeyCode = Keys.Delete Then
            If MsgBox("确定删除？", MsgBoxStyle.OkCancel, "警告") = MsgBoxResult.Cancel Then
                Exit Sub
            End If
            '::'删除记录
            Dim NewPath As String = TreeView_Mark.SelectedNode.Name
            Try
                Dim AccessString As String = "DELETE FROM 书签 WHERE 节点='" & NewPath & "'"
                Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                AccessConn.Open()
                Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
                AccessCmd.ExecuteNonQuery()
                AccessConn.Close()
            Catch AccessException As Exception
                MsgBox(AccessException.Message, , sender.Text)
            End Try
            '::'删除子记录
            Try
                Dim AccessString As String
                Select Case NewPath
                    Case Is = "更早"
                        AccessString = "DELETE FROM 书签 WHERE DATEDIFF('d',日期,Date()) >= 3"
                    Case Is = "历史"
                        AccessString = "DELETE FROM 书签"
                    Case Else
                        AccessString = "DELETE FROM 书签 WHERE 日期=#" & NewPath & "#"
                End Select
                Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                AccessConn.Open()
                Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
                AccessCmd.ExecuteNonQuery()
                AccessConn.Close()
            Catch AccessException As Exception
                MsgBox(AccessException.Message, , sender.Text)
            End Try
            TreeView_Mark.SelectedNode.Collapse()
            ConText.Text = ""
        End If
    End Sub

    Sub AddMark(ByVal NodeDate As String, ByVal NodeText As String, ByVal LinkStr As String, ByVal ImageIndex As Integer, ByVal SImageIndex As String)
        Dim TempID As String = ""
        Try
            Dim AccessString As String = "DELETE FROM 书签 WHERE 节点='" & LinkStr & "'"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            Status_Tree.Text = "记忆记录历史失败！(CODE:ADDMARK)"
        End Try
        Try
            Dim AccessString As String = "INSERT INTO 书签(日期,名称,节点,图标,标记) VALUES('" & NodeDate & "','" & NodeText & "','" & LinkStr & "'," & ImageIndex & "," & SImageIndex & ")"
            Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            Status_Tree.Text = "记忆记录历史失败！(CODE:ADDMARK)"
        End Try
        If ImageIndex = 0 Then
            Try
                Dim AccessString As String = "SELECT * FROM 书签 "
                Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                AccessConn.Open()
                Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
                Dim TempDataSet As New DataSet
                AccessAdapter.Fill(TempDataSet)
                AccessConn.Close()
                If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
                TempID = TempDataSet.Tables(0).Rows(TempDataSet.Tables(0).Rows.Count - 1).Item(0)
            Catch AccessException As Exception
                Status_Tree.Text = "记忆记录历史失败！(CODE:GETID)"
            End Try

            Try
                Dim AccessString As String = "UPDATE 书签 SET 节点= '" & TempID & "' WHERE 编号 = " & TempID
                Dim AccessConn As New OleDb.OleDbConnection(AccessConnectionString)
                AccessConn.Open()
                Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
                AccessCmd.ExecuteNonQuery()
                AccessConn.Close()
            Catch AccessException As Exception
                Status_Tree.Text = "记忆记录历史失败！(CODE:UPDATE)"
            End Try
        End If
    End Sub

    Private Sub TreeView_Mark_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView_Mark.AfterSelect
        If e.Node.Level = 2 Then
            ReadNode(e.Node.Name)
        End If
        当前ToolStripStatusLabel.Text = "当前:" & e.Node.Text & " (" & e.Node.Nodes.Count() & PicCStr & ")"
    End Sub

    Private Sub TreeView_Mark_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeView_Mark.NodeMouseDoubleClick
        If TreeView_Mark.SelectedNode Is Nothing Then Exit Sub
        If TreeView_Mark.SelectedNode.Level <> 2 Then Exit Sub
        FindNode(e.Node.Name)
        If TreeView_Cata.SelectedNode IsNot Nothing Then Exit Sub
        If TreeView_Cata.SelectedNode.Name = e.Node.Name Then
            Tab_Tree.SelectedIndex = 0
        Else
            MsgBox("原始记录不存在！")
            Status_Tree.Text = "空链接"
        End If

    End Sub

#End Region

#Region "The NotifyIcon Ctrl"

    Private Sub Notify_退出程序_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Notify_退出程序.Click
        SaveSetting("MyNote", "TextMark", "LastText", ConText.SelectionStart)
        SaveSetting("MyNote", "FormTop", "TopMost", Me.TopMost)
        SaveSetting("MyNote", "FormSize", "Width", Me.Width)
        SaveSetting("MyNote", "FormSize", "Height", Me.Height)
        SaveSetting("MyNote", "FormSize", "State", Me.WindowState)
        SaveSetting("MyNote", "TreeNode", "Selected", Me.Remember)
        SaveSetting("MyNote", "TreeNode", "MarkSelected", Me.MarkRemember)
        NotifyIcon_Key.Visible = False
        End
    End Sub

    Private Sub Notify_隐藏窗口_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Notify_隐藏窗口.Click
        If Notify_隐藏窗口.Checked Then
            Me.Visible = False
            Notify_隐藏窗口.Checked = True
        Else
            Me.Visible = True
            Notify_隐藏窗口.Checked = False
        End If
    End Sub

    Private Sub Notify_保持置顶_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Notify_保持置顶.Click
        Try
            If Notify_保持置顶.Checked Then
                Me.Visible = True
                Me.TopMost = True
            Else
                Me.TopMost = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, , ex.Source)
        End Try
    End Sub
 

    Private Sub NotifyIcon_Key_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon_Key.MouseClick
        If e.Button <> MouseButtons.Left Then Return
        If Me.WindowState = FormWindowState.Minimized Then
            Me.WindowState = FormWindowState.Normal
        Else
            If Me.Visible Then
                Notify_隐藏窗口.Checked = True
                Me.Visible = False
            Else
                Notify_隐藏窗口.Checked = False
                Me.Visible = True
            End If
        End If
    End Sub

    Private Sub Notify_联系作者_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Notify_联系作者.Click
        Help_Form.ShowDialog()
    End Sub

#End Region

#Region "The Global Hotkey"
    Public Const WM_HOTKEY = &H312
    Public Const MOD_ALT = &H1
    Public Const MOD_CONTROL = &H2
    Public Const MOD_SHIFT = &H4
    Public Const GWL_WNDPROC = (-4)
    Public Declare Auto Function RegisterHotKey Lib "user32.dll" Alias _
        "RegisterHotKey" (ByVal hwnd As IntPtr, ByVal id As Integer, ByVal fsModifiers As Integer, ByVal vk As Integer) As Boolean

    Public Declare Auto Function UnRegisterHotKey Lib "user32.dll" Alias _
        "UnregisterHotKey" (ByVal hwnd As IntPtr, ByVal id As Integer) As Boolean

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        RegisterHotKey(Handle, 1, Nothing, Keys.F4) '第二个热键F4
        RegisterHotKey(Handle, 2, MOD_CONTROL, Keys.D1)
        RegisterHotKey(Handle, 3, MOD_CONTROL, Keys.D2)
        RegisterHotKey(Handle, 4, MOD_CONTROL, Keys.D3)
        RegisterHotKey(Handle, 5, MOD_CONTROL, Keys.D4)
        RegisterHotKey(Handle, 6, MOD_ALT, Keys.D4)
        RegisterHotKey(Handle, 7, MOD_ALT, Keys.D3)
    End Sub

    Private Sub Form1_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        '注销全局热键
        'UnRegisterHotKey(Handle, 0)
        UnRegisterHotKey(Handle, 1)
        UnRegisterHotKey(Handle, 2)
        UnRegisterHotKey(Handle, 3)
        UnRegisterHotKey(Handle, 4)
        UnRegisterHotKey(Handle, 5)
        UnRegisterHotKey(Handle, 6)
        UnRegisterHotKey(Handle, 7)
    End Sub

    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = WM_HOTKEY Then
            Select Case m.WParam
                Case 1
                    If Me.Visible = False Then
                        Me.Visible = True
                        Notify_隐藏窗口.Checked = False
                    Else
                        If Me.WindowState = FormWindowState.Minimized Then
                            Me.WindowState = FormWindowState.Normal
                        Else
                            Me.Visible = False
                            Notify_隐藏窗口.Checked = True
                        End If
                    End If
                Case 2
                    If Text_Form.Radio_VK.Checked Then
                        SendKeys.SendWait(Text_Form.Rich_O1.Text)
                    Else
                        CopyPest(Text_Form.Rich_O1)
                    End If
                Case 3
                    If Text_Form.Radio_VK.Checked Then
                        SendKeys.SendWait(Text_Form.Rich_O2.Text)
                    Else
                        CopyPest(Text_Form.Rich_O2)
                    End If
                Case 4
                    If Text_Form.Radio_VK.Checked Then
                        SendKeys.SendWait(Text_Form.Rich_O3.Text)
                    Else
                        CopyPest(Text_Form.Rich_O3)
                    End If
                Case 5
                    If Text_Form.Radio_VK.Checked Then
                        SendKeys.SendWait(Me.ConText.Text)
                    Else
                        CopyPest(Me.ConText)
                    End If
                Case 6
                    Shell("shutdown -s")
                Case 7
                    Shell("shutdown -r")
            End Select
        End If
        MyBase.WndProc(m)
    End Sub

    Sub CopyPest(ByVal InStr As RichTextBox)
        InStr.SelectAll()
        InStr.Copy()
        InStr.SelectionLength = 0
        SendKeys.SendWait("^v")
    End Sub
#End Region


End Class

Module ModuleRecycleBin

    Function DeleteToRecycleBin(ByVal path As String) As Integer
        Dim pm As New SHFILEOPSTRUCT
        With pm
            .wFunc = Convert.ToUInt32(FO_DELETE)
            .pFrom = path & Chr(0)
            .pTo = Nothing
            .fFlags = Convert.ToUInt16(FOF_ALLOWUNDO Or FOF_NOCONFIRMATION)
        End With
        Return SHFileOperation(pm)
    End Function

    Public Const FO_DELETE As Integer = &H3
    Public Const FOF_NOCONFIRMATION As Short = &H10
    Public Const FOF_ALLOWUNDO As Short = &H40

    Declare Unicode Function SHFileOperation Lib "shell32.dll" (<[In](), Out()> ByVal sfo As SHFILEOPSTRUCT) As Int32

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Class SHFILEOPSTRUCT
        Public hwnd As IntPtr
        Public wFunc As UInt32
        Public pFrom As String
        Public pTo As String
        Public fFlags As UInt16
        Public fAnyOperationsAborted As Int32
        Public hNameMappings As IntPtr
        Public lpszProgressTitle As String
    End Class
End Module

Module EncryptMod
    '用?MD5加ó密ü方?式?加ó密ü文?本?,可é以?直±接ó使?用?。￡在ú模￡块é中D的?主÷要癮作痢?用?是?用?于?加ó密ü关?键ü字?
    'strData     需è要癮加ó密ü的?文?本?
    '返う?回?值μ      加ó密ü后ó的?文?本?
    Friend Function MD5EncryptProc(ByVal strData As String) As String
        Dim MD As New System.Security.Cryptography.MD5CryptoServiceProvider
        Try
            Return System.Text.Encoding.Default.GetString(MD.ComputeHash(System.Text.Encoding.Default.GetBytes(strData.Trim)))
        Catch ex As Exception
            Return ""
        End Try
    End Function

    '加ó密ü
    'StrText     需è要癮加ó密ü的?文?本?
    'strKey      加ó密ü时骸?使?用?的?关?键ü字?，?俗×称?密ü码?。￡
    '返う?回?值μ      加ó密ü后ó的?文?本?
    Friend Function EnText(ByVal StrText As String, ByVal strKey As String) As String
        Try
            Dim Des As New DESCryptoServiceProvider
            Dim inputByteArray() As Byte

            inputByteArray = Encoding.Default.GetBytes(StrText)
            Des.Key = ASCIIEncoding.ASCII.GetBytes(Left(MD5EncryptProc(strKey), 8).PadRight(8))
            Des.IV = ASCIIEncoding.ASCII.GetBytes(Left(MD5EncryptProc(strKey), 8).PadRight(8))

            Dim MS As New System.IO.MemoryStream
            Dim CS As New CryptoStream(MS, Des.CreateEncryptor, CryptoStreamMode.Write)
            CS.Write(inputByteArray, 0, inputByteArray.Length)
            CS.FlushFinalBlock()

            Dim Ret As New StringBuilder

            For Each b As Byte In MS.ToArray()
                Ret.AppendFormat("{0:X2}", b)
            Next

            Return Ret.ToString()
        Catch ex As Exception
            Return ""
        End Try
    End Function

    '解a密ü
    'strText     需è要癮解a密ü的?文?本?
    'strKey      解a密ü时骸?使?用?的?关?键ü字?，?必?需è和í加ó密ü时骸?的?关?健?字?相à同?，?才?可é以?解a密ü出?正y确ā?的?文?本?
    '返う?回?值μ      解a密ü后ó的?文?本?
    Friend Function DeText(ByVal strText As String, ByVal strKey As String) As String
        Try
            Dim Des As New DESCryptoServiceProvider

            Dim intLen As Integer
            intLen = strText.Length / 2 - 1
            Dim inputByteArray(intLen) As Byte
            Dim x, i As Integer
            For x = 0 To intLen
                i = Convert.ToInt32(strText.Substring(x * 2, 2), 16)
                inputByteArray(x) = CType(i, Byte)
            Next

            Des.Key = ASCIIEncoding.ASCII.GetBytes(Left(MD5EncryptProc(strKey), 8).PadRight(8))
            Des.IV = ASCIIEncoding.ASCII.GetBytes(Left(MD5EncryptProc(strKey), 8).PadRight(8))

            Dim MS As New System.IO.MemoryStream

            Dim CS As New CryptoStream(MS, Des.CreateDecryptor, CryptoStreamMode.Write)

            CS.Write(inputByteArray, 0, inputByteArray.Length)

            CS.FlushFinalBlock()

            Return Encoding.Default.GetString(MS.ToArray)
        Catch ex As Exception
            Return ""
        End Try
    End Function

End Module


