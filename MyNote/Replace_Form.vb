﻿Imports System.Text.RegularExpressions

Public Class Replace_Form

#Region "界面设计"

    Private Sub 替换_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 替换.Click
        替换.Checked = True
        查找.Checked = False
        替换UI()
    End Sub

    Private Sub 查找_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 查找.Click
        替换.Checked = False
        查找.Checked = True
        查找UI()
    End Sub

    Private Sub Replace_Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If 查找.Checked Then
            查找UI()
        Else
            替换UI()
        End If
        ComboBox_F.Focus()
        ComboBox_F.Text = Main_Form.ConText.SelectedText
    End Sub

    Sub 查找UI()
        Label2.Visible = False
        TextBox_R.Visible = False
        Button_R.Visible = False
        Button_RA.Visible = False
        Button_FNext.Top = ComboBox_F.Top + ComboBox_F.Height + 5
        Me.Height = Button_FNext.Top + Button_FNext.Height + 35
        Label3.Top = Button_FNext.Top
        'Com_Match.Top = Button_FNext.Top
    End Sub

    Sub 替换UI()
        Label2.Visible = True
        TextBox_R.Visible = True
        Button_R.Visible = True
        Button_RA.Visible = True
        Button_FNext.Top = TextBox_R.Top + TextBox_R.Height + 5
        Button_RA.Top = Button_FNext.Top + Button_FNext.Height + 5
        Button_R.Top = Button_FNext.Top + Button_FNext.Height + 5
        Me.Height = Button_RA.Top + Button_RA.Height + 35
        Label3.Top = Button_FNext.Top
        ' Com_Match.Top = Button_FNext.Top
    End Sub

#End Region

    Private Sub Button_FNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_FNext.Click
        Dim rgx As New System.Text.RegularExpressions.Regex(ComboBox_F.Text, RegexOptions.IgnoreCase)
        Dim matches As MatchCollection = rgx.Matches(Main_Form.ConText.Text)
        If matches.Count > 0 Then
            Label3.Text = "匹配到" & matches.Count & "个字符"
            For Each match As Match In matches
                Dim FindText As String = match.Value
                If Main_Form.ConText.Find(FindText, Main_Form.ConText.SelectionStart - 1, RichTextBoxFinds.None) = -1 Then
                    Label3.Text = "查找结束！"
                Else
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub Button_R_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_R.Click
        Dim rgx As New System.Text.RegularExpressions.Regex(ComboBox_F.Text, RegexOptions.IgnoreCase)
        Dim matches As MatchCollection = rgx.Matches(Main_Form.ConText.Text)
        If matches.Count > 0 Then
            Label3.Text = "匹配到" & matches.Count & "个字符"
            For Each match As Match In matches
                Dim FindText As String = match.Value
                If Main_Form.ConText.Find(FindText, Main_Form.ConText.SelectionStart - 1, RichTextBoxFinds.None) = -1 Then
                    Label3.Text = "查找结束！"
                Else
                    Main_Form.ConText.SelectedText = TextBox_R.Text
                    Main_Form.ConText.Find(FindText, Main_Form.ConText.SelectionStart + 1, RichTextBoxFinds.None)
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub Button_RA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_RA.Click
        Main_Form.ConText.SelectionStart = 1
        Main_Form.ConText.SelectionLength = 0
        Dim rgx As New System.Text.RegularExpressions.Regex(ComboBox_F.Text, RegexOptions.IgnoreCase)
        Dim matches As MatchCollection = rgx.Matches(Main_Form.ConText.Text)
        If matches.Count > 0 Then
            Label3.Text = "匹配到" & matches.Count & "个字符"
            For Each match As Match In matches
                Dim FindText As String = match.Value
                Main_Form.ConText.Text = Main_Form.ConText.Text.Replace(FindText, TextBox_R.Text)
            Next
        End If
    End Sub

End Class