﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' 有关程序集的常规信息通过下列特性集
' 控制。更改这些特性值可修改
' 与程序集关联的信息。

' 查看程序集特性的值

<Assembly: AssemblyTitle("我的笔记")> 
<Assembly: AssemblyDescription("好用的免费笔记软件")> 
<Assembly: AssemblyCompany("陈恩点")> 
<Assembly: AssemblyProduct("我的笔记")> 
<Assembly: AssemblyCopyright("Copyright © Dvmu 2012")> 
<Assembly: AssemblyTrademark("MyNote")> 

<Assembly: ComVisible(False)>

'如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
<Assembly: Guid("657c250b-5976-4340-934c-37218c72f00e")> 

' 程序集的版本信息由下面四个值组成:
'
'      主版本
'      次版本
'      内部版本号
'      修订号
'
' 可以指定所有这些值，也可以使用“内部版本号”和“修订号”的默认值，
' 方法是按如下所示使用“*”:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2014.8.3.1")> 
<Assembly: AssemblyFileVersion("2014.8.3.1")> 
