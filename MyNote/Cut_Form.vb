﻿Public Class Cut_Form

    Private Sub Button_WOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_WOK.Click
        sender.Enabled = False
        Dim TempText As String = Main_Form.ConText.Text
        Dim AllConTextLine() As String = TempText.Split(vbLf)
        If AllConTextLine.Length < 5 Then
            AllConTextLine = TempText.Split(vbCrLf)
        End If
        Dim OnePartConText As String = ""
        Dim NodeText, NodeConText, NodeDir As String
        Dim PartCount As Integer = 0
        ProgressBar1.Maximum = AllConTextLine.Length - 1
        ProgressBar1.Value = 0
        For i As Integer = 0 To AllConTextLine.Length - 1
            ProgressBar1.Value = i
            If AllConTextLine(i).Length < 20 And InStr(AllConTextLine(i), TextBox_KeyWord.Text) Then
                If OnePartConText <> "" And OnePartConText.Split(vbLf).Length > 5 Then
                    PartCount = PartCount + 1
                    Dim PartID As String = StrDup(CStr(ProgressBar1.Maximum).Length - CStr(PartCount).Length, "0") & PartCount
                    NodeText = OnePartConText.Split(vbLf)(0)
                    NodeText = NodeText.Trim(vbLf)
                    Do Until NodeText = NodeText.Trim(" ").Trim("　")
                        NodeText = NodeText.Trim(" ").Trim("　")
                    Loop
                    If CheckBox_WID.Checked Then NodeText = PartID & " " & NodeText
                    NodeConText = OnePartConText.Replace("""", "双引号")
                    NodeDir = Main_Form.TreeView_Cata.SelectedNode.Name

                    Main_Form.AddNode(NodeDir, NodeText, True, NodeConText, 2, 5)
                    Label1.Text = NodeText
                    Me.Refresh()
                End If
                OnePartConText = AllConTextLine(i)
                Continue For
            End If
            If OnePartConText <> "" Then
                OnePartConText = OnePartConText & vbLf & AllConTextLine(i)
            Else
                OnePartConText = AllConTextLine(i)
            End If
            If i = AllConTextLine.Length - 1 Then
                PartCount = PartCount + 1
                Dim PartID As String = StrDup(CStr(ProgressBar1.Maximum).Length - CStr(PartCount).Length, "0") & PartCount
                NodeText = OnePartConText.Split(vbLf)(0)
                NodeText = NodeText.Trim
                If CheckBox_WID.Checked Then NodeText = PartID & " " & NodeText
                NodeConText = OnePartConText.Replace("""", "双引号")
                NodeDir = Main_Form.TreeView_Cata.SelectedNode.Name
                Main_Form.AddNode(NodeDir, NodeText, True, NodeConText, 2, 5)
            End If
        Next
        ProgressBar1.Value = 0
        If Main_Form.TreeView_Cata.SelectedNode.Nodes.Count = 0 Then
            Main_Form.TreeView_Cata.SelectedNode.Nodes.Add("NULL")
            Main_Form.TreeView_Cata.SelectedNode.Expand()
        Else
            If Main_Form.TreeView_Cata.SelectedNode.IsExpanded Then
                Main_Form.TreeView_Cata.SelectedNode.Collapse()
                Main_Form.TreeView_Cata.SelectedNode.Expand()
            Else
                Main_Form.TreeView_Cata.SelectedNode.Expand()
            End If
        End If
        Me.Close()
    End Sub

    Private Sub TextBox_NCount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_Num.KeyPress
        If Char.IsDigit(e.KeyChar) Or e.KeyChar = Chr(8) Then
            e.Handled = False
        Else
            e.Handled = True
        End If

    End Sub

    Private Sub Button_NCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_NCancel.Click
        Me.Close()
    End Sub

    Private Sub Button_WCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_WCancel.Click
        Me.Close()
    End Sub

    Private Sub Cut_Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TextBox_KeyWord.Focus()
        TextBox_Num.Text = Main_Form.ConText.Text.Length
        Button_NOK.Enabled = True
        Button_WOK.Enabled = True
        Label1.Text = "请输入关键字：" & vbLf & "说明：关键字用于标识章节名，一般为：章 、节、篇、卷等。"
        Label2.Text = "请输入字数：" & vbLf & "说明：字数指每一段的文本字数，包括标点符号。"
    End Sub

    Private Sub Button_NOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_NOK.Click
        sender.Enabled = False
        Dim TempText As String = Main_Form.ConText.Text
        Dim AllConText As String = TempText
        Dim OnePartConText As String = ""
        Dim NodeText, NodeConText, NodeDir As String
        Dim PartCount As Integer = 0
        ProgressBar1.Maximum = AllConText.Length \ Val(TextBox_Num.Text)
        ProgressBar1.Value = 0
        For i As Integer = 0 To AllConText.Length \ Val(TextBox_Num.Text)
            ProgressBar1.Value = i
            PartCount = PartCount + 1
            Dim PartID As String
            If CStr(ProgressBar1.Maximum).Length > CStr(PartCount).Length Then
                PartID = StrDup(CStr(ProgressBar1.Maximum).Length - CStr(PartCount).Length, "0") & PartCount
            Else
                PartID = PartCount
            End If
            NodeText = Main_Form.TreeView_Cata.SelectedNode.Text & PartID
            If CheckBox_NID.Checked Then NodeText = PartID & NodeText.Replace(PartID, "")
            NodeConText = Mid(AllConText, i * Val(TextBox_Num.Text) + 1, Val(TextBox_Num.Text)).Replace("""", "双引号")
            NodeDir = Main_Form.TreeView_Cata.SelectedNode.Name
            Main_Form.AddNode(NodeDir, NodeText, True, NodeConText, 2, 5)
            Label2.Text = NodeText
            Me.Refresh()
        Next
        If Main_Form.TreeView_Cata.SelectedNode.Nodes.Count = 0 Then
            Main_Form.TreeView_Cata.SelectedNode.Nodes.Add("NULL")
            Main_Form.TreeView_Cata.SelectedNode.Expand()
        Else
            If Main_Form.TreeView_Cata.SelectedNode.IsExpanded Then
                Main_Form.TreeView_Cata.SelectedNode.Collapse()
                Main_Form.TreeView_Cata.SelectedNode.Expand()
            Else
                Main_Form.TreeView_Cata.SelectedNode.Expand()
            End If
        End If
        ProgressBar1.Value = 0
        Me.Close()
    End Sub
End Class