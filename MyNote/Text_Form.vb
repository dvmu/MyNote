﻿Imports System.Data.OleDb

Public Class Text_Form

    Private Sub Button_C_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_C.Click
        Me.Close()
    End Sub

    Private Sub Text_Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        SaveSetting("MyNote", "Text_Form", "Text_Class", ComboBox1.SelectedIndex)
        Me.Visible = False
        e.Cancel = True
    End Sub

    Sub AddClass(ByVal ClassName As String)
        Try
            Dim AccessString As String = "INSERT INTO 短语(分类) VALUES(" & "'" & ClassName & "'" & ")"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
            ComboBox1.Items.Add(ClassName)
        Catch AccessException As Exception
            MsgBox(AccessException.Message, , "添加短语分类")
        End Try

    End Sub


    Private Sub Button_O_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_O.Click
        Try
            Dim AccessString As String = "SELECT * FROM 短语 WHERE 分类=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then
                AddClass(ComboBox1.Text)
            End If
        Catch AccessException As Exception
            MsgBox("读取短语分类出错" & vbCrLf & "信息：" & AccessException.Message, , "短语")
        End Try

        Try
            Dim AccessString As String = "UPDATE 短语 SET O1 = " & """" & Rich_O1.Rtf.Replace("""", "双引号") & """" & " WHERE 分类=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存短语1出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 短语 SET O2 = " & """" & Rich_O2.Rtf.Replace("""", "双引号") & """" & " WHERE 分类=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存短语2出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 短语 SET O3 = " & """" & Rich_O3.Rtf.Replace("""", "双引号") & """" & " WHERE 分类=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存短语3出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Try
            Dim AccessString As String = "UPDATE 短语 SET 模式 = " & Radio_CP.Checked & " WHERE 分类=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
            AccessCmd.ExecuteNonQuery()
            AccessConn.Close()
        Catch AccessException As Exception
            MsgBox("保存短语3出错" & vbCrLf & "信息：" & AccessException.Message, , "保存")
        End Try
        Me.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Try
            Dim AccessString As String = "SELECT * FROM 短语 WHERE 分类=" & "'" & ComboBox1.Text & "'"
            Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
            AccessConn.Open()
            Dim AccessAdapter As OleDbDataAdapter = New OleDbDataAdapter(AccessString, AccessConn)
            AccessConn.Close()
            Dim TempDataSet As New DataSet
            AccessAdapter.Fill(TempDataSet)
            If TempDataSet.Tables(0).Rows.Count = 0 Then Exit Sub
            Rich_O1.Rtf = TempDataSet.Tables(0).Rows(0).Item(3).ToString.Replace("双引号", """")
            Rich_O2.Rtf = TempDataSet.Tables(0).Rows(0).Item(4).ToString.Replace("双引号", """")
            Rich_O3.Rtf = TempDataSet.Tables(0).Rows(0).Item(5).ToString.Replace("双引号", """")
            Radio_CP.Checked = TempDataSet.Tables(0).Rows(0).Item(2)
        Catch AccessException As Exception
            MsgBox("读取短语分类出错" & vbCrLf & "信息：" & AccessException.Message, , "短语")
        End Try
    End Sub

    Private Sub Button_Del_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Del.Click
        If MsgBox("删除该分类！是否继续？", MsgBoxStyle.YesNo, "警告") = MsgBoxResult.Yes Then
            Try
                Dim AccessString As String = "DELETE FROM 短语 WHERE 分类=" & "'" & ComboBox1.SelectedItem & "'"
                Dim AccessConn As New OleDb.OleDbConnection(Main_Form.AccessConnectionString)
                AccessConn.Open()
                Dim AccessCmd As OleDbCommand = New OleDbCommand(AccessString, AccessConn)
                AccessCmd.ExecuteNonQuery()
                AccessConn.Close()
            Catch AccessException As Exception
                MsgBox(AccessException.Message, , "删除短语分类")
            End Try
            ComboBox1.Items.Remove(ComboBox1.SelectedItem)
            ComboBox1.SelectedIndex = 0
            Exit Sub
        Else
            Exit Sub
        End If
    End Sub

    Private Sub Text_Form_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Main_Form.AddText()
    End Sub
End Class