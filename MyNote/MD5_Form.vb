﻿Public Class MD5_Form

    Private Sub Button_L_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_L.Click
        Main_Form.ConText.Text = EncryptMod.EnText(Main_Form.ConText.Text, TextBox_Key.Text)
    End Sub

    Private Sub Button_U_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_U.Click
        Main_Form.ConText.Text = EncryptMod.DeText(Main_Form.ConText.Text, TextBox_Key.Text)
    End Sub

    Private Sub MD5_Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TextBox_Key.Clear()
    End Sub
End Class