﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MulitText_Form
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.File_List = New System.Windows.Forms.ListBox()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.TextBox_Pr = New System.Windows.Forms.TextBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button_AR = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox_New = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox_Old = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TextBox_Sl = New System.Windows.Forms.TextBox()
        Me.CheckBox_Sl = New System.Windows.Forms.CheckBox()
        Me.CheckBox_YL = New System.Windows.Forms.CheckBox()
        Me.CheckBox_ID = New System.Windows.Forms.CheckBox()
        Me.CheckBox_FN = New System.Windows.Forms.CheckBox()
        Me.Button_ZH = New System.Windows.Forms.Button()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'File_List
        '
        Me.File_List.Dock = System.Windows.Forms.DockStyle.Fill
        Me.File_List.FormattingEnabled = True
        Me.File_List.IntegralHeight = False
        Me.File_List.ItemHeight = 12
        Me.File_List.Location = New System.Drawing.Point(3, 17)
        Me.File_List.Name = "File_List"
        Me.File_List.Size = New System.Drawing.Size(205, 208)
        Me.File_List.TabIndex = 0
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.SplitContainer2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.FlowLayoutPanel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.ProgressBar1)
        Me.SplitContainer1.Size = New System.Drawing.Size(434, 427)
        Me.SplitContainer1.SplitterDistance = 228
        Me.SplitContainer1.TabIndex = 1
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.GroupBox3)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.GroupBox4)
        Me.SplitContainer2.Size = New System.Drawing.Size(434, 228)
        Me.SplitContainer2.SplitterDistance = 211
        Me.SplitContainer2.TabIndex = 1
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.File_List)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox3.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(211, 228)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "文件列表"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.TextBox_Pr)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox4.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(219, 228)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "文本内容"
        '
        'TextBox_Pr
        '
        Me.TextBox_Pr.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TextBox_Pr.Location = New System.Drawing.Point(3, 17)
        Me.TextBox_Pr.Multiline = True
        Me.TextBox_Pr.Name = "TextBox_Pr"
        Me.TextBox_Pr.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox_Pr.Size = New System.Drawing.Size(213, 208)
        Me.TextBox_Pr.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox1)
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox2)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(434, 184)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button_AR)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TextBox_New)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TextBox_Old)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(209, 122)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "批量替换"
        '
        'Button_AR
        '
        Me.Button_AR.Location = New System.Drawing.Point(11, 93)
        Me.Button_AR.Name = "Button_AR"
        Me.Button_AR.Size = New System.Drawing.Size(192, 23)
        Me.Button_AR.TabIndex = 4
        Me.Button_AR.Text = "开始替换(&R)"
        Me.Button_AR.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 12)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "新文本："
        '
        'TextBox_New
        '
        Me.TextBox_New.Location = New System.Drawing.Point(68, 47)
        Me.TextBox_New.Name = "TextBox_New"
        Me.TextBox_New.Size = New System.Drawing.Size(135, 21)
        Me.TextBox_New.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 12)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "旧文本："
        '
        'TextBox_Old
        '
        Me.TextBox_Old.Location = New System.Drawing.Point(68, 20)
        Me.TextBox_Old.Name = "TextBox_Old"
        Me.TextBox_Old.Size = New System.Drawing.Size(135, 21)
        Me.TextBox_Old.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBox_Sl)
        Me.GroupBox2.Controls.Add(Me.CheckBox_Sl)
        Me.GroupBox2.Controls.Add(Me.CheckBox_YL)
        Me.GroupBox2.Controls.Add(Me.CheckBox_ID)
        Me.GroupBox2.Controls.Add(Me.CheckBox_FN)
        Me.GroupBox2.Controls.Add(Me.Button_ZH)
        Me.GroupBox2.Location = New System.Drawing.Point(218, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(209, 122)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "文本整合"
        '
        'TextBox_Sl
        '
        Me.TextBox_Sl.Location = New System.Drawing.Point(89, 64)
        Me.TextBox_Sl.Name = "TextBox_Sl"
        Me.TextBox_Sl.Size = New System.Drawing.Size(105, 21)
        Me.TextBox_Sl.TabIndex = 9
        '
        'CheckBox_Sl
        '
        Me.CheckBox_Sl.AutoSize = True
        Me.CheckBox_Sl.Location = New System.Drawing.Point(11, 66)
        Me.CheckBox_Sl.Name = "CheckBox_Sl"
        Me.CheckBox_Sl.Size = New System.Drawing.Size(72, 16)
        Me.CheckBox_Sl.TabIndex = 8
        Me.CheckBox_Sl.Text = "加分割线"
        Me.CheckBox_Sl.UseVisualStyleBackColor = True
        '
        'CheckBox_YL
        '
        Me.CheckBox_YL.AutoSize = True
        Me.CheckBox_YL.Location = New System.Drawing.Point(11, 44)
        Me.CheckBox_YL.Name = "CheckBox_YL"
        Me.CheckBox_YL.Size = New System.Drawing.Size(72, 16)
        Me.CheckBox_YL.TabIndex = 7
        Me.CheckBox_YL.Text = "开启预览"
        Me.CheckBox_YL.UseVisualStyleBackColor = True
        '
        'CheckBox_ID
        '
        Me.CheckBox_ID.AutoSize = True
        Me.CheckBox_ID.Location = New System.Drawing.Point(122, 22)
        Me.CheckBox_ID.Name = "CheckBox_ID"
        Me.CheckBox_ID.Size = New System.Drawing.Size(72, 16)
        Me.CheckBox_ID.TabIndex = 6
        Me.CheckBox_ID.Text = "添加序号"
        Me.CheckBox_ID.UseVisualStyleBackColor = True
        '
        'CheckBox_FN
        '
        Me.CheckBox_FN.AutoSize = True
        Me.CheckBox_FN.Location = New System.Drawing.Point(11, 22)
        Me.CheckBox_FN.Name = "CheckBox_FN"
        Me.CheckBox_FN.Size = New System.Drawing.Size(72, 16)
        Me.CheckBox_FN.TabIndex = 5
        Me.CheckBox_FN.Text = "留文件名"
        Me.CheckBox_FN.UseVisualStyleBackColor = True
        '
        'Button_ZH
        '
        Me.Button_ZH.Location = New System.Drawing.Point(11, 93)
        Me.Button_ZH.Name = "Button_ZH"
        Me.Button_ZH.Size = New System.Drawing.Size(192, 23)
        Me.Button_ZH.TabIndex = 4
        Me.Button_ZH.Text = "开始整合(&Z)"
        Me.Button_ZH.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ProgressBar1.Location = New System.Drawing.Point(0, 184)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(434, 11)
        Me.ProgressBar1.TabIndex = 1
        '
        'FolderBrowserDialog1
        '
        Me.FolderBrowserDialog1.Description = "请选择文件目录："
        Me.FolderBrowserDialog1.ShowNewFolderButton = False
        '
        'MulitText_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(434, 427)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "MulitText_Form"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "多文本处理"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents File_List As System.Windows.Forms.ListBox
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents TextBox_Pr As System.Windows.Forms.TextBox
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button_AR As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox_New As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox_Old As System.Windows.Forms.TextBox
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox_FN As System.Windows.Forms.CheckBox
    Friend WithEvents Button_ZH As System.Windows.Forms.Button
    Friend WithEvents CheckBox_YL As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox_ID As System.Windows.Forms.CheckBox
    Friend WithEvents TextBox_Sl As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox_Sl As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
End Class
